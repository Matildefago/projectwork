DROP DATABASE IF EXISTS pj_db;
CREATE DATABASE pj_db;
USE pj_db;

CREATE TABLE tipi (
	tipo_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	descrizione VARCHAR(100) NOT NULL
);

CREATE TABLE tipologie (
	tipologia_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	descrizione VARCHAR(50) NOT NULL
);

CREATE TABLE misure (
	misura_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	descrizione VARCHAR(20) NOT NULL
);

CREATE TABLE societa (
	societa_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	ragione_sociale VARCHAR(200) NOT NULL,
	indirizzo VARCHAR(200) NOT NULL,
	localita VARCHAR(100) NOT NULL,
	provincia VARCHAR(2) NOT NULL,
	nazione VARCHAR(2) NOT NULL,
	telefono VARCHAR(100) NOT NULL,
	fax VARCHAR(100) NOT NULL,
	partita_iva VARCHAR(20) NOT NULL
);

CREATE TABLE corsi (
	corso_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	societa INT NOT NULL,
	tipo INT NOT NULL,
	tipologia INT NOT NULL,
	descrizione VARCHAR(100) NOT NULL,
	edizione VARCHAR(30),
	previsto INT,
	erogato INT,
	durata FLOAT NOT NULL,
	misura INT NOT NULL,
	note TEXT,
	luogo VARCHAR(200) NOT NULL,
	ente VARCHAR(100) NOT NULL,
	data_erogazione DATETIME NOT NULL,
	data_chiusura DATETIME NOT NULL,
	data_censimento DATETIME NOT NULL,
	interno INT,
	FOREIGN KEY (tipologia) REFERENCES tipologie (tipologia_id) ON DELETE CASCADE,
	FOREIGN KEY (tipo) REFERENCES tipi (tipo_id) ON DELETE CASCADE,
	FOREIGN KEY (misura) REFERENCES misure (misura_id) ON DELETE CASCADE,
	FOREIGN KEY (societa) REFERENCES societa (societa_id) ON DELETE CASCADE
);

CREATE TABLE dipendenti (
	dipendente_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	matricola int not null,
	societa int NOT NULL, 
	cognome VARCHAR(100) NOT NULL,
	nome VARCHAR(100) NOT NULL,
	sesso VARCHAR(1) NOT NULL,
	data_nascita DATETIME NOT NULL,
	luogo_nascita VARCHAR(100) NOT NULL,
	stato_civile VARCHAR(1) NOT NULL,
	titolo_studio VARCHAR(100) NOT NULL,
	conseguito_presso VARCHAR(100) NOT NULL,
	anno_conseguimento INT NOT NULL,
	tipo_dipendente VARCHAR(1) NOT NULL,
	qualifica VARCHAR(1) NOT NULL,
	livello VARCHAR(1) NOT NULL,
	data_assunzione DATETIME NOT NULL,
	responsabile_risorsa INT NOT NULL,
	responsabile_area INT NOT NULL,
	data_fine_rapporto DATETIME NOT NULL,
	data_scadenza_contratto DATETIME NOT NULL,
	FOREIGN KEY (societa) REFERENCES societa (societa_id) ON DELETE CASCADE
);

CREATE TABLE docenti (
	docente_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	titolo VARCHAR(30) NOT NULL,
	ragione_sociale VARCHAR(200) NOT NULL,
	indirizzo VARCHAR(200) NOT NULL,
	localita VARCHAR(100) NOT NULL,
	provincia VARCHAR(2) NOT NULL,
	nazione VARCHAR(2) NOT NULL,
	telefono VARCHAR(100) NOT NULL,
	fax VARCHAR(100) NOT NULL,
	partita_iva VARCHAR(100) NOT NULL,
	referente TEXT NOT NULL	
);

CREATE TABLE corsi_utenti (
	corsi_utenti_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	corso INT NOT NULL,
	dipendente INT NOT NULL,
	durata FLOAT NOT NULL,
	FOREIGN KEY (corso)	REFERENCES corsi (corso_id) ON DELETE CASCADE,
	FOREIGN KEY (dipendente) REFERENCES dipendenti (dipendente_id) ON DELETE CASCADE
);

CREATE TABLE corsi_docenti (
	corsi_docenti_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	corso INT NOT NULL,
	docente INT NOT NULL,
	interno INT NOT NULL,
	FOREIGN KEY (corso)	REFERENCES corsi (corso_id) ON DELETE CASCADE
);

CREATE TABLE valutazioni_docenti (
	valutazioni_docenti_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	corso INT NOT NULL,
	docente INT NOT NULL,
	criterio VARCHAR(100) NOT NULL,
	valore TEXT,
	FOREIGN KEY (corso)	REFERENCES corsi (corso_id) ON DELETE CASCADE,
	FOREIGN KEY (docente) REFERENCES docenti (docente_id) ON DELETE CASCADE
);

CREATE TABLE valutazioni_corsi (
	valutazioni_corsi_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	corso INT NOT NULL,
	criterio VARCHAR(100) NOT NULL,
	valore TEXT,
	FOREIGN KEY (corso)	REFERENCES corsi (corso_id) ON DELETE CASCADE
);

CREATE TABLE valutazioni_utenti (
	valutazioni_utenti_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	corso INT NOT NULL,
	dipendente INT NOT NULL,
	criterio VARCHAR(100) NOT NULL,
	valore TEXT,
	FOREIGN KEY (corso)	REFERENCES corsi (corso_id) ON DELETE CASCADE,
	FOREIGN KEY (dipendente) REFERENCES dipendenti (dipendente_id) ON DELETE CASCADE
);

insert into dipendenti values (null,'123457','Verdi','Marco','M','53-01-01','Milano','Sposato','Laurea L31','Milano','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123458','Gialli','Luigi','M','53-01-01','Verona','Sposato','Laurea L31','Roma','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123459','Arancioni','Matteo','M','53-01-01','Roma','Divorziato','Laurea L07','Verona','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123460','Rosi','Rosa','F','53-01-01','Verona','Sposata','Laurea L31','Roma','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123461','Blu','Gigi','M','53-01-01','Andalucia','Divorziato','Laurea L07','Verona','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);

insert into dipendenti values (null,'123462','Verdi','Marco','M','53-01-01','Milano','Sposato','Laurea L31','Milano','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123463','Gialli','Luigi','M','53-01-01','Verona','Sposato','Laurea L31','Roma','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123464','Arancioni','Matteo','M','53-01-01','Roma','Divorziato','Laurea L07','Verona','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123465','Rosi','Rosa','F','53-01-01','Verona','Sposata','Laurea L31','Roma','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123466','Blu','Gigi','M','53-01-01','Andalucia','Divorziato','Laurea L07','Verona','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);

insert into dipendenti values (null,'123467','Beige','Marco','M','53-01-01','Milano','Sposato','Laurea L31','Milano','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123468','Bordeaux','Luigi','M','53-01-01','Verona','Sposato','Laurea L31','Roma','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123469','Ciano','Matteo','M','53-01-01','Roma','Divorziato','Laurea L07','Verona','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123470','Rossi','Rosa','F','53-01-01','Verona','Sposata','Laurea L31','Roma','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123471','Bianchi','Gigi','M','53-01-01','Andalucia','Divorziato','Laurea L07','Verona','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);

insert into dipendenti values (null,'123472','Verdi','Marco','M','53-01-01','Torino','Sposato','Laurea L31','Milano','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123473','Gialli','Luigi','M','53-01-01','Verona','Sposato','Laurea L31','Roma','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123474','Arancioni','Matteo','M','53-01-01','Roma','Divorziato','Laurea L07','Verona','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123475','Rosi','Rosa','F','53-01-01','Verona','Sposata','Laurea L31','Roma','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123476','Blu','Gigi','M','53-01-01','Molise','Divorziato','Laurea L07','Verona','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);

insert into dipendenti values (null,'123477','Verdi','Marco','M','53-01-01','Milano','Sposato','Laurea L31','Milano','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123478','Gialli','Luigi','M','53-01-01','Verona','Sposato','Laurea L31','Roma','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123479','Arancioni','Matteo','M','53-01-01','Roma','Divorziato','Laurea L07','Verona','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123480','Rosi','Rosa','F','53-01-01','Verona','Sposata','Laurea L31','Roma','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123481','Blu','Gigi','M','53-01-01','Campobasso','Divorziato','Laurea L07','Verona','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);

insert into dipendenti values (null,'123482','Beige','Marco','M','53-01-01','Milano','Sposato','Laurea L31','Milano','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123483','Bordeaux','Luigi','M','53-01-01','Verona','Sposato','Laurea L31','Roma','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123484','Ciano','Matteo','M','53-01-01','Roma','Divorziato','Laurea L07','Verona','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123485','Rossi','Rosa','F','53-01-01','Verona','Sposata','Laurea L31','Roma','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);
insert into dipendenti values (null,'123486','Bianchi','Gigi','M','53-01-01','Veneto','Divorziato','Laurea L07','Verona','53-01-01','D','1','2','53-01-01',12,'','53-01-01','53-01-01',null);


INSERT INTO tipi (tipo_id, descrizione) VALUES 
(1,'Java'),
(2,'Internet'),
(3,'Client-Server'),
(4,'Programmazione Mainframe'),
(5,'RPG'),
(6,'Cobol'),
(7,'Programmazione di Sistemi EDP'),
(8,'Object-Oriented'),
(9,'Sistema di Gestione Qualità'),
(10,'Privacy'),
(11,'Analisi e Programmazione'),
(12,'Programmazione PC'),
(13,'Project Management'),
(14,'Programmazione'),
(15,'CICS'),
(16,'DB2'),
(17,'Linguaggio C'),
(18,'Data Base'),
(19,'Introduzione all''Informatica'),
(20,'Office'),
(21,'JCL'),
(22,'Marketing'),
(23,'Oracle'),
(24,'Formazione Sicurezza (626/94; 81/2008)'),
(25,'Inglese'),
(26,'Amministrazione/Finanza'),
(27,'Windows'),
(28,'Word'),
(29,'Office'),
(30,'Excel'),
(31,'Easytrieve'),
(32,'Comunicazione'),
(33,'Access'),
(34,'Formazione per Formatori'),
(35,'Tecnica Bancaria'),
(36,'Lotus Notes'),
(37,'Visualage'),
(38,'Websphere'),
(39,'Reti/Elettronica'),
(40,'SAS'),
(41,'UML'),
(42,'ViaSoft'),
(43,'Visual Basic'),
(44,'Sistemi Informativi'),
(45,'Cartografia Vettoriale'),
(46,'Cartografia Raster'),
(47,'Gestione Elettronica Documenti-Modulistica'),
(48,'Territorio e Ambiente'),
(49,'Sistemi Informativi Territoriali'),
(50,'Editoria'),
(51,'VARIE'),
(52,'AS/400'),
(53,'Gestione Progetti'),
(55,'Organizzazione'),
(56,'Funzionale Normativo'),
(57,'Funzionale'),
(58,'Manageriale'),
(60,'Metodologia'),
(61,'Prodotti'),
(62,'Business Object'),
(63,'Sistemi di Business Intelligence'),
(64,'Formazione Apprendisti'),
(66,'Primavera'),
(67,'XML'),
(68,'Cattolica – Servizi in ambito Sinistri'),
(69,'Ambito Assicurativo'),
(70,'Gestione della Sicurezza delle Informazioni (SGSI)'),
(71,'PHP'),
(72,'CMS/E-commerce'),
(73,'Microsoft Azure'),
(75,'Eurotech IoT'),
(79,'Infrastrutture'),
(80,'Infrastrutture'),
(81,'Android'),
(82,'SQL'),
(83,'Risorse Umane'),
(74,'Middleware - Sistemi Distribuiti'),
(76,'Blockchain'),
(77,'Angular'),
(78,'Soft Skill');

INSERT INTO tipologie (tipologia_id, descrizione) VALUES 
(1,'Ingresso'),
(2,'Aggiornamento'),
(3,'Seminario');

INSERT INTO misure (misura_id, descrizione) VALUES 
(1,'Ore'),
(2,'Giorni');

