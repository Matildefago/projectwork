use pj_db;

INSERT INTO tipi (tipo_id, descrizione) VALUES 
(1,'Java'),
(2,'Internet'),
(3,'Client-Server'),
(4,'Programmazione Mainframe'),
(5,'RPG'),
(6,'Cobol'),
(7,'Programmazione di Sistemi EDP'),
(8,'Object-Oriented'),
(9,'Sistema di Gestione Qualità'),
(10,'Privacy'),
(11,'Analisi e Programmazione'),
(12,'Programmazione PC'),
(13,'Project Management'),
(14,'Programmazione'),
(15,'CICS'),
(16,'DB2'),
(17,'Linguaggio C'),
(18,'Data Base'),
(19,'Introduzione all''Informatica'),
(20,'Office'),
(21,'JCL'),
(22,'Marketing'),
(23,'Oracle'),
(24,'Formazione Sicurezza (626/94; 81/2008)'),
(25,'Inglese'),
(26,'Amministrazione/Finanza'),
(27,'Windows'),
(28,'Word'),
(29,'Office'),
(30,'Excel'),
(31,'Easytrieve'),
(32,'Comunicazione'),
(33,'Access'),
(34,'Formazione per Formatori'),
(35,'Tecnica Bancaria'),
(36,'Lotus Notes'),
(37,'Visualage'),
(38,'Websphere'),
(39,'Reti/Elettronica'),
(40,'SAS'),
(41,'UML'),
(42,'ViaSoft'),
(43,'Visual Basic'),
(44,'Sistemi Informativi'),
(45,'Cartografia Vettoriale'),
(46,'Cartografia Raster'),
(47,'Gestione Elettronica Documenti-Modulistica'),
(48,'Territorio e Ambiente'),
(49,'Sistemi Informativi Territoriali'),
(50,'Editoria'),
(51,'VARIE'),
(52,'AS/400'),
(53,'Gestione Progetti'),
(55,'Organizzazione'),
(56,'Funzionale Normativo'),
(57,'Funzionale'),
(58,'Manageriale'),
(60,'Metodologia'),
(61,'Prodotti'),
(62,'Business Object'),
(63,'Sistemi di Business Intelligence'),
(64,'Formazione Apprendisti'),
(66,'Primavera'),
(67,'XML'),
(68,'Cattolica – Servizi in ambito Sinistri'),
(69,'Ambito Assicurativo'),
(70,'Gestione della Sicurezza delle Informazioni (SGSI)'),
(71,'PHP'),
(72,'CMS/E-commerce'),
(73,'Microsoft Azure'),
(75,'Eurotech IoT'),
(79,'Infrastrutture'),
(80,'Infrastrutture'),
(81,'Android'),
(82,'SQL'),
(83,'Risorse Umane'),
(74,'Middleware - Sistemi Distribuiti'),
(76,'Blockchain'),
(77,'Angular'),
(78,'Soft Skill');

INSERT INTO tipologie (tipologia_id, descrizione) VALUES 
(1,'Ingresso'),
(2,'Aggiornamento'),
(3,'Seminario');

INSERT INTO misure (misura_id, descrizione) VALUES 
(1,'Ore'),
(2,'Giorni');

INSERT INTO societa VALUES (null, 'Corvallis','Via Savelli', 'Padova','PD','IT','3512451251','12451242','12345678945');

INSERT into corsi values (null, 1, 2, 2, 'Javascript', 'Settima', 1, 0 , 1, 2, 'Corso Aggiornamento', 'Verona', 'Timmi', '2020-04-03', '2020-04-03', '2020-05-07', 0);
INSERT into corsi values (null, 1, 2, 2, 'Angular', 'Seconda', 1, 0 , 2, 1, 'Seminario', 'Verona', 'Maria', '2020-04-03', '2020-04-03', '2020-05-07', 1);
INSERT into corsi values (null, 2, 2, 2, 'Corso Generico', 'Terza', 1, 0 , 1, 2, 'Corso Java', 'Roma', 'Timmi', '2020-04-03', '2020-04-03', '2020-05-07', 0);
INSERT into corsi values (null, 1, 1, 1, 'SQL', 'Settima', 0, 1 , 2, 2, 'Api', 'Firenze', 'Gino', '2020-04-03', '2020-04-03', '2020-05-07', 0);


INSERT into corsi values (null, 1, 2, 2, 'Training', 'Settima', 1, 0 , 1, 2, 'Corso Aggiornamento', 'Verona', 'Timmi', '2020-04-03', '2020-04-03', '2020-05-07', 1);
INSERT into corsi values (null, 1, 2, 2, 'Conferenza', 'Seconda', 1, 0 , 2, 1, 'Seminario', 'Verona', 'Maria', '2020-04-03', '2020-04-03', '2020-05-07', 0);
INSERT into corsi values (null, 1, 2, 2, 'Php', 'Terza', 1, 0 , 1, 2, 'Corso Java', 'Roma', 'Timmi', '2020-04-03', '2020-04-03', '2020-05-07', 1);
INSERT into corsi values (null, 1, 2, 2, 'C++', 'Settima', 0, 1 , 2, 2, 'Api', 'Bari', 'Linda', '2020-04-03', '2020-04-03', '2020-05-07', 1);

INSERT into corsi values (null, 1, 2, 2, 'Aggiornamento', 'Settima', 1, 0 , 1, 2, 'Corso Aggiornamento', 'Verona', 'Timmi', '2020-04-03', '2020-04-03', '2020-05-07', 1);
INSERT into corsi values (null, 1, 1, 1, 'Seminario', 'Seconda', 1, 0 , 2, 1, 'Seminario', 'Verona', 'Maria', '2020-04-03', '2020-04-03', '2020-05-07', 1);
INSERT into corsi values (null, 1, 2, 2, 'Java', 'Terza', 1, 0 , 1, 2, 'Corso Java', 'Roma', 'Timmi', '2020-04-03', '2020-04-03', '2020-05-07', 0);
INSERT into corsi values (null, 1, 1, 1, 'SpringBoot', 'Settima', 0, 1 , 2, 2, 'Api', 'Firenze', 'Gino', '2020-04-03', '2020-04-03', '2020-05-07', 1);


INSERT into corsi values (null, 1, 1, 1, 'Training', 'Settima', 1, 0 , 1, 2, 'Corso Aggiornamento', 'Verona', 'Timmi', '2020-04-03', '2020-04-03', '2020-05-07', 1);
INSERT into corsi values (null, 1, 2, 2, 'Conferenza', 'Seconda', 1, 0 , 2, 1, 'Seminario', 'Verona', 'Maria', '2020-04-03', '2020-04-03', '2020-05-07', 0);
INSERT into corsi values (null, 1, 2, 2, 'Php', 'Terza', 1, 0 , 1, 2, 'Corso Java', 'Roma', 'Timmi', '2020-04-03', '2020-04-03', '2020-05-07', 1);
INSERT into corsi values (null, 1, 2, 2, 'C++', 'Settima', 0, 1 , 2, 2, 'Api', 'Bari', 'Linda', '2020-04-03', '2020-04-03', '2020-05-07', 1);


insert into docenti values (null,'Prof','Marco Verdi','Via Aquileia','Padova','PD','IT','3451254852','121458215','12345621524','Marco Beige');
insert into docenti values (null,'Prof','Laura Finzi','Via Savoia','Padova','PD','IT','352145124','12354513','1425181211','Francesco Beige');
insert into docenti values (null,'Prof','Camillo Benso','Via Ippica','Milano','MI','IT','3254512452','121458215','12345621524','Paolo Beige');
insert into docenti values (null,'Prof','Claudio Marroni','Via Referente','Padova','PD','IT','3451254852','121458215','12345621524','Pippo Beige');
insert into docenti values (null,'Prof','Vincenza Beige','Via Docker','Padova','PD','IT','3451254852','121458215','12345621524','Marco Beige');
insert into docenti values (null,'Prof','Ciccio Pasticcio','Via Alluminio','Padova','PD','IT','3451254852','121458215','12345621524','Marco Beige');

insert into docenti values (null,'Prof','Marco Verdi','Via Aquileia','Padova','PD','IT','3451254852','121458215','12345621524','Marco Beige');
insert into docenti values (null,'Prof','Laura Finzi','Via Savoia','Padova','PD','IT','352145124','12354513','1425181211','Francesco Beige');
insert into docenti values (null,'Prof','Camillo Benso','Via Ippica','Milano','MI','IT','3254512452','121458215','12345621524','Paolo Beige');
insert into docenti values (null,'Prof','Claudio Marroni','Via Referente','Padova','PD','IT','3451254852','121458215','12345621524','Pippo Beige');
insert into docenti values (null,'Prof','Vincenza Beige','Via Docker','Padova','PD','IT','3451254852','121458215','12345621524','Marco Beige');
insert into docenti values (null,'Prof','Ciccio Pasticcio','Via Alluminio','Padova','PD','IT','3451254852','121458215','12345621524','Marco Beige');




INSERT into dipendenti values(null, '524936', 1, 'Finzi', 'Laura', 'F', '1985-04-01', 'Venezia', 'S', 'Master Economia', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524937', 1, 'Verdi', 'Carlo', 'M', '1985-04-01', 'Padova', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524938', 1, 'Beige', 'Claudio', 'M', '1985-04-01', 'Venezia', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524939', 1, 'Arancioni', 'Nicola', 'M', '1985-04-01', 'Padova', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524940', 1, 'Fixi', 'Andrea', 'M', '1985-04-01', 'Venezia', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524941', 1, 'Marroni', 'Lina', 'F', '1985-04-01', 'Padova', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524942', 1, 'Bianchi', 'Valeria', 'F', '1985-04-01', 'Venezia', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524943', 1, 'Bebe', 'Valentina', 'F', '1985-04-01', 'Padova', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524944', 1, 'Poli', 'Gina', 'F', '1985-04-01', 'Venezia', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524945', 1, 'Fafa', 'Gino', 'M', '1985-04-01', 'Padova', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524946', 1, 'Grigi', 'Paolo', 'M', '1985-04-01', 'Venezia', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524947', 1, 'Fate', 'Papa', 'M', '1985-04-01', 'Padova', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524948', 1, 'Posti', 'Gianluca', 'M', '1985-04-01', 'Venezia', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524949', 1, 'Lini', 'Leria', 'F', '1985-04-01', 'Padova', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524950', 1, 'HAJD', 'Lalla', 'F', '1985-04-01', 'Venezia', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
INSERT into dipendenti values(null, '524951', 1, 'sjs', 'La', 'F', '1985-04-01', 'Padova', 'S', 'Master', 'Ca Foscari', '1998', 'I', '1', '2', '2019-01-02', 2,1,'2025-05-01', '2050-05-04');
