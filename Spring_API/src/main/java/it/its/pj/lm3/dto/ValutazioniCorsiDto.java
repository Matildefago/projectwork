package it.its.pj.lm3.dto;
import it.its.pj.lm3.dao.CorsoDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor

@Data
public class ValutazioniCorsiDto {
	
	private int valutazioniCorsiId;
	private String criterio;
	private String valore; 
	
	//Chiave Esterna
	private CorsoDao corso;
	
}
