package it.its.pj.lm3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.its.pj.lm3.dao.ValutazioniCorsiDao;





public interface ValutazioniCorsiRepository extends JpaRepository<ValutazioniCorsiDao, Integer>{
	
	@Query(nativeQuery = true, value = "select * from valutazioni_corsi inner join corsi on valutazioni_corsi.corso = corsi.corso_id "
			+ "where valutazioni_corsi.corso = :num" )
	List<ValutazioniCorsiDao> infoValCorso(@Param("num") int num);
	
	@Query(nativeQuery = true, value = "select * from valutazioni_corsi inner join corsi ON valutazioni_corsi.corso = corsi.corso_id "
			+ "WHERE valutazioni_corsi.criterio LIKE %:filter% " 
			+ "OR valutazioni_corsi.valore LIKE %:filter% "
			+ "OR corsi.descrizione LIKE %:filter% "
			+ "OR corsi.luogo LIKE %:filter% "
			+ "OR corsi.tipologia LIKE %:filter% "
			+ "OR corsi.ente LIKE %:filter% "
			+ "OR corsi.societa LIKE %:filter% "
			)
	public List<ValutazioniCorsiDao> findByAll(@Param("filter") String filter);
	

}
