package it.its.pj.lm3.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.pj.lm3.dto.BaseResponseDto;
import it.its.pj.lm3.dto.SocietaDto;
import it.its.pj.lm3.services.interfaces.SocietaService;

@RestController
@RequestMapping(value = "api/societa")
public class SocietaController {
	
	@Autowired
	SocietaService societaService;
	
	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<SocietaDto>> selTutti() {

		BaseResponseDto<List<SocietaDto>> response = new BaseResponseDto<>();

		List<SocietaDto> dipendenti = societaService.parseDto();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (dipendenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(dipendenti);
		return response;
	}

}
