package it.its.pj.lm3.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.pj.lm3.dao.CorsiDocentiDao;
import it.its.pj.lm3.dto.BaseResponseDto;
import it.its.pj.lm3.dto.CorsiDocentiDto;
import it.its.pj.lm3.dto.DipendenteDto;
import it.its.pj.lm3.services.interfaces.CorsiDocentiService;

@RestController
@RequestMapping(value = "api/corsiDocenti")
public class CorsiDocentiController {

	@Autowired
	CorsiDocentiService corsiDocentiService;

	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<CorsiDocentiDao>> selTuttiDoc() {

		BaseResponseDto<List<CorsiDocentiDao>> response = new BaseResponseDto<>();

		List<CorsiDocentiDao> corsi = corsiDocentiService.selTuttiDoc();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (corsi.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(corsi);
		return response;
	}
	
	@DeleteMapping(value = "/elimina/{corsiDocentiId}")
	public BaseResponseDto<CorsiDocentiDto> deleteDoc(@PathVariable("corsiDocentiId") int corsiDocentiId) {

		BaseResponseDto<CorsiDocentiDto> response = new BaseResponseDto<>();

		if (!corsiDocentiService.trovaId(corsiDocentiId)) {

			response.setResponse(null);
			response.setMessagge("NON_TROVO_ASSOCIAZIONE");
			return response;
		}

		corsiDocentiService.delete(corsiDocentiId);
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		
		return response;
	}

	@PostMapping(consumes = "application/json", produces = "application/json", value = "/inserisci")
	public BaseResponseDto<CorsiDocentiDto> createDoc(@RequestBody CorsiDocentiDto corsiDocenti) {

		BaseResponseDto<CorsiDocentiDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corsiDocentiService.insCorsiDocenti(corsiDocenti));
		return response;

	}
	
	@GetMapping(produces = "application/json", value = "/docenti/{num}")
	public BaseResponseDto<CorsiDocentiDto> getDocentiByCorso(@PathVariable("num") int num){
		BaseResponseDto<CorsiDocentiDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corsiDocentiService.getDtoDoc(num));
		return response;
	}
	
	@GetMapping(produces = "application/json", value = "/corsiEsterni/{num}")
	public BaseResponseDto<CorsiDocentiDto> getCorsiEsterni(@PathVariable("num") int num){
		BaseResponseDto<CorsiDocentiDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corsiDocentiService.getDtoEsterni(num));
		return response;
	}
	
	@GetMapping(produces = "application/json", value = "/corsiInterni/{num}")
	public BaseResponseDto<CorsiDocentiDto> getCorsiInterni(@PathVariable("num") int num){
		BaseResponseDto<CorsiDocentiDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corsiDocentiService.getDtoInterni(num));
		return response;
	}
	
	@GetMapping(produces = "application/json", value = "/docentiEst/{pagine}..{quantita}")
	public BaseResponseDto<CorsiDocentiDto> docentiEst(@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		 BaseResponseDto<CorsiDocentiDto> response = new BaseResponseDto<>();

			
			response.setTimestamp(new Date());
			response.setStatus(HttpStatus.OK.value());
			response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setResponse(corsiDocentiService.dtoPagesEsterni(pagine, quantita));
			return response;

	}
	
	@GetMapping(produces = "application/json", value = "/pagesDocEst/{pagine}..{quantita}")
	public BaseResponseDto<CorsiDocentiDto> numberEsterni(@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		 BaseResponseDto<CorsiDocentiDto> response = new BaseResponseDto<>();

			
			response.setTimestamp(new Date());
			response.setStatus(HttpStatus.OK.value());
			response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setResponse(corsiDocentiService.getNumberEsterni(pagine, quantita));
			return response;

	}
	
	@GetMapping(produces = "application/json", value = "/docentiInt/{pagine}..{quantita}")
	public BaseResponseDto<CorsiDocentiDto> docentiInterni(@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		 BaseResponseDto<CorsiDocentiDto> response = new BaseResponseDto<>();

			
			response.setTimestamp(new Date());
			response.setStatus(HttpStatus.OK.value());
			response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setResponse(corsiDocentiService.dtoPagesInterni(pagine, quantita));
			return response;

	}
	
	@GetMapping(produces = "application/json", value = "/pagesDocInt/{pagine}..{quantita}")
	public BaseResponseDto<CorsiDocentiDto> numberIntenrni(@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		 BaseResponseDto<CorsiDocentiDto> response = new BaseResponseDto<>();

			
			response.setTimestamp(new Date());
			response.setStatus(HttpStatus.OK.value());
			response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setResponse(corsiDocentiService.getNumberInterni(pagine, quantita));
			return response;

	}

	@PostMapping(produces = "application/json", value ="/cerca/interno/{pagina}..{quantita}")
	public BaseResponseDto<List<DipendenteDto>> cercaDocentiInterni(
			@RequestBody Map<String, String> value,
			@PathVariable("pagina") int pagina,
			@PathVariable("quantita") int quantita
	){
		BaseResponseDto<List<DipendenteDto>> response = new BaseResponseDto<List<DipendenteDto>>();
		
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(this.corsiDocentiService.filtraDocInterni(value.get("cerca"), pagina, quantita));
		
		return response;
	}
	
	@PostMapping(produces = "application/json", value ="/getPages/interno/filtro/{quantita}")
	public BaseResponseDto<Integer> filtraPagesInterni(
			@RequestBody Map<String, String> value,
			@PathVariable("quantita") int quantita
	){
		BaseResponseDto<Integer> response = new BaseResponseDto<Integer>();
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(this.corsiDocentiService.dtoFilterPages(quantita,value.get("cerca")));
		
		
		return response;
		
	}
	
	
	
	
	
	
	
	
}