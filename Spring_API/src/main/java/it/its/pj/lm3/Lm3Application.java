package it.its.pj.lm3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lm3Application {

	public static void main(String[] args) {
		SpringApplication.run(Lm3Application.class, args);
	}

}
