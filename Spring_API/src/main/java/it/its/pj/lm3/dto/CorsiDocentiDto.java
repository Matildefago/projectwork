package it.its.pj.lm3.dto;

import it.its.pj.lm3.dao.CorsoDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CorsiDocentiDto {

	private int corsiDocentiId;
	private int docente;
	private int interno;
	//CHIAVE ESTERNA
	private CorsoDao corso;
	
	
}
