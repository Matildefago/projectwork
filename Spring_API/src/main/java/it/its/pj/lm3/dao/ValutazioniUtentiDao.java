package it.its.pj.lm3.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;
@Entity
@Table(name = "valutazioniUtenti")
@Data

public class ValutazioniUtentiDao {
	
	@Id 
	@Column(name = "valutazioniUtentiId")
	private int valutazioniUtentiId;
	
	@Column(name = "criterio")
	private String criterio;

	@Column(name = "valore")
	private String valore;
	
	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "corso", referencedColumnName = "corsoId")
	@JsonBackReference(value = "corso")
	private CorsoDao corso;

	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "dipendente", referencedColumnName = "dipendenteId")
	@JsonBackReference(value = "dipendente")
	private DipendenteDao dipendente;
	
	

}
