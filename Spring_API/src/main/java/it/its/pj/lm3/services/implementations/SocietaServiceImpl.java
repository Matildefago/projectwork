package it.its.pj.lm3.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.pj.lm3.dao.SocietaDao;
import it.its.pj.lm3.dto.SocietaDto;
import it.its.pj.lm3.repository.SocietaRepository;
import it.its.pj.lm3.services.interfaces.SocietaService;

@Service
@Transactional
public class SocietaServiceImpl implements SocietaService {

	@Autowired
	SocietaRepository societaRepository;

	@Override
	public List<SocietaDao> selTutti() {
		return societaRepository.findAll();
	}

	@Override
	public List<SocietaDto> parseDto() {
		List<SocietaDto> dto = new ArrayList<SocietaDto>();
		List<SocietaDao> dao = this.selTutti();
		for (SocietaDao d : dao) {
			dto.add(new SocietaDto(d.getSocietaId(), d.getRagioneSociale(), d.getIndirizzo(), d.getLocalita(),
					d.getProvincia(), d.getNazione(), d.getTelefono(), d.getFax(), d.getPartitaIva()));
		}
		return dto;
	}
}
