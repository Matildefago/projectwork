package it.its.pj.lm3.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.pj.lm3.dto.BaseResponseDto;
import it.its.pj.lm3.dto.MisuraDto;
import it.its.pj.lm3.services.interfaces.MisuraService;

@RestController
@RequestMapping(value = "api/misure")
public class MisuraController {
	
	@Autowired
	MisuraService misuraService;
	
	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<MisuraDto>> selTutti() {

		BaseResponseDto<List<MisuraDto>> response = new BaseResponseDto<>();

		List<MisuraDto> misure = misuraService.parseDto();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (misure.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(misure);
		return response;
	}

}
