package it.its.pj.lm3.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.its.pj.lm3.dao.DocenteDao;
import it.its.pj.lm3.dto.DocenteDto;
import it.its.pj.lm3.repository.DocenteRepository;
import it.its.pj.lm3.services.interfaces.DocenteService;

@Service
@Transactional
public class DocenteServiceImpl implements DocenteService {

	@Autowired
	DocenteRepository docenteRepository;

	@Override
	public List<DocenteDao> selTuttiPage(int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<DocenteDao> resp = docenteRepository.findAll(p);
//		int pages = resp.getTotalPages(); mai usato
		List<DocenteDao> res = new ArrayList<DocenteDao>();
		for(DocenteDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<DocenteDto> parseDtoPage(int pagina, int quantita) {
		List<DocenteDto> dto = new ArrayList<DocenteDto>();
		List<DocenteDao> dao = this.selTuttiPage(pagina, quantita);
		for (DocenteDao d : dao) {
			dto.add(new DocenteDto(d.getDocenteId(), d.getTitolo(),
					d.getRagioneSociale(), d.getIndirizzo(), d.getLocalita(), d.getProvincia(), d.getNazione(),
					d.getTelefono(), d.getFax(), d.getPartitaIva(), d.getReferente()));
		}
		return dto;
	}
	
	

	@Override
	public void delDoc(int docenteId) {
		docenteRepository.deleteById(docenteId);

	}

	@Override
	public boolean trovaId(int docenteId) {
		boolean trovato = false;
		List<DocenteDto> lista = this.parseDto();
		for (DocenteDto d : lista) {
			if (d.getDocenteId() == docenteId) {
				trovato = true;
				break;
			}
		}
		return trovato;
	}

	@Override
	public boolean insDoc(DocenteDto docente) {
		DocenteDao dao = new DocenteDao();
		dtoToDao(docente, dao);
		try {
			docenteRepository.save(dao);

		} catch (Exception e) {
			return false;
		}

		return true;

	}

	@Override
	public boolean replaceDoc(DocenteDto docente) {
		DocenteDao dao = docenteRepository.getOne(docente.getDocenteId());
		dtoToDao(docente, dao);
		try {
			docenteRepository.save(dao);

		} catch (Exception e) {
			return false;
		}

		return true;

	}

	@Override
	public void dtoToDao(DocenteDto dto, DocenteDao dao) {
		dao.setTitolo(dto.getTitolo() != null ? dto.getTitolo() : dao.getTitolo());
		dao.setRagioneSociale(dto.getRagioneSociale() != null ? dto.getRagioneSociale() : dao.getRagioneSociale());
		dao.setIndirizzo(dto.getIndirizzo() != null ? dto.getIndirizzo() : dao.getIndirizzo());
		dao.setLocalita(dto.getLocalita() != null ? dto.getLocalita() : dao.getLocalita());
		dao.setProvincia(dto.getProvincia() != null ? dto.getProvincia() : dao.getProvincia());
		dao.setNazione(dto.getNazione() != null ? dto.getNazione() : dao.getNazione());
		dao.setTelefono(dto.getTelefono() != null ? dto.getTelefono() : dao.getTelefono());
		dao.setFax(dto.getFax() != null ? dto.getFax() : dao.getFax());
		dao.setPartitaIva(dto.getPartitaIva() != null ? dto.getPartitaIva() : dao.getPartitaIva());
		dao.setReferente(dto.getReferente() != null ? dto.getReferente() : dao.getReferente());

	}

	@Override
	public void daoToDto(DocenteDto dto, DocenteDao dao) {
		dto.setDocenteId(dao.getDocenteId());
		dto.setTitolo(dao.getTitolo());
		dto.setRagioneSociale(dao.getRagioneSociale());
		dto.setIndirizzo(dao.getIndirizzo());
		dto.setLocalita(dao.getLocalita());
		dto.setProvincia(dao.getProvincia());
		dto.setNazione(dao.getNazione());
		dto.setTelefono(dao.getTelefono());
		dto.setFax(dao.getFax());
		dto.setPartitaIva(dao.getPartitaIva());
		dto.setReferente(dao.getReferente());

	}

	@Override
	public DocenteDto selById(int docenteId) {
		try {
			DocenteDto dto = new DocenteDto();
			daoToDto(dto, docenteRepository.getOne(docenteId));
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<DocenteDto> parseDto() {
		List<DocenteDto> dto = new ArrayList<DocenteDto>();
		List<DocenteDao> dao = this.selTutti();
		for (DocenteDao d : dao) {
			dto.add(new DocenteDto(d.getDocenteId(), d.getTitolo(),
					d.getRagioneSociale(), d.getIndirizzo(), d.getLocalita(), d.getProvincia(), d.getNazione(),
					d.getTelefono(), d.getFax(), d.getPartitaIva(), d.getReferente()));
		}
		return dto;
	}

	@Override
	public List<DocenteDao> selTutti() {
		return docenteRepository.findAll();
	}

	@Override
	public int getPages(int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<DocenteDao> resp = docenteRepository.findAll(p);
		int pages = resp.getTotalPages();
		return pages;
	}
	
	@Override
	public List<DocenteDao> gFilter(String filter,int pagine, int quantita) {
		
		Pageable p = PageRequest.of(pagine, quantita);
		Page<DocenteDao> resp = docenteRepository.findByAll(filter, p);
		List<DocenteDao> res = new ArrayList<DocenteDao>();
		for (DocenteDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<DocenteDto> dtoFilter(String filter,int pagine, int quantita) {
		List<DocenteDto> dto = new ArrayList<DocenteDto>();
		List<DocenteDao> dao = this.gFilter(filter,pagine,quantita);
		for (DocenteDao d : dao) {
			dto.add(new DocenteDto(d.getDocenteId(), d.getTitolo(),
					d.getRagioneSociale(), d.getIndirizzo(), d.getLocalita(), d.getProvincia(), d.getNazione(),
					d.getTelefono(), d.getFax(), d.getPartitaIva(), d.getReferente()));
		}
		return dto;
	}

	@Override
	public int getPagesFilter(String filter,int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<DocenteDao> resp = docenteRepository.findByAll(filter, p);
		int pages = resp.getTotalPages();
		return pages;
	}

}
