package it.its.pj.lm3.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.its.pj.lm3.dao.DipendenteDao;
import it.its.pj.lm3.dto.DipendenteDto;
import it.its.pj.lm3.repository.DipendenteRepository;
import it.its.pj.lm3.services.interfaces.DipendenteService;

@Service
@Transactional
public class DipendenteServiceImpl implements DipendenteService{
	
	@Autowired
	DipendenteRepository dipendenteRepository;

	@Override
	public List<DipendenteDao> selTutti() {
		return dipendenteRepository.findAll();
	}
	
	@Override
	public List<DipendenteDao> selAlunni() {
		List<DipendenteDao> risp = new ArrayList<DipendenteDao>();
		List<DipendenteDao> all = selTutti();
		for (DipendenteDao dip : all) {
			if (dip.getTipoDipendente().equals("A") || dip.getTipoDipendente().equals("AD")) {
				risp.add(dip);
			}
		}
		return risp;
	}

	@Override
	public List<DipendenteDto> parseDto() {
		List<DipendenteDto> dto = new ArrayList<DipendenteDto>();
		List<DipendenteDao> dao = this.selTutti();
		for(DipendenteDao d : dao) {
			dto.add(new DipendenteDto(d.getDipendenteId(), d.getMatricola(),
					d.getCognome(), d.getNome(), d.getSesso(), d.getDataNascita(),
					d.getLuogoNascita(), d.getStatoCivile(), d.getConseguitoPresso(), 
					d.getAnnoConsenguimento(), d.getTipoDipendente(),
					d.getQualifica(), d.getLivello(), d.getDataAssunzione(),
					d.getResponsabileRisorsa(), d.getResponsabileArea(),
					d.getDataFineRapporto(), d.getDataScadenzaContratto(), d.getTitoloStudio(), d.getSocieta() ));
		}
		return dto;
	}
	
	@Override
	public List<DipendenteDto> allAlunni() {
		List<DipendenteDto> dto = new ArrayList<DipendenteDto>();
		List<DipendenteDao> dao = this.selAlunni();
		for(DipendenteDao d : dao) {
			dto.add(new DipendenteDto(d.getDipendenteId(), d.getMatricola(),
					d.getCognome(), d.getNome(), d.getSesso(), d.getDataNascita(),
					d.getLuogoNascita(), d.getStatoCivile(), d.getConseguitoPresso(), 
					d.getAnnoConsenguimento(), d.getTipoDipendente(),
					d.getQualifica(), d.getLivello(), d.getDataAssunzione(),
					d.getResponsabileRisorsa(), d.getResponsabileArea(),
					d.getDataFineRapporto(), d.getDataScadenzaContratto(), d.getTitoloStudio(), d.getSocieta() ));
		}
		return dto;
	}

	@Override
	public DipendenteDto selById(int dipendentiId) {
		try {
			DipendenteDto dto = new DipendenteDto();
			daoToDto(dto, dipendenteRepository.getOne(dipendentiId));
			return dto;
		}catch(Exception e) {
			return null;
		}
		
	}

	@Override
	public void daoToDto(DipendenteDto dto, DipendenteDao dao) {
		dto.setDipendenteId(dao.getDipendenteId());
		dto.setMatricola(dao.getMatricola());
		dto.setCognome(dao.getCognome());
		dto.setNome(dao.getNome());
		dto.setSesso(dao.getSesso());
		dto.setDataNascita(dao.getDataNascita());
		dto.setLuogoNascita(dao.getLuogoNascita());
		dto.setStatoCivile(dao.getStatoCivile());
		dto.setConseguitoPresso(dao.getConseguitoPresso());
		dto.setAnnoConsenguimento(dao.getAnnoConsenguimento());
		dto.setTipoDipendente(dao.getTipoDipendente());
		dto.setQualifica(dao.getQualifica());
		dto.setLivello(dao.getLivello());
		dto.setDataAssunzione(dao.getDataAssunzione());
		dto.setResponsabileRisorsa(dao.getResponsabileRisorsa());
		dto.setResponsabileArea(dao.getResponsabileArea());
		dto.setDataFineRapporto(dao.getDataFineRapporto());
		dto.setDataScadenzaContratto(dao.getDataScadenzaContratto());
		dto.setTitoloStudio(dao.getTitoloStudio());
		dto.setSocieta(dao.getSocieta());
		
	}

	@Override
	public List<DipendenteDto> docentiInterni() {
		List<DipendenteDto> tutti = this.parseDto();
		List<DipendenteDto> docenti = new ArrayList<DipendenteDto>();
//		List<DipendenteDto> corsisti = new ArrayList<DipendenteDto>(); mai usato
		for(DipendenteDto d : tutti) {
			if(d.getTipoDipendente().contains("D") || d.getTipoDipendente().contains("d")) {
				docenti.add(d);
			}
		}
		return docenti;
	}

	@Override
	public List<DipendenteDao> selTuttiPage(int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina, quantita);
		Page<DipendenteDao> resp = dipendenteRepository.findAll(p);
//		int pages = resp.getTotalPages(); mai usato
		List<DipendenteDao> res = new ArrayList<DipendenteDao>();
		for(DipendenteDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<DipendenteDto> parseDtoPage(int pagina, int quantita) {
		List<DipendenteDto> dto = new ArrayList<DipendenteDto>();
		List<DipendenteDao> dao = this.selTuttiPage(pagina, quantita);
		for(DipendenteDao d : dao) {
			dto.add(new DipendenteDto(d.getDipendenteId(), d.getMatricola(),
					d.getCognome(), d.getNome(), d.getSesso(), d.getDataNascita(),
					d.getLuogoNascita(), d.getStatoCivile(), d.getConseguitoPresso(), 
					d.getAnnoConsenguimento(), d.getTipoDipendente(),
					d.getQualifica(), d.getLivello(), d.getDataAssunzione(),
					d.getResponsabileRisorsa(), d.getResponsabileArea(),
					d.getDataFineRapporto(), d.getDataScadenzaContratto(), d.getTitoloStudio(), d.getSocieta() ));
		}
		return dto;
	}

	@Override
	public int getPages(int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<DipendenteDao> resp = dipendenteRepository.findAll(p);
		int pages = resp.getTotalPages();
		return pages;
	}

	@Override
	public int getPagesFilter(String filter, int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina, quantita);
		Page<DipendenteDao> resp = dipendenteRepository.findByAll(filter, p);
		int pages = resp.getTotalPages();
		return pages;
		
	}

	@Override
	public List<DipendenteDao> gFilter(String filter, int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina, quantita);
		Page<DipendenteDao> resp = dipendenteRepository.findByAll(filter, p);
//		System.out.println("test: " + resp.toString());
		List<DipendenteDao> res = new ArrayList<DipendenteDao>();
		for (DipendenteDao d : resp) {
			res.add(d);
		}
				
		return res;
	}

	@Override
	public List<DipendenteDto> dtoFilter(String filter, int pagina, int quantita) {
		ArrayList<DipendenteDto> dto = new ArrayList<DipendenteDto>();
		List<DipendenteDao> dao = this.gFilter(filter, pagina, quantita);
		
		for(DipendenteDao d : dao) {
			DipendenteDto temp = new DipendenteDto();
			temp.setDipendenteId(d.getDipendenteId());
			temp.setMatricola(d.getMatricola());
			temp.setCognome(d.getCognome());
			temp.setNome(d.getNome());
			temp.setSesso(d.getSesso());
			temp.setDataNascita(d.getDataNascita());
			temp.setLuogoNascita(d.getLuogoNascita());
			temp.setStatoCivile(d.getStatoCivile());
			temp.setTitoloStudio(d.getTitoloStudio());
			temp.setConseguitoPresso(d.getConseguitoPresso());
			temp.setAnnoConsenguimento(d.getAnnoConsenguimento());
			temp.setTipoDipendente(d.getTipoDipendente());
			temp.setQualifica(d.getQualifica());
			temp.setLivello(d.getLivello());
			temp.setDataAssunzione(d.getDataAssunzione());
			temp.setResponsabileArea(d.getResponsabileArea());
			temp.setResponsabileRisorsa(d.getResponsabileRisorsa());
			temp.setDataScadenzaContratto(d.getDataScadenzaContratto());
			temp.setDataFineRapporto(d.getDataFineRapporto());
			temp.setSocieta(d.getSocieta());
			
			dto.add(temp);
		}
		return dto;
	}

	@Override
	public List<DipendenteDao> doppioni() {
		return dipendenteRepository.doppioni();
	}

	@Override
	public List<DipendenteDto> dtoDoppioni() {
		ArrayList<DipendenteDto> dto = new ArrayList<DipendenteDto>();
		List<DipendenteDao> dao = this.doppioni();
		
		for(DipendenteDao d : dao) {
			DipendenteDto temp = new DipendenteDto();
			temp.setDipendenteId(d.getDipendenteId());
			temp.setMatricola(d.getMatricola());
			temp.setCognome(d.getCognome());
			temp.setNome(d.getNome());
			temp.setSesso(d.getSesso());
			temp.setDataNascita(d.getDataNascita());
			temp.setLuogoNascita(d.getLuogoNascita());
			temp.setStatoCivile(d.getStatoCivile());
			temp.setTitoloStudio(d.getTitoloStudio());
			temp.setConseguitoPresso(d.getConseguitoPresso());
			temp.setAnnoConsenguimento(d.getAnnoConsenguimento());
			temp.setTipoDipendente(d.getTipoDipendente());
			temp.setQualifica(d.getQualifica());
			temp.setLivello(d.getLivello());
			temp.setDataAssunzione(d.getDataAssunzione());
			temp.setResponsabileArea(d.getResponsabileArea());
			temp.setResponsabileRisorsa(d.getResponsabileRisorsa());
			temp.setDataScadenzaContratto(d.getDataScadenzaContratto());
			temp.setDataFineRapporto(d.getDataFineRapporto());
			temp.setSocieta(d.getSocieta());
			
			dto.add(temp);
		}
		return dto;
	}

	@Override
	public List<DipendenteDao> doppPages(int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<DipendenteDao> resp = dipendenteRepository.doppPages(p);
		List<DipendenteDao> res = new ArrayList<DipendenteDao>();
		for(DipendenteDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<DipendenteDto> dtoDoppPages(int pagine, int quantita) {
		ArrayList<DipendenteDto> dto = new ArrayList<DipendenteDto>();
		List<DipendenteDao> dao = this.doppPages(pagine, quantita);
		
		for(DipendenteDao d : dao) {
			DipendenteDto temp = new DipendenteDto();
			temp.setDipendenteId(d.getDipendenteId());
			temp.setMatricola(d.getMatricola());
			temp.setCognome(d.getCognome());
			temp.setNome(d.getNome());
			temp.setSesso(d.getSesso());
			temp.setDataNascita(d.getDataNascita());
			temp.setLuogoNascita(d.getLuogoNascita());
			temp.setStatoCivile(d.getStatoCivile());
			temp.setTitoloStudio(d.getTitoloStudio());
			temp.setConseguitoPresso(d.getConseguitoPresso());
			temp.setAnnoConsenguimento(d.getAnnoConsenguimento());
			temp.setTipoDipendente(d.getTipoDipendente());
			temp.setQualifica(d.getQualifica());
			temp.setLivello(d.getLivello());
			temp.setDataAssunzione(d.getDataAssunzione());
			temp.setResponsabileArea(d.getResponsabileArea());
			temp.setResponsabileRisorsa(d.getResponsabileRisorsa());
			temp.setDataScadenzaContratto(d.getDataScadenzaContratto());
			temp.setDataFineRapporto(d.getDataFineRapporto());
			temp.setSocieta(d.getSocieta());
			
			dto.add(temp);
		}
		return dto;
	}

	@Override
	public int numberDopp(int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<DipendenteDao> resp = dipendenteRepository.doppPages(p);
		int pages = resp.getTotalPages();
		return pages;
	}

	@Override
	public List<DipendenteDao> filterPage(String filter, int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<DipendenteDao> resp = dipendenteRepository.pagesFilterInterni(filter, p);
		List<DipendenteDao> res = new ArrayList<DipendenteDao>();
		for(DipendenteDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<DipendenteDto> dtoFilterPage(String filter, int pagine, int quantita) {
		ArrayList<DipendenteDto> dto = new ArrayList<DipendenteDto>();
		List<DipendenteDao> dao = this.filterPage(filter, pagine, quantita);
		
		for(DipendenteDao d : dao) {
			DipendenteDto temp = new DipendenteDto();
			temp.setDipendenteId(d.getDipendenteId());
			temp.setMatricola(d.getMatricola());
			temp.setCognome(d.getCognome());
			temp.setNome(d.getNome());
			temp.setSesso(d.getSesso());
			temp.setDataNascita(d.getDataNascita());
			temp.setLuogoNascita(d.getLuogoNascita());
			temp.setStatoCivile(d.getStatoCivile());
			temp.setTitoloStudio(d.getTitoloStudio());
			temp.setConseguitoPresso(d.getConseguitoPresso());
			temp.setAnnoConsenguimento(d.getAnnoConsenguimento());
			temp.setTipoDipendente(d.getTipoDipendente());
			temp.setQualifica(d.getQualifica());
			temp.setLivello(d.getLivello());
			temp.setDataAssunzione(d.getDataAssunzione());
			temp.setResponsabileArea(d.getResponsabileArea());
			temp.setResponsabileRisorsa(d.getResponsabileRisorsa());
			temp.setDataScadenzaContratto(d.getDataScadenzaContratto());
			temp.setDataFineRapporto(d.getDataFineRapporto());
			temp.setSocieta(d.getSocieta());
			
			dto.add(temp);
		}
		return dto;
	}

	@Override
	public int numFilter(String filter, int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<DipendenteDao> resp = dipendenteRepository.pagesFilterInterni(filter, p);
		int pages = resp.getTotalPages();
		return pages;
	}
	



}
