package it.its.pj.lm3.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.its.pj.lm3.dao.CorsiDocentiDao;
import it.its.pj.lm3.dao.DipendenteDao;

public interface CorsiDocentiRepository extends JpaRepository<CorsiDocentiDao, Integer>{

	@Query(value = "select * from corsi_docenti inner join corsi on corsi_docenti.corso = corsi.corso_id "
			+ "where corsi_docenti.corso = :num", nativeQuery = true)
	public List<CorsiDocentiDao> getAllDoc(@Param("num") int num);
	
	@Query(value = "select * from corsi_docenti inner join corsi on corsi_docenti.corso = corsi.corso_id "
			+ "where corsi_docenti.docente = :num and corsi_docenti.interno = 0", nativeQuery = true)
	public List<CorsiDocentiDao> getAllCorsiDocEsterni(@Param("num") int num);
	
	@Query(value = "select * from corsi_docenti inner join corsi on corsi_docenti.corso = corsi.corso_id "
			+ "where corsi_docenti.docente = :num and corsi_docenti.interno = 1", nativeQuery = true)
	public List<CorsiDocentiDao> getAllCorsiDocInterni(@Param("num") int num);
	
	
	@Query(value = "select * from corsi_docenti inner join corsi on corsi_docenti.corso = corsi.corso_id "
			+ "where corsi_docenti.interno = 0", nativeQuery = true)
	public Page<CorsiDocentiDao> listaPageEsterni(Pageable p);
	
	@Query(value = "select * from corsi_docenti inner join corsi on corsi_docenti.corso = corsi.corso_id "
			+ "where corsi_docenti.interno = 1", nativeQuery = true)
	public Page<CorsiDocentiDao> listaPageInterni(Pageable p);
	
	@Query(value = "select docente from corsi_docenti where interno = 1", nativeQuery = true)
	public int[] getDocenti();
//	
//	@Query(nativeQuery = true, value = "select * from dipendenti inner join societa on dipendenti.societa = societa.societa_id "
//			+ "inner join corsi_docenti on dipendenti.dipendente_id = corsi_docenti.dipendente "
//			+ "where dipendenti.matricola like %:filter% "
//			+ "or dipendenti.nome like %:filter% "
//			+ "or dipendenti.cognome like %:filter% "
//			+ "or dipendenti.sesso like %:filter% "
//			+ "or dipendenti.data_nascita like %:filter% "
//			+ "or dipendenti.luogo_nascita like %:filter% "
//			+ "or dipendenti.stato_civile like %:filter%"
//			+ " or dipendenti.conseguito_presso like %:filter%"
//			+ " or dipendenti.anno_conseguimento like %:filter%"
//			+ " or dipendenti.tipo_dipendente like %:filter%"
//			+ " or dipendenti.qualifica like %:filter%"
//			+ " or dipendenti.livello like %:filter%"
//			+ " or dipendenti.data_assunzione like %:filter%"
//			+ " or dipendenti.responsabile_risorsa like %:filter%"
//			+ " or dipendenti.responsabile_area like %:filter%"
//			+ " or dipendenti.data_fine_rapporto like %:filter%"
//			+ " or dipendenti.data_scadenza_contratto like %:filter%"
//			+ " or dipendenti.titolo_studio like %:filter%"
//			+ " or societa.ragione_sociale like %:filter%")
//	public Page<DipendenteDao> findByAll(@Param("filter") String filter, Pageable p);
}
