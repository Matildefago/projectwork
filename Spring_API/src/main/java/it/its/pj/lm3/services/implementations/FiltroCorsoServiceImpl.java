package it.its.pj.lm3.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.its.pj.lm3.dao.CorsoDao;
import it.its.pj.lm3.dto.CorsoDto;
import it.its.pj.lm3.repository.FiltroCorsoRepository;
import it.its.pj.lm3.services.interfaces.FiltroCorsoService;

@Service
@Transactional
public class FiltroCorsoServiceImpl implements FiltroCorsoService {

	@Autowired
	FiltroCorsoRepository filtroCorsoRepository;

	@Override
	public List<CorsoDao> gDescrizione(String stringa, int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina, quantita);
		Page<CorsoDao> resp = filtroCorsoRepository.findByDescrizione(stringa, p);
		List<CorsoDao> res = new ArrayList<CorsoDao>();
		for (CorsoDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<CorsoDto> parseDtoDescrizione(String stringa, int pagina, int quantita) {
		List<CorsoDto> dto = new ArrayList<CorsoDto>();
		List<CorsoDao> dao = this.gDescrizione(stringa, pagina, quantita);
		for (CorsoDao d : dao) {
			dto.add(new CorsoDto(d.getCorsoId(), d.getDescrizione(), d.getEdizione(), d.getPrevisto(), d.getErogato(),
					d.getDurata(), d.getNote(), d.getLuogo(), d.getEnte(), d.getDataErogazione(), d.getDataChiusura(),
					d.getDataCensimento(), d.getInterno(), d.getSocieta(), d.getTipo(), d.getTipologia(),
					d.getMisura()));
		}
		return dto;
	}

	@Override
	public int getDescrizionePages(String stringa, int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<CorsoDao> resp = filtroCorsoRepository.findByDescrizione(stringa, p);
		int pages = resp.getTotalPages();
		return pages;
	}

	@Override
	public List<CorsoDao> gLuogo(String stringa, int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina, quantita);
		Page<CorsoDao> resp = filtroCorsoRepository.findByLuogo(stringa, p);
		List<CorsoDao> res = new ArrayList<CorsoDao>();
		for (CorsoDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<CorsoDto> parseDtoLuogo(String stringa, int pagina, int quantita) {
		List<CorsoDto> dto = new ArrayList<CorsoDto>();
		List<CorsoDao> dao = this.gLuogo(stringa, pagina, quantita);
		for (CorsoDao d : dao) {
			dto.add(new CorsoDto(d.getCorsoId(), d.getDescrizione(), d.getEdizione(), d.getPrevisto(), d.getErogato(),
					d.getDurata(), d.getNote(), d.getLuogo(), d.getEnte(), d.getDataErogazione(), d.getDataChiusura(),
					d.getDataCensimento(), d.getInterno(), d.getSocieta(), d.getTipo(), d.getTipologia(),
					d.getMisura()));
		}
		return dto;
	}

	@Override
	public int getLuogoPages(String stringa, int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<CorsoDao> resp = filtroCorsoRepository.findByLuogo(stringa, p);
		int pages = resp.getTotalPages();
		return pages;
	}

	@Override
	public List<CorsoDao> gEnte(String stringa, int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina, quantita);
		Page<CorsoDao> resp = filtroCorsoRepository.findByEnte(stringa, p);
		List<CorsoDao> res = new ArrayList<CorsoDao>();
		for (CorsoDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<CorsoDto> parseDtoEnte(String stringa, int pagina, int quantita) {
		List<CorsoDto> dto = new ArrayList<CorsoDto>();
		List<CorsoDao> dao = this.gEnte(stringa, pagina, quantita);
		for (CorsoDao d : dao) {
			dto.add(new CorsoDto(d.getCorsoId(), d.getDescrizione(), d.getEdizione(), d.getPrevisto(), d.getErogato(),
					d.getDurata(), d.getNote(), d.getLuogo(), d.getEnte(), d.getDataErogazione(), d.getDataChiusura(),
					d.getDataCensimento(), d.getInterno(), d.getSocieta(), d.getTipo(), d.getTipologia(),
					d.getMisura()));
		}
		return dto;
	}

	@Override
	public int getEntePages(String stringa, int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<CorsoDao> resp = filtroCorsoRepository.findByEnte(stringa, p);
		int pages = resp.getTotalPages();
		return pages;
	}

	@Override
	public List<CorsoDao> gTipologia(String stringa, int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina, quantita);
		Page<CorsoDao> resp = filtroCorsoRepository.findByTipologia(stringa, p);
		List<CorsoDao> res = new ArrayList<CorsoDao>();
		for (CorsoDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<CorsoDto> parseDtoTipologia(String stringa, int pagina, int quantita) {
		List<CorsoDto> dto = new ArrayList<CorsoDto>();
		List<CorsoDao> dao = this.gTipologia(stringa, pagina, quantita);
		for (CorsoDao d : dao) {
			dto.add(new CorsoDto(d.getCorsoId(), d.getDescrizione(), d.getEdizione(), d.getPrevisto(), d.getErogato(),
					d.getDurata(), d.getNote(), d.getLuogo(), d.getEnte(), d.getDataErogazione(), d.getDataChiusura(),
					d.getDataCensimento(), d.getInterno(), d.getSocieta(), d.getTipo(), d.getTipologia(),
					d.getMisura()));
		}
		return dto;
	}

	@Override
	public int getTipologiaPages(String stringa, int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<CorsoDao> resp = filtroCorsoRepository.findByTipologia(stringa, p);
		int pages = resp.getTotalPages();
		return pages;
	}

	@Override
	public List<CorsoDao> gSocieta(String stringa, int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina, quantita);
		Page<CorsoDao> resp = filtroCorsoRepository.findBySocieta(stringa, p);
		List<CorsoDao> res = new ArrayList<CorsoDao>();
		for (CorsoDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<CorsoDto> parseDtoSocieta(String stringa, int pagina, int quantita) {
		List<CorsoDto> dto = new ArrayList<CorsoDto>();
		List<CorsoDao> dao = this.gSocieta(stringa, pagina, quantita);
		for (CorsoDao d : dao) {
			dto.add(new CorsoDto(d.getCorsoId(), d.getDescrizione(), d.getEdizione(), d.getPrevisto(), d.getErogato(),
					d.getDurata(), d.getNote(), d.getLuogo(), d.getEnte(), d.getDataErogazione(), d.getDataChiusura(),
					d.getDataCensimento(), d.getInterno(), d.getSocieta(), d.getTipo(), d.getTipologia(),
					d.getMisura()));
		}
		return dto;
	}

	@Override
	public int getSocietaPages(String stringa, int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<CorsoDao> resp = filtroCorsoRepository.findBySocieta(stringa, p);
		int pages = resp.getTotalPages();
		return pages;
	}
}
