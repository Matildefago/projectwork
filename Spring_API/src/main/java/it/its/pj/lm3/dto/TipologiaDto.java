package it.its.pj.lm3.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TipologiaDto {
	
	private int tipologiaId;
	
	private String descrizione;

}
