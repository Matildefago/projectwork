package it.its.pj.lm3.services.interfaces;

import java.util.List;

import it.its.pj.lm3.dao.CorsiUtentiDao;
import it.its.pj.lm3.dto.CorsiUtentiDto;

public interface CorsiUtentiService {
	
	public List<CorsiUtentiDao> selTuttiUte();
	
	public List<CorsiUtentiDto> dtoUte();
	
	public boolean insCorsiUtenti(CorsiUtentiDto corsiUtenti);

	public void dtoToDao(CorsiUtentiDto dto, CorsiUtentiDao dao);
	
    public List<CorsiUtentiDao> selDip(int num, int pagina, int quantita);
	
    public List<CorsiUtentiDto> dipDto(int num, int pagina, int quantita);
	
	public int dipPages(int num, int pagina, int quantita);
	
	public List<CorsiUtentiDao> getAllDip(int num);
	
	public List<CorsiUtentiDto> getAllDto(int num);
	
	public void deleteAss(int n);
	
	public boolean trovaId(int n, int numero);
	
	public List<CorsiUtentiDao> getAllCorsi(int num);
	
	public List<CorsiUtentiDto> getDtoCorsi(int num);
	
    public List<CorsiUtentiDao> selTuttiUtePage(int pagine, int quantita);
	
	public List<CorsiUtentiDto> dtoUtePage(int pagine, int quantita);
	
	public int pagesAll(int pagine, int quantita);
	
	public List<CorsiUtentiDao> filter(int pagine, int quantita, String filter);
	
	public List<CorsiUtentiDto> dtoFilter(int pagine, int quantita, String filter);
	
	public int dtoFilterPages(int pagine, int quantita, String filter);

	public boolean isId(int corsoId);

	public List<CorsiUtentiDto> parseDto();

	public boolean replaceCorso(CorsiUtentiDto corso);
}
