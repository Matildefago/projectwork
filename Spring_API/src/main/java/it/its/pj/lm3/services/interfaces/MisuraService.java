package it.its.pj.lm3.services.interfaces;

import java.util.List;

import it.its.pj.lm3.dao.MisuraDao;
import it.its.pj.lm3.dto.MisuraDto;

public interface MisuraService {
	
    public List<MisuraDto> parseDto();
	
	public List<MisuraDao> selTutti();

}
