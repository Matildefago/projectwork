package it.its.pj.lm3.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.pj.lm3.dto.BaseResponseDto;
import it.its.pj.lm3.dto.CorsoDto;
import it.its.pj.lm3.services.interfaces.CorsoService;

@RestController
@RequestMapping(value = "api/corsi")
public class CorsoController {

	@Autowired
	CorsoService corsoService;

	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<CorsoDto>> selTutti() {

		BaseResponseDto<List<CorsoDto>> response = new BaseResponseDto<>();

		List<CorsoDto> corsi = corsoService.parseDto();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (corsi.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(corsi);
		return response;
	}

	@GetMapping(value = "/{pagine}...{quantita}", produces = "application/json")
	public BaseResponseDto<List<CorsoDto>> selTuttiPage(@PathVariable("pagine") int pagine,
			@PathVariable("quantita") int quantita) {

		BaseResponseDto<List<CorsoDto>> response = new BaseResponseDto<>();

		List<CorsoDto> corsi = corsoService.parseDtoPage(pagine, quantita);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (corsi.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(corsi);
		return response;
	}

	@GetMapping(value = "/getPages/{pagine}...{quantita}", produces = "application/json")
	public BaseResponseDto<Integer> getPages(@PathVariable("pagine") int pagine,
			@PathVariable("quantita") int quantita) {

		BaseResponseDto<Integer> response = new BaseResponseDto<>();

		Integer numero = corsoService.getPages(pagine, quantita);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (numero == 0) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(numero);
		return response;
	}

	@GetMapping(value = "/id/{corsoId}")
	public BaseResponseDto<CorsoDto> selById(@PathVariable("corsoId") int corsoId)

	{
		BaseResponseDto<CorsoDto> response = new BaseResponseDto<>();

		CorsoDto corso = corsoService.selById(corsoId);

		if (corso == null) {
			response.setResponse(null);
			response.setMessagge("NON_TROVO_IL_CORSO");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corso);
		return response;

	}

	@DeleteMapping(value = "/elimina/{corsoId}")
	public BaseResponseDto<CorsoDto> deleteDoc(@PathVariable("corsoId") int corsoId) {

		BaseResponseDto<CorsoDto> response = new BaseResponseDto<>();

		if (!corsoService.trovaId(corsoId)) {

			response.setResponse(null);
			response.setMessagge("NON_TROVO_IL_DOCENTE");
			response.setResponse(corsoId);
			return response;
		}

		corsoService.delCorso(corsoId);
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corsoId);

		return response;
	}

	@PostMapping(consumes = "application/json", produces = "application/json", value = "/inserisci")
	public BaseResponseDto<CorsoDto> createDoc(@RequestBody CorsoDto corso) {

		BaseResponseDto<CorsoDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corsoService.insCorso(corso));
		return response;

	}

	@PutMapping(consumes = "application/json", produces = "application/json", value = "/modifica")
	public BaseResponseDto<CorsoDto> updateDoc(@RequestBody CorsoDto corso) {

		BaseResponseDto<CorsoDto> response = new BaseResponseDto<>();

		if (corsoService.trovaId(corso.getCorsoId())) {
			corsoService.replaceCorso(corso);
			response.setTimestamp(new Date());
			response.setStatus(HttpStatus.OK.value());
			response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setResponse(corso);
			return response;
		} else {
			response.setTimestamp(new Date());
			response.setMessagge("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
			response.setResponse(corso);
			return response;
		}
	}
	

	@PostMapping(value = "/cerca/{pagine}...{quantita}", produces = "application/json")
	public BaseResponseDto<ArrayList<CorsoDto>> filtra(
			@RequestBody Map<String, String> value,
			@PathVariable("pagine") int pagine,
			@PathVariable("quantita") int quantita
	){
		BaseResponseDto<ArrayList<CorsoDto>> response = new BaseResponseDto<ArrayList<CorsoDto>>();
		
		response.setResponse(this.corsoService.dtoFilter(value.get("cerca"), pagine, quantita));
		
		return response;
	}
	@PostMapping(value = "/getPagesFilter/{pagine}...{quantita}" , produces = "application/json")
	public BaseResponseDto<Integer> getPages(@RequestBody Map<String,String> corso, @PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<Integer> response = new BaseResponseDto<>();

		Integer numero = corsoService.getPagesFilter(corso.get("cerca"), pagine, quantita);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (numero == 0) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(numero);
		return response;
	}
	

}
