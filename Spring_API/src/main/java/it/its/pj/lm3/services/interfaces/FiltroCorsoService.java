package it.its.pj.lm3.services.interfaces;

import java.util.List;

import it.its.pj.lm3.dao.CorsoDao;
import it.its.pj.lm3.dto.CorsoDto;

public interface FiltroCorsoService {

	public List<CorsoDao> gDescrizione(String stringa, int pagina, int quantita);

	public List<CorsoDto> parseDtoDescrizione(String stringa, int pagina, int quantita);
	
	public int getDescrizionePages(String stringa, int pagine, int quantita);
	
	public List<CorsoDao> gLuogo(String stringa, int pagina, int quantita);

	public List<CorsoDto> parseDtoLuogo(String stringa, int pagina, int quantita);
	
	public int getLuogoPages(String stringa, int pagine, int quantita);
	
	public List<CorsoDao> gEnte(String stringa, int pagina, int quantita);

	public List<CorsoDto> parseDtoEnte(String stringa, int pagina, int quantita);
	
	public int getEntePages(String stringa, int pagine, int quantita);
	
	public List<CorsoDao> gTipologia(String stringa, int pagina, int quantita);

	public List<CorsoDto> parseDtoTipologia(String stringa, int pagina, int quantita);
	
	public int getTipologiaPages(String stringa, int pagine, int quantita);
	
	public List<CorsoDao> gSocieta(String stringa, int pagina, int quantita);

	public List<CorsoDto> parseDtoSocieta(String stringa, int pagina, int quantita);
	
	public int getSocietaPages(String stringa, int pagine, int quantita);
}
