package it.its.pj.lm3.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.pj.lm3.dao.TipoDao;
import it.its.pj.lm3.dto.TipoDto;
import it.its.pj.lm3.repository.TipoRepository;
import it.its.pj.lm3.services.interfaces.TipoService;

@Service
@Transactional
public class TipoServiceImpl implements TipoService{
	
	@Autowired
	TipoRepository tipoRepository;

	@Override
	public List<TipoDto> parseDto() {
		List<TipoDto> dto = new ArrayList<TipoDto>();
		List<TipoDao> dao = this.selTutti();
		for (TipoDao d : dao) {
			dto.add(new TipoDto(d.getTipoId(), d.getDescrizione()));
		}
		return dto;
	}

	@Override
	public List<TipoDao> selTutti() {
		return tipoRepository.findAll();
	}

}
