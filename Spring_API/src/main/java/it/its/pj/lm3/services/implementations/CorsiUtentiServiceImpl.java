package it.its.pj.lm3.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.its.pj.lm3.dao.CorsiUtentiDao;
import it.its.pj.lm3.dto.CorsiUtentiDto;
import it.its.pj.lm3.repository.CorsiUtentiRepository;
import it.its.pj.lm3.services.interfaces.CorsiUtentiService;

@Service
@Transactional
public class CorsiUtentiServiceImpl implements CorsiUtentiService {

	@Autowired
	CorsiUtentiRepository corsiUtentiRepository;

	@Override
	public List<CorsiUtentiDao> selTuttiUte() {
		List<CorsiUtentiDao> resp = corsiUtentiRepository.findAll();
		List<CorsiUtentiDao> res = new ArrayList<CorsiUtentiDao>();
		for (CorsiUtentiDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public boolean insCorsiUtenti(CorsiUtentiDto corsiUtenti) {
		CorsiUtentiDao dao = new CorsiUtentiDao();
		dtoToDao(corsiUtenti, dao);
		try {
			corsiUtentiRepository.save(dao);

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	@Override
	public void dtoToDao(CorsiUtentiDto dto, CorsiUtentiDao dao) {
		dao.setDurata(dto.getDurata() != 0 ? dto.getDurata() : dao.getDurata());
		dao.setCorso(dto.getCorso() != null ? dto.getCorso() : dao.getCorso());
		dao.setDipendente(dto.getDipendente() != null ? dto.getDipendente() : dao.getDipendente());
	}
	
	@Override
	public List<CorsiUtentiDao> selDip(int num, int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina, quantita);
		Page<CorsiUtentiDao> resp = corsiUtentiRepository.finDip(num, p);
		List<CorsiUtentiDao> res = new ArrayList<CorsiUtentiDao>();
		for (CorsiUtentiDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<CorsiUtentiDto> dipDto(int num, int pagina, int quantita) {
		List<CorsiUtentiDto> dto = new ArrayList<CorsiUtentiDto>();
		List<CorsiUtentiDao> dao = this.selDip(num,pagina, quantita);
		for(CorsiUtentiDao d : dao) {
			dto.add(new CorsiUtentiDto(d.getCorsiUtentiId(), d.getDurata(), d.getCorso(), d.getDipendente()));
		}
		return dto;
	}

	@Override
	public int dipPages(int num, int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina, quantita);
		Page<CorsiUtentiDao> resp = corsiUtentiRepository.finDip(num, p);
		int pages = resp.getTotalPages();
		return pages;
	}

	@Override
	public List<CorsiUtentiDao> getAllDip(int num) {
		return corsiUtentiRepository.getAll(num);
	}

	@Override
	public List<CorsiUtentiDto> getAllDto(int num) {
		List<CorsiUtentiDto> dto = new ArrayList<CorsiUtentiDto>();
		List<CorsiUtentiDao> dao = this.getAllDip(num);
		for(CorsiUtentiDao d : dao) {
			dto.add(new CorsiUtentiDto(d.getCorsiUtentiId(), d.getDurata(), d.getCorso(), d.getDipendente()));
		}
		return dto;
	}

	@Override
	public void deleteAss(int n) {
		corsiUtentiRepository.deleteById(n);
		
	}
	
	@Override
	public boolean trovaId(int id, int numero) {
		boolean trovato = false;
		List<CorsiUtentiDto> lista = this.getAllDto(numero);
		for (CorsiUtentiDto d : lista) {
			if (d.getCorsiUtentiId() == id) {
				trovato = true;
				break;
			}
		}
		return trovato;
	}
	
	@Override
	public boolean isId(int corsoId) {
		boolean trovato = false;
		List<CorsiUtentiDto> lista = this.parseDto();
		for (CorsiUtentiDto d : lista) {
			if (d.getCorsiUtentiId() == corsoId) {
				trovato = true;
				break;
			}
		}
		return trovato;
	}

	@Override
	public List<CorsiUtentiDto> parseDto() {
		
			List<CorsiUtentiDto> dto = new ArrayList<CorsiUtentiDto>();
			List<CorsiUtentiDao> dao = this.selTuttiUte();
			for (CorsiUtentiDao d : dao) {
				dto.add(new CorsiUtentiDto(d.getCorsiUtentiId(),d.getDurata(),d.getCorso(),d.getDipendente()));
			}
			return dto;
		
	}
	
	@Override
	public boolean replaceCorso(CorsiUtentiDto corso) {
		CorsiUtentiDao dao = corsiUtentiRepository.getOne(corso.getCorsiUtentiId());
		dtoToDao(corso, dao);
		try {
			corsiUtentiRepository.save(dao);

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	@Override
	public List<CorsiUtentiDao> getAllCorsi(int num) {
		return corsiUtentiRepository.getAllCorsi(num);
	}

	@Override
	public List<CorsiUtentiDto> getDtoCorsi(int num) {
		List<CorsiUtentiDto> dto = new ArrayList<CorsiUtentiDto>();
		List<CorsiUtentiDao> dao = this.getAllCorsi(num);
		for(CorsiUtentiDao d : dao) {
			dto.add(new CorsiUtentiDto(d.getCorsiUtentiId(), d.getDurata(), d.getCorso(), d.getDipendente()));
		}
		return dto;
	}

	@Override
	public List<CorsiUtentiDto> dtoUte() {
		List<CorsiUtentiDto> dto = new ArrayList<CorsiUtentiDto>();
		List<CorsiUtentiDao> dao = this.selTuttiUte();
		for(CorsiUtentiDao d : dao) {
			dto.add(new CorsiUtentiDto(d.getCorsiUtentiId(), d.getDurata(), d.getCorso(), d.getDipendente()));
		}
		return dto;
	}

	@Override
	public List<CorsiUtentiDao> selTuttiUtePage(int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<CorsiUtentiDao> resp = corsiUtentiRepository.findAll(p);
		List<CorsiUtentiDao> res = new ArrayList<CorsiUtentiDao>();
		for (CorsiUtentiDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<CorsiUtentiDto> dtoUtePage(int pagine, int quantita) {
		List<CorsiUtentiDto> dto = new ArrayList<CorsiUtentiDto>();
		List<CorsiUtentiDao> dao = this.selTuttiUtePage(pagine, quantita);
		for(CorsiUtentiDao d : dao) {
			dto.add(new CorsiUtentiDto(d.getCorsiUtentiId(), d.getDurata(), d.getCorso(), d.getDipendente()));
		}
		return dto;
	}

	@Override
	public int pagesAll(int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<CorsiUtentiDao> resp = corsiUtentiRepository.findAll(p);
		int pages = resp.getTotalPages();
		return pages;
	}

	@Override
	public List<CorsiUtentiDao> filter(int pagine, int quantita, String filter) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<CorsiUtentiDao> resp = corsiUtentiRepository.filter(filter, p);
		List<CorsiUtentiDao> res = new ArrayList<CorsiUtentiDao>();
		for (CorsiUtentiDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<CorsiUtentiDto> dtoFilter(int pagine, int quantita, String filter) {
		List<CorsiUtentiDto> dto = new ArrayList<CorsiUtentiDto>();
		List<CorsiUtentiDao> dao = this.filter(pagine, quantita, filter);
		for(CorsiUtentiDao d : dao) {
			dto.add(new CorsiUtentiDto(d.getCorsiUtentiId(), d.getDurata(), d.getCorso(), d.getDipendente()));
		}
		return dto;
	}

	@Override
	public int dtoFilterPages(int pagine, int quantita, String filter) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<CorsiUtentiDao> resp = corsiUtentiRepository.filter(filter,p);
		int pages = resp.getTotalPages();
		return pages;
	}


}