package it.its.pj.lm3.dto;

import java.util.Date;

import it.its.pj.lm3.dao.SocietaDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DipendenteDto {

	private int dipendenteId;
	private int matricola;
	private String cognome;
	private String nome;
	private String sesso;
	private Date dataNascita;
	private String luogoNascita;
	private String statoCivile;
	private String conseguitoPresso;
	private int annoConsenguimento;
	private String tipoDipendente;
	private String qualifica;
	private String livello;
	private Date dataAssunzione;
	private int responsabileRisorsa;
	private int responsabileArea;
	private Date dataFineRapporto;
	private Date dataScadenzaContratto;
	private String titoloStudio;
	//CHIAVE ESTERNA
	private SocietaDao societa;
	
	
}
