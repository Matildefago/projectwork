package it.its.pj.lm3.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.pj.lm3.dto.BaseResponseDto;
import it.its.pj.lm3.dto.TipologiaDto;
import it.its.pj.lm3.services.interfaces.TipologiaService;

@RestController
@RequestMapping(value = "api/tipologie")
public class TipologiaController {
	
	@Autowired
	TipologiaService tipologiaService;
	
	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<TipologiaDto>> selTutti() {

		BaseResponseDto<List<TipologiaDto>> response = new BaseResponseDto<>();

		List<TipologiaDto> tipologie = tipologiaService.parseDto();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (tipologie.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(tipologie);
		return response;
	}

}