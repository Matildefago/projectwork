package it.its.pj.lm3.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.pj.lm3.dto.BaseResponseDto;
import it.its.pj.lm3.dto.TipoDto;
import it.its.pj.lm3.services.interfaces.TipoService;

@RestController
@RequestMapping(value = "api/tipi")
public class TipoController {
	
	@Autowired
	TipoService tipoService;
	
	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<TipoDto>> selTutti() {

		BaseResponseDto<List<TipoDto>> response = new BaseResponseDto<>();

		List<TipoDto> tipi = tipoService.parseDto();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (tipi.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(tipi);
		return response;
	}

}
