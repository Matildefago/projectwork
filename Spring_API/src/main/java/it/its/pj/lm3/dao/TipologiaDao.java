package it.its.pj.lm3.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "tipologie")
@Data
public class TipologiaDao {
	
	@Id
	@Column(name = "tipologiaId")
	private int tipologiaId;
	
	@Column(name = "descrizione")
	private String descrizione;

}
