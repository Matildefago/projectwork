package it.its.pj.lm3.services.interfaces;

import java.util.List;

import it.its.pj.lm3.dao.CorsoDao;
import it.its.pj.lm3.dto.CorsoDto;

public interface CorsoService {

	public List<CorsoDao> selTuttiPage(int pagina, int quantita);

	public List<CorsoDto> parseDtoPage(int pagina, int quantita);

	public CorsoDto selById(int corsoId);

	public void delCorso(int corsoId);

	public boolean insCorso(CorsoDto corso);

	public boolean trovaId(int corsoId);

	public void dtoToDao(CorsoDto dto, CorsoDao dao);

	public void daoToDto(CorsoDto dto, CorsoDao dao);

	public boolean replaceCorso(CorsoDto corso);

	public List<CorsoDto> parseDto();

	public List<CorsoDao> selTutti();

	public int getPages(int pagine, int quantita);

	public List<CorsoDao> gFilter(String filter, int pagina, int quantita);
	
	public int getPagesFilter(String filter,int pagine, int quantita);
	
	public List<CorsoDto> dtoFilter(String filter, int pagina, int quantita);

}
