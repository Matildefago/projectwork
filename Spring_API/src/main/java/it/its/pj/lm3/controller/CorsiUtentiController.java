package it.its.pj.lm3.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.pj.lm3.dao.CorsiUtentiDao;
import it.its.pj.lm3.dto.BaseResponseDto;
import it.its.pj.lm3.dto.CorsiUtentiDto;
import it.its.pj.lm3.services.interfaces.CorsiUtentiService;

@RestController
@RequestMapping(value = "api/corsiUtenti")
public class CorsiUtentiController {

	@Autowired
	CorsiUtentiService corsiUtentiService;

	@GetMapping(produces = "application/json", value = "/Ute")
	public BaseResponseDto<List<CorsiUtentiDto>> selTuttiUte() {

		BaseResponseDto<List<CorsiUtentiDto>> response = new BaseResponseDto<>();

		List<CorsiUtentiDto> corsi = corsiUtentiService.dtoUte();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (corsi.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(corsi);
		return response;
	}

	@PostMapping(consumes = { "application/json" }, produces = { "application/json" }, value = "/inserisci")
	public BaseResponseDto<CorsiUtentiDto> createUte(@RequestBody CorsiUtentiDto corsiUtenti) {

		BaseResponseDto<CorsiUtentiDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corsiUtentiService.insCorsiUtenti(corsiUtenti));
		return response;

	}

	@GetMapping(produces = "application/json", value = "/corso/{num}")
	public BaseResponseDto<CorsiUtentiDto> getAllByCorso(@PathVariable int num) {

		BaseResponseDto<CorsiUtentiDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corsiUtentiService.getAllDto(num));
		return response;

	}

	@DeleteMapping(value = "/elimina/{corsiUtentiId}..{num}")
	public BaseResponseDto<CorsiUtentiDto> deleteDoc(@PathVariable("corsiUtentiId") int corsiUtentiId,
			@PathVariable("num") int num) {

		BaseResponseDto<CorsiUtentiDto> response = new BaseResponseDto<>();

		if (!corsiUtentiService.trovaId(corsiUtentiId, num)) {

			response.setResponse(null);
			response.setMessagge("NON_TROVO_ASSOCIAZIONE");
			response.setResponse(corsiUtentiId);
			return response;
		}

		corsiUtentiService.deleteAss(corsiUtentiId);
		;
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corsiUtentiId);

		return response;
	}

	@GetMapping(produces = "application/json", value = "/dipendente/{num}")
	public BaseResponseDto<CorsiUtentiDto> getAllByDip(@PathVariable int num) {

		BaseResponseDto<CorsiUtentiDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corsiUtentiService.getDtoCorsi(num));
		return response;

	}

	@GetMapping(produces = "application/json", value = "/utenti/{pagine}..{quantita}")
	public BaseResponseDto<CorsiUtentiDto> utenti(@PathVariable("pagine") int pagine,
			@PathVariable("quantita") int quantita) {

		BaseResponseDto<CorsiUtentiDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corsiUtentiService.dtoUtePage(pagine, quantita));
		return response;

	}

	@GetMapping(produces = "application/json", value = "/pagesUte/{pagine}..{quantita}")
	public BaseResponseDto<CorsiUtentiDao> getPagesUtente(@PathVariable("pagine") int pagine,
			@PathVariable("quantita") int quantita) {

		BaseResponseDto<CorsiUtentiDao> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corsiUtentiService.pagesAll(pagine, quantita));
		return response;

	}

	@PostMapping(value = "/cerca/{pagina}...{quantita}", produces = "application/json")
	public BaseResponseDto<ArrayList<CorsiUtentiDto>> filtra(@RequestBody Map<String, String> value,
			@PathVariable("pagina") int pagina, @PathVariable("quantita") int quantita) {
		BaseResponseDto<ArrayList<CorsiUtentiDto>> response = new BaseResponseDto<ArrayList<CorsiUtentiDto>>();

		response.setResponse(this.corsiUtentiService.dtoFilter(pagina, quantita, value.get("cerca")));

		return response;
	}

	@PostMapping(value = "/cercaPagine/{pagina}...{quantita}", produces = "application/json")
	public BaseResponseDto<Integer> pagineFiltra(@RequestBody Map<String, String> value,
			@PathVariable("pagina") int pagina, @PathVariable("quantita") int quantita) {
		BaseResponseDto<Integer> response = new BaseResponseDto<>();

		Integer numero = corsiUtentiService.dtoFilterPages(pagina, quantita, value.get("cerca"));

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (numero == 0) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(numero);
		return response;
	}

	@GetMapping(produces = "application/json", value = "/dipendenti/{num}..{pagine}..{quantita}")
	public BaseResponseDto<CorsiUtentiDao> cercaTipologia(@PathVariable int num, @PathVariable("pagine") int pagine,
			@PathVariable("quantita") int quantita) {

		BaseResponseDto<CorsiUtentiDao> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corsiUtentiService.dipDto(num, pagine, quantita));
		return response;

	}

	@GetMapping(produces = "application/json", value = "/pages/{num}..{pagine}..{quantita}")
	public BaseResponseDto<CorsiUtentiDao> getPagesTipologia(@PathVariable int num, @PathVariable("pagine") int pagine,
			@PathVariable("quantita") int quantita) {

		BaseResponseDto<CorsiUtentiDao> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(corsiUtentiService.dipPages(num, pagine, quantita));
		return response;

	}
	
	@PutMapping(consumes = "application/json", produces = "application/json", value = "/modifica")
	public BaseResponseDto<CorsiUtentiDto> updateCorUte(@RequestBody CorsiUtentiDto corsoUtenti) {

		BaseResponseDto<CorsiUtentiDto> response = new BaseResponseDto<>();

		if (corsiUtentiService.isId(corsoUtenti.getCorsiUtentiId())) {
			corsiUtentiService.replaceCorso(corsoUtenti);
			response.setTimestamp(new Date());
			response.setStatus(HttpStatus.OK.value());
			response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setResponse(corsoUtenti);
			return response;
		} else {
			response.setTimestamp(new Date());
			response.setMessagge("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
			response.setResponse(corsoUtenti);
			return response;
		}
	}

}