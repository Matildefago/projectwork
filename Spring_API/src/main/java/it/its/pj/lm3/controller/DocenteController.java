package it.its.pj.lm3.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.pj.lm3.dto.BaseResponseDto;
import it.its.pj.lm3.dto.DocenteDto;
import it.its.pj.lm3.services.interfaces.DocenteService;

@RestController
@RequestMapping(value = "api/docenti")
public class DocenteController {

	@Autowired
	private DocenteService docenteService;
	
	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<DocenteDto>> selTutti() {

		BaseResponseDto<List<DocenteDto>> response = new BaseResponseDto<>();

		List<DocenteDto> docenti = docenteService.parseDto();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (docenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(docenti);
		return response;
	}

	@GetMapping(value = "/{pagine}...{quantita}" , produces = "application/json")
	public BaseResponseDto<List<DocenteDto>> selTuttiPage(@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<List<DocenteDto>> response = new BaseResponseDto<>();

		List<DocenteDto> docenti = docenteService.parseDtoPage(pagine, quantita);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (docenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(docenti);
		return response;
	}
	
	@GetMapping(value = "/getPages/{pagine}...{quantita}" , produces = "application/json")
	public BaseResponseDto<Integer> getPages(@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<Integer> response = new BaseResponseDto<>();

		Integer numero = docenteService.getPages(pagine, quantita);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (numero == 0) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(numero);
		return response;
	}

	@GetMapping(value = "/id/{docenteId}")
	public BaseResponseDto<DocenteDto> selById(@PathVariable("docenteId") int docenteId)

	{
		BaseResponseDto<DocenteDto> response = new BaseResponseDto<>();

		DocenteDto docente = docenteService.selById(docenteId);

		if (docente == null) {
			response.setResponse(null);
			response.setMessagge("NON_TROVO_IL_DIPENDENTE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(docente);
		return response;

	}

	@DeleteMapping(value = "/elimina/{docenteId}")
	public BaseResponseDto<DocenteDto> deleteDoc(@PathVariable("docenteId") int docenteId) {

		BaseResponseDto<DocenteDto> response = new BaseResponseDto<>();

		if (!docenteService.trovaId(docenteId)) {

			response.setResponse(null);
			response.setMessagge("NON_TROVO_IL_DOCENTE");
			response.setResponse(docenteId);
			return response;
		}

		docenteService.delDoc(docenteId);
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(docenteId);
		
		return response;
	}

	@PostMapping(consumes = "application/json", produces = "application/json", value = "/inserisci")
	public BaseResponseDto<DocenteDto> createDoc(@RequestBody DocenteDto docente) {

		BaseResponseDto<DocenteDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(docenteService.insDoc(docente));
		return response;

	}

	@PutMapping(consumes = "application/json", produces = "application/json", value = "/modifica")
	public BaseResponseDto<DocenteDto> updateDoc(@RequestBody DocenteDto docente) {

		 BaseResponseDto<DocenteDto> response = new BaseResponseDto<>();

		if (docenteService.trovaId(docente.getDocenteId())) {
			docenteService.replaceDoc(docente);
			response.setTimestamp(new Date());
			response.setStatus(HttpStatus.OK.value());
			response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setResponse(docente);
			return response;
		} else {
			response.setTimestamp(new Date());
			response.setMessagge("SERVIZIO_NON_ELABORATO_CORRETTAMENTE");
			response.setResponse(docente);
			return response;
		}
	}
	
	@PostMapping(consumes = "application/json", produces = "application/json", value = "/cerca/{pagine}...{quantita}")
	public BaseResponseDto<DocenteDto> filter(@RequestBody Map<String,String> docente, @PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		 BaseResponseDto<DocenteDto> response = new BaseResponseDto<>();

			
			response.setTimestamp(new Date());
			response.setStatus(HttpStatus.OK.value());
			response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
			response.setResponse(docenteService.dtoFilter(docente.get("cerca"), pagine, quantita));
			return response;

	}
	
	@PostMapping(value = "/getPagesFilter/{pagine}...{quantita}" , produces = "application/json")
	public BaseResponseDto<Integer> getPages(@RequestBody Map<String,String> docente, @PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<Integer> response = new BaseResponseDto<>();

		Integer numero = docenteService.getPagesFilter(docente.get("cerca"), pagine, quantita);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (numero == 0) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(numero);
		return response;
	}

}
