package it.its.pj.lm3.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "societa")
@Data
public class SocietaDao {

	@Id
	@Column(name = "societaId")
	private int societaId;

	@Column(name = "ragioneSociale")
	private String ragioneSociale;

	@Column(name = "indirizzo")
	private String indirizzo;

	@Column(name = "localita")
	private String localita;

	@Column(name = "provincia")
	private String provincia;

	@Column(name = "nazione")
	private String nazione;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "fax")
	private String fax;

	@Column(name = "partitaIva")
	private String partitaIva;

}
