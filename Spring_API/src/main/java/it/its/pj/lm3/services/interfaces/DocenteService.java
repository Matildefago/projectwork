package it.its.pj.lm3.services.interfaces;

import java.util.List;

import it.its.pj.lm3.dao.DocenteDao;
import it.its.pj.lm3.dto.DocenteDto;

public interface DocenteService {

	public List<DocenteDao> selTuttiPage(int pagina, int quantita);
	
	public List<DocenteDto> parseDtoPage(int pagina, int quantita);
	
	public DocenteDto selById(int docenteId);
	
	public void delDoc(int docenteId);
	
	public boolean insDoc(DocenteDto docente);
	
	public boolean trovaId(int docenteId);
	
	public void dtoToDao(DocenteDto docenteDto, DocenteDao docenteDao);
	
	public void daoToDto(DocenteDto dto, DocenteDao dao);

	public boolean replaceDoc(DocenteDto docente);
	
	public List<DocenteDto> parseDto();
	
	public List<DocenteDao> selTutti();
	
	public int getPages(int pagine, int quantita);
	
	public List<DocenteDao> gFilter(String filter, int pagina, int quantita);
	
	public List<DocenteDto> dtoFilter(String filter, int pagina, int quantita);
	
	public int getPagesFilter(String filter,int pagine, int quantita);
}
