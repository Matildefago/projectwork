package it.its.pj.lm3.services.interfaces;

import java.util.List;

import it.its.pj.lm3.dao.DipendenteDao;
import it.its.pj.lm3.dto.DipendenteDto;

public interface DipendenteService {

	public List<DipendenteDao> selTutti();

	public List<DipendenteDto> parseDto();

	public DipendenteDto selById(int dipendentiId);

	public void daoToDto(DipendenteDto dto, DipendenteDao dao);

	public List<DipendenteDto> docentiInterni();

	public List<DipendenteDao> selAlunni();

	public List<DipendenteDto> allAlunni();

	public List<DipendenteDao> selTuttiPage(int pagina, int quantita);

	public List<DipendenteDto> parseDtoPage(int pagina, int quantita);

	public int getPages(int pagine, int quantita);

	public int getPagesFilter(String filter, int pagine, int quantita);

	public List<DipendenteDao> gFilter(String filter, int pagina, int quantita);

	public List<DipendenteDto> dtoFilter(String filter, int pagina, int quantita);

	public List<DipendenteDao> doppioni();

	public List<DipendenteDto> dtoDoppioni();

	public List<DipendenteDao> doppPages(int pagine, int quantita);

	public List<DipendenteDto> dtoDoppPages(int pagine, int quantita);

	public int numberDopp(int pagine, int quantita);
	
	//Filter Doc Interni

	public List<DipendenteDao> filterPage(String filter, int pagine, int quantita);

	public List<DipendenteDto> dtoFilterPage(String filter, int pagine, int quantita);

	public int numFilter(String filter, int pagine, int quantita);

}
