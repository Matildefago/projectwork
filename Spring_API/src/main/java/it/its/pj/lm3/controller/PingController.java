package it.its.pj.lm3.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api/ping")
public class PingController {

	@GetMapping(produces = "application/json")
	public String ping() {
		String s = "Ping ok";
		return s;
	}
}
