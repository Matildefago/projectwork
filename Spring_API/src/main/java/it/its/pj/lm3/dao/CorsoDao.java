package it.its.pj.lm3.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "corsi")
@Data
public class CorsoDao {
	
	@Id
	@Column(name = "corsoId")
	private int corsoId;
	
	@Column(name = "descrizione")
	private String descrizione;
	
	@Column(name = "edizione")
	private String edizione;
	
	@Column(name = "previsto")
	private int previsto;
	
	@Column(name = "erogato")
	private int erogato;

	@Column(name = "durata")
	private int durata;
	
	@Column(name = "note")
	private String note;
	
	@Column(name = "luogo")
	private String luogo;
	
	@Column(name = "ente")
	private String ente;

	@Column(name = "dataErogazione")
	private Date dataErogazione;
	
	@Column(name = "dataChiusura")
	private Date dataChiusura;
	
	@Column(name = "dataCensimento")
	private Date dataCensimento;
	
	@Column(name = "interno")
	private int interno;
	//Chiavi esterne
	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "societa", referencedColumnName = "societaId")
	@JsonBackReference(value = "societa")
	private SocietaDao societa;
	
	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "tipo", referencedColumnName = "tipoId")
	@JsonBackReference(value="tipo")
	private TipoDao tipo;
	
	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "tipologia", referencedColumnName = "tipologiaId")
	@JsonBackReference(value="tipologia")
	private TipologiaDao tipologia;
	
	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "misura", referencedColumnName = "misuraId")
	@JsonBackReference(value="misura")
	private MisuraDao misura;
}
