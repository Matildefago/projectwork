package it.its.pj.lm3.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocenteDto {

	private int docenteId;

	private String titolo;
	private String ragioneSociale;
	private String indirizzo;
	private String localita;
	private String provincia;
	private String nazione;
	private String telefono;
	private String fax;
	private String partitaIva;
	private String referente;

}
