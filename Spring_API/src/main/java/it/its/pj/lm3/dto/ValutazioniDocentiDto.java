package it.its.pj.lm3.dto;

import it.its.pj.lm3.dao.CorsoDao;
import it.its.pj.lm3.dao.DocenteDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValutazioniDocentiDto {
	private int valutazioniDocentiId; 
	private String criterio; 
	private String valore; 
	
	//Chiavi esterne
	private CorsoDao corso; 
	private DocenteDao docente;
	
	

}
