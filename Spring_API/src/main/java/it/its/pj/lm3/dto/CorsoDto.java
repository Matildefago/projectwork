package it.its.pj.lm3.dto;

import java.util.Date;

import it.its.pj.lm3.dao.MisuraDao;
import it.its.pj.lm3.dao.SocietaDao;
import it.its.pj.lm3.dao.TipoDao;
import it.its.pj.lm3.dao.TipologiaDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CorsoDto {

	private int corsoId;
	private String descrizione;
	private String edizione;
	private int previsto;
	private int erogato;
	private int durata;
	private String note;
	private String luogo;
	private String ente;
	private Date dataErogazione;
	private Date dataChiusura;
	private Date dataCensimento;
	private int interno;
	//CHIAVE ESTERNA
	private SocietaDao societa;
	private TipoDao tipo;
	private TipologiaDao tipologia;
	private MisuraDao misura;
	
	
}
