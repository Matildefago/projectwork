package it.its.pj.lm3.services.interfaces;

import java.util.List;

import it.its.pj.lm3.dao.TipoDao;
import it.its.pj.lm3.dto.TipoDto;

public interface TipoService {
	
    public List<TipoDto> parseDto();
	
	public List<TipoDao> selTutti();

}
