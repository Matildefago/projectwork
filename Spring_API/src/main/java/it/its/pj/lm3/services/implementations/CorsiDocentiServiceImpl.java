package it.its.pj.lm3.services.implementations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.its.pj.lm3.dao.CorsiDocentiDao;
import it.its.pj.lm3.dao.DipendenteDao;
import it.its.pj.lm3.dto.CorsiDocentiDto;
import it.its.pj.lm3.dto.DipendenteDto;
import it.its.pj.lm3.repository.CorsiDocentiRepository;
import it.its.pj.lm3.repository.DipendenteRepository;
import it.its.pj.lm3.services.interfaces.CorsiDocentiService;
import it.its.pj.lm3.services.interfaces.DipendenteService;

@Service
@Transactional
public class CorsiDocentiServiceImpl implements CorsiDocentiService {

	@Autowired
	CorsiDocentiRepository corsiDocentiRepository;
	@Autowired
	DipendenteService dipendentiService;
	@Autowired
	DipendenteRepository dipendenteRepository;
	
	
	@Override
	public List<CorsiDocentiDao> selTuttiDoc() {

		List<CorsiDocentiDao> resp = corsiDocentiRepository.findAll();
		List<CorsiDocentiDao> res = new ArrayList<CorsiDocentiDao>();
		for (CorsiDocentiDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public boolean insCorsiDocenti(CorsiDocentiDto corsiDocenti) {
		CorsiDocentiDao dao = new CorsiDocentiDao();
		dtoToDao(corsiDocenti, dao);
		try {
			corsiDocentiRepository.save(dao);

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	@Override
	public void dtoToDao(CorsiDocentiDto dto, CorsiDocentiDao dao) {
		dao.setDocente(dto.getDocente() != 0 ? dto.getDocente() : dao.getDocente());
		dao.setInterno(dto.getInterno() != 0 ? dto.getInterno() : dao.getInterno());
		dao.setCorso(dto.getCorso() != null ? dto.getCorso() : dao.getCorso());
		
	}

	@Override
	public List<CorsiDocentiDao> getAllDoc(int num) {
		return corsiDocentiRepository.getAllDoc(num);
	}

	@Override
	public List<CorsiDocentiDto> getDtoDoc(int num) {
		List<CorsiDocentiDto> dto = new ArrayList<CorsiDocentiDto>();
		List<CorsiDocentiDao> dao = this.getAllDoc(num);
		for(CorsiDocentiDao d : dao) {
			dto.add(new CorsiDocentiDto(d.getCorsiDocentiId(), d.getDocente(), d.getInterno(), d.getCorso()));
		}
		return dto;
	}

	@Override
	public List<CorsiDocentiDao> getCorsiEsterni(int num) {
		return corsiDocentiRepository.getAllCorsiDocEsterni(num);
	}

	@Override
	public List<CorsiDocentiDto> getDtoEsterni(int num) {
		List<CorsiDocentiDto> dto = new ArrayList<CorsiDocentiDto>();
		List<CorsiDocentiDao> dao = this.getCorsiEsterni(num);
		for(CorsiDocentiDao d : dao) {
			dto.add(new CorsiDocentiDto(d.getCorsiDocentiId(), d.getDocente(), d.getInterno(), d.getCorso()));
		}
		return dto;
	}

	@Override
	public List<CorsiDocentiDao> getCorsiInterni(int num) {
		return corsiDocentiRepository.getAllCorsiDocInterni(num);
	}

	@Override
	public List<CorsiDocentiDto> getDtoInterni(int num) {
		List<CorsiDocentiDto> dto = new ArrayList<CorsiDocentiDto>();
		List<CorsiDocentiDao> dao = this.getCorsiInterni(num);
		for(CorsiDocentiDao d : dao) {
			dto.add(new CorsiDocentiDto(d.getCorsiDocentiId(), d.getDocente(), d.getInterno(), d.getCorso()));
		}
		return dto;
	}

	@Override
	public List<CorsiDocentiDao> pagesEsterni(int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<CorsiDocentiDao> resp = corsiDocentiRepository.listaPageEsterni(p);
		List<CorsiDocentiDao> res = new ArrayList<CorsiDocentiDao>();
		for (CorsiDocentiDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<CorsiDocentiDto> dtoPagesEsterni(int pagine, int quantita) {
		List<CorsiDocentiDto> dto = new ArrayList<CorsiDocentiDto>();
		List<CorsiDocentiDao> dao = this.pagesEsterni(pagine, quantita);
		for(CorsiDocentiDao d : dao) {
			dto.add(new CorsiDocentiDto(d.getCorsiDocentiId(), d.getDocente(), d.getInterno(), d.getCorso()));
		}
		return dto;
	}

	@Override
	public int getNumberEsterni(int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<CorsiDocentiDao> resp = corsiDocentiRepository.listaPageEsterni(p);
		int pages = resp.getTotalPages();
		return pages;
	}

	@Override
	public List<CorsiDocentiDao> pagesInterni(int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<CorsiDocentiDao> resp = corsiDocentiRepository.listaPageInterni(p);
		List<CorsiDocentiDao> res = new ArrayList<CorsiDocentiDao>();
		for (CorsiDocentiDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<CorsiDocentiDto> dtoPagesInterni(int pagine, int quantita) {
		List<CorsiDocentiDto> dto = new ArrayList<CorsiDocentiDto>();
		List<CorsiDocentiDao> dao = this.pagesInterni(pagine, quantita);
		for(CorsiDocentiDao d : dao) {
			dto.add(new CorsiDocentiDto(d.getCorsiDocentiId(), d.getDocente(), d.getInterno(), d.getCorso()));
		}
		return dto;
	}

	@Override
	public int getNumberInterni(int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<CorsiDocentiDao> resp = corsiDocentiRepository.listaPageInterni(p);
		int pages = resp.getTotalPages();
		return pages;
	}

	@Override
	public void delete(int id) {
		corsiDocentiRepository.deleteById(id);
		
	}

	@Override
	public boolean trovaId(int id) {
		boolean trovato = false;
		List<CorsiDocentiDto> lista = this.dtoAll();
		for (CorsiDocentiDto d : lista) {
			if (d.getCorsiDocentiId() == id) {
				trovato = true;
				break;
			}
		}
		return trovato;
	}

	@Override
	public List<CorsiDocentiDto> dtoAll() {
		List<CorsiDocentiDto> dto = new ArrayList<CorsiDocentiDto>();
		List<CorsiDocentiDao> dao = this.selTuttiDoc();
		for(CorsiDocentiDao d : dao) {
			dto.add(new CorsiDocentiDto(d.getCorsiDocentiId(), d.getDocente(), d.getInterno(), d.getCorso()));
		}
		return dto;
	}
	
	public List<DipendenteDto> filtraDocInterni(String valore, int pagina, int q) {
		List<DipendenteDto> allDip = this.dipendentiService.dtoFilter(valore, pagina, q);
		
		ArrayList<DipendenteDto> dip = new ArrayList<>();
		for(DipendenteDto d : allDip) {
			int[] allDocInterni = this.corsiDocentiRepository.getDocenti();
			if(Arrays.stream(allDocInterni).anyMatch(k -> k == d.getDipendenteId())) dip.add(d);
		}
		return dip;
	}
	
	@Override
	public int dtoFilterPages(int quantita, String filter) {
		List<DipendenteDao> allDip = this.dipendenteRepository.findByDocInterni(filter);
		int[] allDocInterni = this.corsiDocentiRepository.getDocenti();
		ArrayList<DipendenteDao> dip = new ArrayList<DipendenteDao>();
		for(DipendenteDao d : allDip) {
			if(Arrays.stream(allDocInterni).anyMatch(k -> k == d.getDipendenteId())) dip.add(d);
		}
		int i = dip.size();
		int cont = 1;
		int c = 1;
		int dipendenti = 0;
		if(i <= quantita) {
			return cont;
		}else {
			while(dipendenti < i) {
				if(c == quantita) {
					cont++;
					c = 0;
				}else {
					c++;
				}
				dipendenti++;
			}	
			return cont;
		}
	}

}