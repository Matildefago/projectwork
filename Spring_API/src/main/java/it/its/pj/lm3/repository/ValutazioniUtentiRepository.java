package it.its.pj.lm3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.its.pj.lm3.dao.ValutazioniUtentiDao;

public interface ValutazioniUtentiRepository extends JpaRepository<ValutazioniUtentiDao, Integer>{
	
	@Query(value = "select * from valutazioni_utenti inner join dipendenti on valutazioni_utenti.dipendente = dipendenti.dipendente_id"
			+ " inner join corsi on valutazioni_utenti.corso = corsi.corso_id where valutazioni_utenti.dipendente = :dip"
			+ " and valutazioni_utenti.corso = :cor", nativeQuery = true)
	public List<ValutazioniUtentiDao> getAllCorsi(@Param("dip") int dip, @Param("cor") int cor);
	
	@Query(nativeQuery = true, value = "select * from valutazioni_utenti inner join dipendenti on valutazioni_utenti.dipendente = dipendenti.dipendente_id "
			+ " inner join corsi ON valutazioni_utenti.corso = corsi.corso_id "
			+ "WHERE valutazioni_utenti.criterio LIKE %:filter% " 
			+ "OR valutazioni_utenti.valore LIKE %:filter% "
			+ "OR dipendenti.matricola LIKE %:filter% "
			+ "OR dipendenti.nome LIKE %:filter% "
			+ "OR dipendenti.cognome LIKE %:filter% "
			+ "OR corsi.descrizione LIKE %:filter% ")
	public List<ValutazioniUtentiDao> findByAll(@Param("filter") String filter);
	
}
