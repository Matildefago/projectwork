package it.its.pj.lm3.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.pj.lm3.dto.BaseResponseDto;
import it.its.pj.lm3.dto.CorsoDto;
import it.its.pj.lm3.services.interfaces.FiltroCorsoService;

@RestController
@RequestMapping(value = "api/filtro")
public class FiltroCorsoController {

	@Autowired
	FiltroCorsoService filtroCorsoService;

	@GetMapping(produces = "application/json", value = "/descrizione/{stringa}..{pagine}..{quantita}")
	public BaseResponseDto<CorsoDto> cercaDescrizione(@PathVariable("stringa") String stringa,
			@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<CorsoDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(filtroCorsoService.parseDtoDescrizione(stringa, pagine, quantita));
		return response;

	}

	@GetMapping(produces = "application/json", value = "/pagesDesc/{stringa}..{pagine}..{quantita}")
	public BaseResponseDto<CorsoDto> getPagesDescrizione(@PathVariable("stringa") String stringa,
			@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<CorsoDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(filtroCorsoService.getDescrizionePages(stringa, pagine, quantita));
		return response;

	}

	@GetMapping(produces = "application/json", value = "/luogo/{stringa}..{pagine}..{quantita}")
	public BaseResponseDto<CorsoDto> cercaLuogo(@PathVariable("stringa") String stringa,
			@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<CorsoDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(filtroCorsoService.parseDtoLuogo(stringa, pagine, quantita));
		return response;

	}

	@GetMapping(produces = "application/json", value = "/pagesLuo/{stringa}..{pagine}..{quantita}")
	public BaseResponseDto<CorsoDto> getPagesLuogo(@PathVariable("stringa") String stringa,
			@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<CorsoDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(filtroCorsoService.getLuogoPages(stringa, pagine, quantita));
		return response;

	}

	@GetMapping(produces = "application/json", value = "/ente/{stringa}..{pagine}..{quantita}")
	public BaseResponseDto<CorsoDto> cercaEnte(@PathVariable("stringa") String stringa,
			@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<CorsoDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(filtroCorsoService.parseDtoEnte(stringa, pagine, quantita));
		return response;

	}

	@GetMapping(produces = "application/json", value = "/pagesEnt/{stringa}..{pagine}..{quantita}")
	public BaseResponseDto<CorsoDto> getPagesEnte(@PathVariable("stringa") String stringa,
			@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<CorsoDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(filtroCorsoService.getEntePages(stringa, pagine, quantita));
		return response;

	}

	@GetMapping(produces = "application/json", value = "/tipologia/{stringa}..{pagine}..{quantita}")
	public BaseResponseDto<CorsoDto> cercaTipologia(@PathVariable("stringa") String stringa,
			@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<CorsoDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(filtroCorsoService.parseDtoTipologia(stringa, pagine, quantita));
		return response;

	}

	@GetMapping(produces = "application/json", value = "/pagesTip/{stringa}..{pagine}..{quantita}")
	public BaseResponseDto<CorsoDto> getPagesTipologia(@PathVariable("stringa") String stringa,
			@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<CorsoDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(filtroCorsoService.getTipologiaPages(stringa, pagine, quantita));
		return response;

	}

	@GetMapping(produces = "application/json", value = "/societa/{stringa}..{pagine}..{quantita}")
	public BaseResponseDto<CorsoDto> cercaSocieta(@PathVariable("stringa") String stringa,
			@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<CorsoDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(filtroCorsoService.parseDtoSocieta(stringa, pagine, quantita));
		return response;

	}

	@GetMapping(produces = "application/json", value = "/pagesSoc/{stringa}..{pagine}..{quantita}")
	public BaseResponseDto<CorsoDto> getPagesSocieta(@PathVariable("stringa") String stringa,
			@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<CorsoDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(filtroCorsoService.getSocietaPages(stringa, pagine, quantita));
		return response;

	}

}
