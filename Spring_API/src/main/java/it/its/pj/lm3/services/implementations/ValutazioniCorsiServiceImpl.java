package it.its.pj.lm3.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.pj.lm3.dao.ValutazioniCorsiDao;
import it.its.pj.lm3.dto.ValutazioniCorsiDto;
import it.its.pj.lm3.repository.ValutazioniCorsiRepository;
import it.its.pj.lm3.services.interfaces.ValutazioniCorsiService;

@Service
@Transactional

public class ValutazioniCorsiServiceImpl implements ValutazioniCorsiService {

	@Autowired
	ValutazioniCorsiRepository valutazioniCorsiRepository;

	@Override
	public List<ValutazioniCorsiDao> selTuttiCorsi() {
		List<ValutazioniCorsiDao> resp = valutazioniCorsiRepository.findAll();
		List<ValutazioniCorsiDao> res = new ArrayList<ValutazioniCorsiDao>();
		for (ValutazioniCorsiDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public void dtoToDao(ValutazioniCorsiDto dto, ValutazioniCorsiDao dao) {
		dao.setCriterio(dto.getCriterio() != null ? dto.getCriterio() : dao.getCriterio());
		dao.setValore(dto.getValore() != null ? dto.getValore() : dao.getValore());
		dao.setCorso(dto.getCorso() != null ? dto.getCorso() : dao.getCorso());
	}

	@Override
	public boolean insValCorsi(ValutazioniCorsiDto valutazioniCorsi) {
		ValutazioniCorsiDao dao = new ValutazioniCorsiDao();
		dtoToDao(valutazioniCorsi, dao);
		try {
			valutazioniCorsiRepository.saveAndFlush(dao);

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	@Override
	public boolean findId(int id) {
		return valutazioniCorsiRepository.existsById(id);

	}

	@Override
	public void deleteAss(int valCoCo) {
		valutazioniCorsiRepository.deleteById(valCoCo);

	}

	@Override
	public ValutazioniCorsiDto selById(int valCoCo) {
		try {
			ValutazioniCorsiDto dto = new ValutazioniCorsiDto();
			daoToDto(dto, valutazioniCorsiRepository.getOne(valCoCo));
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void daoToDto(ValutazioniCorsiDto dto, ValutazioniCorsiDao dao) {
		dto.setValutazioniCorsiId(dao.getValutazioniCorsiId());
		dto.setCriterio(dao.getCriterio());
		dto.setValore(dao.getValore());
		dto.setCorso(dao.getCorso());

	}

	@Override
	public List<ValutazioniCorsiDao> getInfo(int num) {
		return valutazioniCorsiRepository.infoValCorso(num);
	}

	@Override
	public List<ValutazioniCorsiDto> dtoInfo(int num) {
		List<ValutazioniCorsiDto> dto = new ArrayList<ValutazioniCorsiDto>();
		List<ValutazioniCorsiDao> dao = this.getInfo(num);
		for (ValutazioniCorsiDao d : dao) {
			dto.add(new ValutazioniCorsiDto(d.getValutazioniCorsiId(), d.getCriterio(), d.getValore(), d.getCorso()));
		}
		return dto;
	}

	@Override
	public List<ValutazioniCorsiDao> gFilter(String filter) {
		List<ValutazioniCorsiDao> resp = valutazioniCorsiRepository.findByAll(filter);
		System.out.println("test: " + resp.toString());
		List<ValutazioniCorsiDao> res = new ArrayList<ValutazioniCorsiDao>();
		for (ValutazioniCorsiDao d : resp) {
			res.add(d);

		}

		return res;
	}

	@Override
	public List<ValutazioniCorsiDto> dtoFilter(String filter) {
		List<ValutazioniCorsiDto> dto = new ArrayList<ValutazioniCorsiDto>();
		List<ValutazioniCorsiDao> dao = this.gFilter(filter);
		for (ValutazioniCorsiDao d : dao) {
			ValutazioniCorsiDto temp = new ValutazioniCorsiDto();
			temp.setValutazioniCorsiId(d.getValutazioniCorsiId());
			temp.setCorso(d.getCorso());
			temp.setCriterio(d.getCriterio());
			temp.setValore(d.getValore());
			
			dto.add(temp);

		}

		return dto;
	}

}
