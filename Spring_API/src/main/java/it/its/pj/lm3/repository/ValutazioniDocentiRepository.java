package it.its.pj.lm3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.its.pj.lm3.dao.ValutazioniDocentiDao;

public interface ValutazioniDocentiRepository extends JpaRepository<ValutazioniDocentiDao, Integer> {
	
	@Query(value = "select * from valutazioni_docenti inner join docenti on valutazioni_docenti.docente = docenti.docente_id "
			+ "where valutazioni_docenti.docente = :idDoc and valutazioni_docenti.corso = :idCor", nativeQuery = true)
	List<ValutazioniDocentiDao> getInfo(@Param("idDoc") int idDoc, @Param("idCor") int idCor);
	
	@Query(nativeQuery = true, value = "select * from valutazioni_docenti inner join docenti on valutazioni_docenti.docente = docenti.docente_id "
			+ " inner join corsi ON valutazioni_docenti.corso = corsi.corso_id "
			+ "WHERE valutazioni_docenti.criterio LIKE %:filter% " 
			+ "OR valutazioni_docenti.valore LIKE %:filter% "
			+ "OR docenti.titolo LIKE %:filter% "
			+ "OR docenti.ragione_sociale LIKE %:filter% "
			+ "OR docenti.localita LIKE %:filter% "
			+ "OR docenti.referente LIKE %:filter% "
			+ "OR corsi.descrizione LIKE %:filter% ")
	public List<ValutazioniDocentiDao> findByAll(@Param("filter") String filter);

}
