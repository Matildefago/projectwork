package it.its.pj.lm3.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.pj.lm3.dao.ValutazioniDocentiDao;
import it.its.pj.lm3.dto.ValutazioniDocentiDto;
import it.its.pj.lm3.repository.ValutazioniDocentiRepository;
import it.its.pj.lm3.services.interfaces.ValutazioniDocentiService;

@Service
@Transactional

public class ValutazioniDocentiServiceImpl implements ValutazioniDocentiService {

	@Autowired
	ValutazioniDocentiRepository valutazioniDocentiRepository;

	@Override
	public List<ValutazioniDocentiDao> selTuttiDocenti() {
		List<ValutazioniDocentiDao> resp = valutazioniDocentiRepository.findAll();
		List<ValutazioniDocentiDao> res = new ArrayList<ValutazioniDocentiDao>();
		for (ValutazioniDocentiDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public boolean insValDocenti(ValutazioniDocentiDto valutazioniDocenti) {
		ValutazioniDocentiDao dao = new ValutazioniDocentiDao();
		dtoToDao(valutazioniDocenti, dao);
		try {
			valutazioniDocentiRepository.save(dao);

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	@Override
	public void dtoToDao(ValutazioniDocentiDto dto, ValutazioniDocentiDao dao) {
		dao.setCriterio(dto.getCriterio() != null ? dto.getCriterio() : dao.getCriterio());
		dao.setValore(dto.getValore() != null ? dto.getValore() : dao.getValore());
		dao.setCorso(dto.getCorso() != null ? dto.getCorso() : dao.getCorso());
		dao.setDocente(dto.getDocente() != null ? dto.getDocente() : dao.getDocente());

	}

	@Override
	public boolean findId(int id) {
		return valutazioniDocentiRepository.existsById(id);

	}

	@Override
	public void deleteAss(int valDoCo) {
		valutazioniDocentiRepository.deleteById(valDoCo);

	}

	@Override
	public ValutazioniDocentiDto selById(int valDoCo) {
		try {
			ValutazioniDocentiDto dto = new ValutazioniDocentiDto();
			daoToDto(dto, valutazioniDocentiRepository.getOne(valDoCo));
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void daoToDto(ValutazioniDocentiDto dto, ValutazioniDocentiDao dao) {
		dto.setValutazioniDocentiId(dao.getValutazioniDocentiId());
		dto.setCriterio(dao.getCriterio());
		dto.setValore(dao.getValore());
		dto.setCorso(dao.getCorso());
		dto.setDocente(dao.getDocente());

	}

	@Override
	public List<ValutazioniDocentiDao> getInfo(int idDoc, int idCor) {
		return valutazioniDocentiRepository.getInfo(idDoc, idCor);
	}

	@Override
	public List<ValutazioniDocentiDto> dtoInfo(int idDoc, int idCor) {
		List<ValutazioniDocentiDto> dto = new ArrayList<ValutazioniDocentiDto>();
		List<ValutazioniDocentiDao> dao = this.getInfo(idDoc, idCor);
		for(ValutazioniDocentiDao d : dao) {
			dto.add(new ValutazioniDocentiDto(d.getValutazioniDocentiId(), d.getCriterio(), d.getValore(), d.getCorso(), d.getDocente()));
		}
		return dto;
	}

	@Override
	public List<ValutazioniDocentiDao> gFilter(String filter) {
		List<ValutazioniDocentiDao> resp = valutazioniDocentiRepository.findByAll(filter);
		 List<ValutazioniDocentiDao> res = new ArrayList<ValutazioniDocentiDao>();
		 for(ValutazioniDocentiDao d : resp) {
			 res.add(d);
			 
		 }
		
		return res;

	}

	@Override
	public List<ValutazioniDocentiDto> dtoFilter(String filter) {
		List<ValutazioniDocentiDto> dto = new ArrayList<ValutazioniDocentiDto>();
		List<ValutazioniDocentiDao> dao = this.gFilter(filter);
		for (ValutazioniDocentiDao d : dao) {
			ValutazioniDocentiDto temp = new ValutazioniDocentiDto();
			temp.setValutazioniDocentiId(d.getValutazioniDocentiId());
			temp.setCorso(d.getCorso());
			temp.setDocente(d.getDocente());
			temp.setCriterio(d.getCriterio());
			temp.setValore(d.getValore());
			dto.add(temp);
			
		}
		
		return dto;
	}

}
