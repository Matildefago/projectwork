package it.its.pj.lm3.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.its.pj.lm3.dao.TipologiaDao;

public interface TipologiaRepository extends JpaRepository<TipologiaDao, Integer>{

}
