package it.its.pj.lm3.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.its.pj.lm3.dao.DocenteDao;

public interface DocenteRepository extends JpaRepository<DocenteDao, Integer> {

	@Query(value = "SELECT * FROM docenti WHERE titolo LIKE %:filter% "
			+ "OR ragione_sociale LIKE %:filter% "
			+ "OR indirizzo LIKE %:filter% "
			+ "OR localita LIKE %:filter% "
			+ "OR provincia LIKE %:filter% "
			+ "OR nazione LIKE %:filter% "
			+ "OR telefono LIKE %:filter% "
			+ "OR fax LIKE %:filter% "
			+ "OR partita_iva LIKE %:filter% "
			+ "OR referente LIKE %:filter%", nativeQuery = true)
	public Page<DocenteDao> findByAll(@Param("filter") String filter, Pageable p);
}
