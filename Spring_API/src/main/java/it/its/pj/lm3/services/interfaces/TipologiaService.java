package it.its.pj.lm3.services.interfaces;

import java.util.List;

import it.its.pj.lm3.dao.TipologiaDao;
import it.its.pj.lm3.dto.TipologiaDto;

public interface TipologiaService {
	
    public List<TipologiaDto> parseDto();
	
	public List<TipologiaDao> selTutti();

}
