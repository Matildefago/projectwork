package it.its.pj.lm3.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.pj.lm3.dao.ValutazioniUtentiDao;
import it.its.pj.lm3.dto.ValutazioniUtentiDto;
import it.its.pj.lm3.repository.ValutazioniUtentiRepository;
import it.its.pj.lm3.services.interfaces.ValutazioniUtentiService;

@Service
@Transactional

public class ValutazioniUtentiServiceImpl implements ValutazioniUtentiService {
	@Autowired
	ValutazioniUtentiRepository valutazioniUtentiRepository;

	@Override
	public List<ValutazioniUtentiDao> selTuttiUtenti() {
		List<ValutazioniUtentiDao> resp = valutazioniUtentiRepository.findAll();
		List<ValutazioniUtentiDao> res = new ArrayList<ValutazioniUtentiDao>();
		for (ValutazioniUtentiDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public void dtoToDao(ValutazioniUtentiDto dto, ValutazioniUtentiDao dao) {
		dao.setCriterio(dto.getCriterio() != null ? dto.getCriterio() : dao.getCriterio());
		dao.setValore(dto.getValore() != null ? dto.getValore() : dao.getValore());
		dao.setCorso(dto.getCorso() != null ? dto.getCorso() : dao.getCorso());
		dao.setDipendente(dto.getDipendente() != null ? dto.getDipendente() : dao.getDipendente());

	}

	@Override
	public boolean insValUtenti(ValutazioniUtentiDto valutazioniUtenti) {
		ValutazioniUtentiDao dao = new ValutazioniUtentiDao();
		dtoToDao(valutazioniUtenti, dao);
		try {
			valutazioniUtentiRepository.save(dao);

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	@Override
	public boolean findId(int id) {
		return valutazioniUtentiRepository.existsById(id);

	}

	@Override
	public void deleteAss(int valUtCo) {
		valutazioniUtentiRepository.deleteById(valUtCo);

	}

	@Override
	public ValutazioniUtentiDto selById(int valUtCo) {
		try {
			ValutazioniUtentiDto dto = new ValutazioniUtentiDto();
			daoToDto(dto, valutazioniUtentiRepository.getOne(valUtCo));
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void daoToDto(ValutazioniUtentiDto dto, ValutazioniUtentiDao dao) {
		dto.setValutazioniUtentiId(dao.getValutazioniUtentiId());
		dto.setCriterio(dao.getCriterio());
		dto.setValore(dao.getValore());
		dto.setCorso(dao.getCorso());
		dto.setDipendente(dao.getDipendente());

	}

	@Override
	public List<ValutazioniUtentiDao> getVal(int idDip, int idCors) {
		return valutazioniUtentiRepository.getAllCorsi(idDip, idCors);
	}

	@Override
	public List<ValutazioniUtentiDto> dtoVal(int idDip, int idCors) {
		List<ValutazioniUtentiDto> dto = new ArrayList<ValutazioniUtentiDto>();
		List<ValutazioniUtentiDao> dao = this.getVal(idDip,idCors);
		for(ValutazioniUtentiDao d : dao) {
			dto.add(new ValutazioniUtentiDto(d.getValutazioniUtentiId(), d.getCriterio(), d.getValore(), d.getCorso(), d.getDipendente()));
		}
		return dto;
	}

	@Override
	public List<ValutazioniUtentiDao> gFilter(String filter) {
		List<ValutazioniUtentiDao> resp = valutazioniUtentiRepository.findByAll(filter);
		
		 List<ValutazioniUtentiDao> res = new ArrayList<ValutazioniUtentiDao>();
		 for(ValutazioniUtentiDao d : resp) {
			 res.add(d);
			 
		 }
		
		return res;
	}

	@Override
	public List<ValutazioniUtentiDto> dtoFilter(String filter) {
		List<ValutazioniUtentiDto> dto = new ArrayList<ValutazioniUtentiDto>();
		List<ValutazioniUtentiDao> dao = this.gFilter(filter);
		for (ValutazioniUtentiDao d : dao) {
			ValutazioniUtentiDto temp = new ValutazioniUtentiDto();
			temp.setValutazioniUtentiId(d.getValutazioniUtentiId());
			temp.setCorso(d.getCorso());
			temp.setDipendente(d.getDipendente());
			temp.setCriterio(d.getCriterio());
			temp.setValore(d.getValore());
			dto.add(temp);
			
		}
		
		return dto;
	}

	
}
