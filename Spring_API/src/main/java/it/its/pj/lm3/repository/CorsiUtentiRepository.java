package it.its.pj.lm3.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.its.pj.lm3.dao.CorsiUtentiDao;

public interface CorsiUtentiRepository extends JpaRepository<CorsiUtentiDao, Integer>{
	
	@Query(value = "select * from corsi_utenti inner join dipendenti on corsi_utenti.dipendente = dipendenti.dipendente_id inner join corsi on corsi_utenti.corso = corsi.corso_id where corsi_utenti.corso = :num", nativeQuery = true)
	public Page<CorsiUtentiDao> finDip(@Param("num") int num, Pageable p);
	
	@Query(value = "select * from corsi_utenti inner join dipendenti on corsi_utenti.dipendente = dipendenti.dipendente_id inner join corsi on corsi_utenti.corso = corsi.corso_id where corsi_utenti.corso = :num", nativeQuery = true)
	public List<CorsiUtentiDao> getAll(@Param("num") int num);
	
	@Query(value = "select * from corsi_utenti inner join dipendenti on corsi_utenti.dipendente = dipendenti.dipendente_id inner join corsi on corsi_utenti.corso = corsi.corso_id where corsi_utenti.dipendente = :num", nativeQuery = true)
	public List<CorsiUtentiDao> getAllCorsi(@Param("num") int num);
	
	@Query(value = "select * from corsi_utenti inner join dipendenti on corsi_utenti.dipendente = dipendenti.dipendente_id inner join "
			+ "corsi on corsi_utenti.corso = corsi.corso_id where corsi.descrizione like %:filter%", nativeQuery = true)
	public Page<CorsiUtentiDao> filter(@Param("filter") String filter, Pageable p);

}
