package it.its.pj.lm3.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "misure")
@Data
public class MisuraDao {
	
	@Id
	@Column(name = "misuraId")
	private int misuraId;
	
	@Column(name = "descrizione")
	private String descrizione;

}
