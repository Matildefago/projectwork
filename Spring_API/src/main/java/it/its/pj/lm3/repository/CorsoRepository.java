package it.its.pj.lm3.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.its.pj.lm3.dao.CorsoDao;

public interface CorsoRepository extends JpaRepository<CorsoDao, Integer> {

	@Query(nativeQuery = true, value = "SELECT * FROM corsi inner join societa on corsi.societa = societa.societa_id "
			+ "INNER JOIN tipologie ON corsi.tipologia = tipologie.tipologia_id "
			+ "INNER JOIN tipi ON corsi.tipo = tipi.tipo_id "
			+ "INNER JOIN misure ON corsi.misura = misure.misura_id "
			+ "WHERE corsi.descrizione LIKE %:filter% "
			+ "OR corsi.edizione LIKE %:filter% "
			+ "OR corsi.previsto LIKE %:filter% "
			+ "OR corsi.erogato LIKE %:filter% "
			+ "OR corsi.durata LIKE %:filter% "
			+ "OR corsi.note LIKE %:filter% "
			+ "OR corsi.luogo LIKE %:filter% " 
			+ "OR corsi.ente LIKE %:filter% "
			+ "OR corsi.data_erogazione LIKE %:filter% "
			+ "OR corsi.data_chiusura LIKE %:filter% "
			+ "OR corsi.data_censimento LIKE %:filter% "
			+ "OR corsi.interno LIKE %:filter% "
			+ "OR societa.ragione_sociale LIKE %:filter% "
			+ "OR tipologie.descrizione LIKE %:filter% "
			+ "OR tipi.descrizione LIKE %:filter% "
			+ "OR misure.descrizione LIKE %:filter% "
			)
	public Page<CorsoDao> findByAll(@Param("filter") String filter, Pageable p);
}


