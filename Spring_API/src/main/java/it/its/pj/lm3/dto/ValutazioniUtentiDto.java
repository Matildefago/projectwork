package it.its.pj.lm3.dto;
import it.its.pj.lm3.dao.CorsoDao;
import it.its.pj.lm3.dao.DipendenteDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor

public class ValutazioniUtentiDto {
	private int valutazioniUtentiId;
	private String criterio;
	private String valore; 
	
	
	//Chiavi Esterni
	private CorsoDao corso;
	private DipendenteDao dipendente;
	

}
