package it.its.pj.lm3.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "corsiUtenti")
@Data
public class CorsiUtentiDao {

	@Id
	@Column(name = "corsiUtentiId")
	private int corsiUtentiId;

	@Column(name = "durata")
	private int durata;

	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "corso", referencedColumnName = "corsoId")
	@JsonBackReference(value = "corso")
	private CorsoDao corso;

	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "dipendente", referencedColumnName = "dipendenteId")
	@JsonBackReference(value = "dipendente")
	private DipendenteDao dipendente;

}
