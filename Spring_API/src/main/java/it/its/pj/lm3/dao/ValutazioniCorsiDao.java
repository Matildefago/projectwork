package it.its.pj.lm3.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "valutazioniCorsi")
@Data

public class ValutazioniCorsiDao {
	
	@Id
	@Column(name = "valutazioniCorsiId")
	private int valutazioniCorsiId;

	@Column(name = "criterio")
	private String criterio;

	@Column(name = "valore")
	private String valore;
	
	//Chiave Esterna
	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "corso", referencedColumnName = "corsoId")
	@JsonBackReference(value="corso")
	private CorsoDao corso;
	


}
