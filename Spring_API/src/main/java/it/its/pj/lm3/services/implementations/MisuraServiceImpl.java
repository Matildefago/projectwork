package it.its.pj.lm3.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.pj.lm3.dao.MisuraDao;
import it.its.pj.lm3.dto.MisuraDto;
import it.its.pj.lm3.repository.MisuraRepository;
import it.its.pj.lm3.services.interfaces.MisuraService;

@Service
@Transactional
public class MisuraServiceImpl implements MisuraService{
	
	@Autowired 
	MisuraRepository misuraRepository;

	@Override
	public List<MisuraDto> parseDto() {
		List<MisuraDto> dto = new ArrayList<MisuraDto>();
		List<MisuraDao> dao = this.selTutti();
		for (MisuraDao d : dao) {
			dto.add(new MisuraDto(d.getMisuraId(), d.getDescrizione()));
		}
		return dto;
	}

	@Override
	public List<MisuraDao> selTutti() {
		return misuraRepository.findAll();
	}

}
