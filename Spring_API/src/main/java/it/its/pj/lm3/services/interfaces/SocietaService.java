package it.its.pj.lm3.services.interfaces;

import java.util.List;

import it.its.pj.lm3.dao.SocietaDao;
import it.its.pj.lm3.dto.SocietaDto;

public interface SocietaService {

	public List<SocietaDao> selTutti();

	public List<SocietaDto> parseDto();
	
	
	

}
