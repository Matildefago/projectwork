package it.its.pj.lm3.services.interfaces;

import java.util.List;

import it.its.pj.lm3.dao.ValutazioniDocentiDao;
import it.its.pj.lm3.dto.ValutazioniDocentiDto;

public interface ValutazioniDocentiService {

	public List<ValutazioniDocentiDao> selTuttiDocenti();

	public boolean insValDocenti(ValutazioniDocentiDto valutazioniDocenti);

	public void dtoToDao(ValutazioniDocentiDto dto, ValutazioniDocentiDao dao);
	
	public boolean findId(int id);

	public void deleteAss(int valDoCo);

	public ValutazioniDocentiDto selById(int valDoCo);

	public void daoToDto(ValutazioniDocentiDto dto, ValutazioniDocentiDao dao);
	
	public List<ValutazioniDocentiDao> getInfo(int idDoc, int idCor);
	
	public List<ValutazioniDocentiDto> dtoInfo(int idDoc, int idCor);

	// Filter
	public List<ValutazioniDocentiDao> gFilter(String filter);

	public List<ValutazioniDocentiDto> dtoFilter(String filter);

}
