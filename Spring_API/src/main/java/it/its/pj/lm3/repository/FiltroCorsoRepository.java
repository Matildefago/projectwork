package it.its.pj.lm3.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.its.pj.lm3.dao.CorsoDao;

public interface FiltroCorsoRepository extends JpaRepository<CorsoDao, Integer> {

	@Query(value = "SELECT * FROM corsi " 
			+ "INNER JOIN tipologie ON corsi.tipologia = tipologie.tipologia_id "
			+ "INNER JOIN tipi ON corsi.tipo = tipi.tipo_id "
			+ "INNER JOIN misure ON corsi.misura = misure.misura_id "
			+ "INNER JOIN societa ON corsi.societa = societa.societa_id "
			+ "WHERE corsi.descrizione = :stringa", nativeQuery = true)
	public Page<CorsoDao> findByDescrizione(String stringa, Pageable p);

	@Query(value = "SELECT * FROM corsi " 
			+ "INNER JOIN tipologie ON corsi.tipologia = tipologie.tipologia_id "
			+ "INNER JOIN tipi ON corsi.tipo = tipi.tipo_id "
			+ "INNER JOIN misure ON corsi.misura = misure.misura_id "
			+ "INNER JOIN societa ON corsi.societa = societa.societa_id "
			+ "WHERE corsi.luogo = :stringa", nativeQuery = true)
	public Page<CorsoDao> findByLuogo(String stringa, Pageable p);

	@Query(value = "SELECT * FROM corsi " 
			+ "INNER JOIN tipologie ON corsi.tipologia = tipologie.tipologia_id "
			+ "INNER JOIN tipi ON corsi.tipo = tipi.tipo_id "
			+ "INNER JOIN misure ON corsi.misura = misure.misura_id "
			+ "INNER JOIN societa ON corsi.societa = societa.societa_id "
			+ "WHERE corsi.ente = :stringa", nativeQuery = true)
	public Page<CorsoDao> findByEnte(String stringa, Pageable p);
	
	@Query(value = "SELECT * FROM corsi " 
			+ "INNER JOIN tipologie ON corsi.tipologia = tipologie.tipologia_id "
			+ "INNER JOIN tipi ON corsi.tipo = tipi.tipo_id "
			+ "INNER JOIN misure ON corsi.misura = misure.misura_id "
			+ "INNER JOIN societa ON corsi.societa = societa.societa_id "
			+ "WHERE tipologie.descrizione = :stringa", nativeQuery = true)
	public Page<CorsoDao> findByTipologia(@Param("stringa") String stringa, Pageable p);

	@Query(value = "SELECT * FROM corsi " 
			+ "INNER JOIN tipologie ON corsi.tipologia = tipologie.tipologia_id "
			+ "INNER JOIN tipi ON corsi.tipo = tipi.tipo_id "
			+ "INNER JOIN misure ON corsi.misura = misure.misura_id "
			+ "INNER JOIN societa ON corsi.societa = societa.societa_id "
			+ "WHERE societa.ragione_sociale = :stringa", nativeQuery = true)
	public Page<CorsoDao> findBySocieta(String stringa, Pageable p);
}


