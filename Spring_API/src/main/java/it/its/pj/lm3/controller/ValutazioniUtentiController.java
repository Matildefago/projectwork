package it.its.pj.lm3.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.pj.lm3.dao.ValutazioniUtentiDao;
import it.its.pj.lm3.dto.BaseResponseDto;
import it.its.pj.lm3.dto.ValutazioniUtentiDto;
import it.its.pj.lm3.services.interfaces.ValutazioniUtentiService;

@RestController
@RequestMapping(value = "api/valutazioniUtenti")

public class ValutazioniUtentiController {
	@Autowired
	ValutazioniUtentiService valutazioniUtentiService;

	@GetMapping(produces = "application/json", value = "/valutenti")
	public BaseResponseDto<List<ValutazioniUtentiDao>> selTuttiUtenti() {

		BaseResponseDto<List<ValutazioniUtentiDao>> response = new BaseResponseDto<>();

		List<ValutazioniUtentiDao> dipendenti = valutazioniUtentiService.selTuttiUtenti();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (dipendenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(dipendenti);
		return response;
	}

	@PostMapping(consumes = { "application/json" }, produces = { "application/json" }, value = "/inserisci")
	public BaseResponseDto<ValutazioniUtentiDto> createValUte(@RequestBody ValutazioniUtentiDto valutazioniUtenti) {

		BaseResponseDto<ValutazioniUtentiDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(valutazioniUtentiService.insValUtenti(valutazioniUtenti));
		return response;

	}

	@DeleteMapping(value = "/elimina/{valUtCo}")
	public BaseResponseDto<ValutazioniUtentiDto> deleteValUtCo(@PathVariable("valUtCo") int valUtCo) {

		BaseResponseDto<ValutazioniUtentiDto> response = new BaseResponseDto<>();

		if (!valutazioniUtentiService.findId(valUtCo)) {

			response.setResponse(null);
			response.setMessagge("NON_TROVO_ASSOCIAZIONE");
			response.setResponse(valUtCo);
			return response;
		}

		valutazioniUtentiService.deleteAss(valUtCo);
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(valUtCo);

		return response;
	}

	@GetMapping(value = "/id/{valUtCo}")
	public BaseResponseDto<ValutazioniUtentiDto> selById(@PathVariable("valUtCo") int valUtCo)

	{
		BaseResponseDto<ValutazioniUtentiDto> response = new BaseResponseDto<>();

		ValutazioniUtentiDto val = valutazioniUtentiService.selById(valUtCo);

		if (val == null) {
			response.setResponse(null);
			response.setMessagge("NON_TROVO_LA_VALUTAZIONE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(val);
		return response;

	}

	@GetMapping(value = "/info/{idDip}..{idCors}")
	public BaseResponseDto<ValutazioniUtentiDto> getInfo(@PathVariable("idDip") int idDip,
			@PathVariable("idCors") int idCors)

	{
		BaseResponseDto<ValutazioniUtentiDto> response = new BaseResponseDto<>();

		List<ValutazioniUtentiDto> lista = valutazioniUtentiService.dtoVal(idDip, idCors);

		if (lista == null) {
			response.setResponse(null);
			response.setMessagge("NON_FUNZIA");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(lista);
		return response;

	}

	@PostMapping(value = "/cerca/", produces = "application/json")
	public BaseResponseDto<ArrayList<ValutazioniUtentiDto>> filtra(@RequestBody Map<String, String> value) {
		BaseResponseDto<ArrayList<ValutazioniUtentiDto>> response = new BaseResponseDto<ArrayList<ValutazioniUtentiDto>>();
		response.setResponse(this.valutazioniUtentiService.dtoFilter(value.get("cerca")));

		return response;

	}
	

}
