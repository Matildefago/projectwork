package it.its.pj.lm3.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.pj.lm3.dao.ValutazioniDocentiDao;
import it.its.pj.lm3.dto.BaseResponseDto;
import it.its.pj.lm3.dto.ValutazioniDocentiDto;
import it.its.pj.lm3.services.interfaces.ValutazioniDocentiService;

@RequestMapping(value = "api/valutazioniDocenti")
@RestController
public class ValutazioniDocentiController {
	
	@Autowired
	ValutazioniDocentiService valutazioniDocentiService; 
	
	@GetMapping(produces = "application/json", value = "/valdocenti")
	public BaseResponseDto<List<ValutazioniDocentiDao>> selTuttiDocenti() {

		BaseResponseDto<List<ValutazioniDocentiDao>> response = new BaseResponseDto<>();

		List<ValutazioniDocentiDao> docenti = valutazioniDocentiService.selTuttiDocenti();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (docenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(docenti);
		return response;
	}

	
	@PostMapping(consumes = {"application/json"}, produces = {"application/json"}, value = "/inserisci")
	public BaseResponseDto<ValutazioniDocentiDto> createValDocenti(@RequestBody ValutazioniDocentiDto valutazioniDocenti) {

		BaseResponseDto<ValutazioniDocentiDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(valutazioniDocentiService.insValDocenti(valutazioniDocenti));
		return response;

	}
	
	@DeleteMapping(value = "/elimina/{valDoCo}")
	public BaseResponseDto<ValutazioniDocentiDto> deleteValDoCo(@PathVariable("valDoCo") int valDoCo) {

		BaseResponseDto<ValutazioniDocentiDto> response = new BaseResponseDto<>();

		if (!valutazioniDocentiService.findId(valDoCo)) {

			response.setResponse(null);
			response.setMessagge("NON_TROVO_ASSOCIAZIONE");
			response.setResponse(valDoCo);
			return response;
		}

		valutazioniDocentiService.deleteAss(valDoCo);
		;
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(valDoCo);

		return response;
	}

	@GetMapping(value = "/id/{valDoCo}")
	public BaseResponseDto<ValutazioniDocentiDto> selById(@PathVariable("valDoCo") int valDoCo)

	{
		BaseResponseDto<ValutazioniDocentiDto> response = new BaseResponseDto<>();

		ValutazioniDocentiDto val = valutazioniDocentiService.selById(valDoCo);

		if (val == null) {
			response.setResponse(null);
			response.setMessagge("NON_TROVO_LA_VALUTAZIONE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(val);
		return response;

	}
	
	@GetMapping(value = "/info/{idDoc}..{idCor}")
	public BaseResponseDto<ValutazioniDocentiDto> info(@PathVariable("idDoc") int idDoc, @PathVariable("idCor") int idCor)

	{
		BaseResponseDto<ValutazioniDocentiDto> response = new BaseResponseDto<>();

		List<ValutazioniDocentiDto> val = valutazioniDocentiService.dtoInfo(idDoc, idCor);

		if (val == null) {
			response.setResponse(null);
			response.setMessagge("NON_TROVO_LA_VALUTAZIONE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(val);
		return response;

	}
	@PostMapping(value = "/cerca/", produces = "application/json")
	public BaseResponseDto<ArrayList<ValutazioniDocentiDto>> filtra(@RequestBody Map<String, String> value) {
		BaseResponseDto<ArrayList<ValutazioniDocentiDto>> response = new BaseResponseDto<ArrayList<ValutazioniDocentiDto>>();
		response.setResponse(this.valutazioniDocentiService.dtoFilter(value.get("cerca")));

		return response;

	}
	
	
}
