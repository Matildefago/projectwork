package it.its.pj.lm3.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "corsiDocenti")
@Data
public class CorsiDocentiDao {

	@Id
	@Column(name = "corsiDocentiId")
	private int corsiDocentiId;

	@Column(name = "docente")
	private int docente;

	@Column(name = "interno")
	private int interno;

	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "corso", referencedColumnName = "corsoId")
	@JsonBackReference
	private CorsoDao corso;

}
