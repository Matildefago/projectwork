package it.its.pj.lm3.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.pj.lm3.dao.ValutazioniCorsiDao;
import it.its.pj.lm3.dto.BaseResponseDto;
import it.its.pj.lm3.dto.ValutazioniCorsiDto;
import it.its.pj.lm3.services.interfaces.ValutazioniCorsiService;

@RestController
@RequestMapping(value = "api/valutazioniCorsi")
public class ValutazioniCorsiController {

	@Autowired
	ValutazioniCorsiService valutazioniCorsiService;

	@GetMapping(produces = "application/json", value = "/valcorso")
	public BaseResponseDto<List<ValutazioniCorsiDao>> selTuttiCorsi() {

		BaseResponseDto<List<ValutazioniCorsiDao>> response = new BaseResponseDto<>();

		List<ValutazioniCorsiDao> corsi = valutazioniCorsiService.selTuttiCorsi();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (corsi.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(corsi);
		return response;
	}

	@PostMapping(consumes = { "application/json" }, produces = { "application/json" }, value = "/inserisci")
	public BaseResponseDto<ValutazioniCorsiDto> createValCorso(@RequestBody ValutazioniCorsiDto valutazioniCorsi) {

		BaseResponseDto<ValutazioniCorsiDto> response = new BaseResponseDto<>();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(valutazioniCorsiService.insValCorsi(valutazioniCorsi));
		return response;

	}

	@DeleteMapping(value = "/elimina/{valCoCo}")
	public BaseResponseDto<ValutazioniCorsiDto> deleteValCoCo(@PathVariable("valCoCo") int valCoCo) {

		BaseResponseDto<ValutazioniCorsiDto> response = new BaseResponseDto<>();

		if (!valutazioniCorsiService.findId(valCoCo)) {

			response.setResponse(null);
			response.setMessagge("NON_TROVO_ASSOCIAZIONE");
			response.setResponse(valCoCo);
			return response;
		}

		valutazioniCorsiService.deleteAss(valCoCo);
		;
		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(valCoCo);

		return response;
	}

	@GetMapping(value = "/id/{valCoCo}")
	public BaseResponseDto<ValutazioniCorsiDto> selById(@PathVariable("valCoCo") int valCoCo)

	{
		BaseResponseDto<ValutazioniCorsiDto> response = new BaseResponseDto<>();

		ValutazioniCorsiDto val = valutazioniCorsiService.selById(valCoCo);

		if (val == null) {
			response.setResponse(null);
			response.setMessagge("NON_TROVO_LA_VALUTAZIONE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(val);
		return response;

	}

	@GetMapping(value = "/info/{num}")
	public BaseResponseDto<ValutazioniCorsiDto> info(@PathVariable("num") int num)

	{
		BaseResponseDto<ValutazioniCorsiDto> response = new BaseResponseDto<>();

		List<ValutazioniCorsiDto> lista = valutazioniCorsiService.dtoInfo(num);

		if (lista == null) {
			response.setResponse(null);
			response.setMessagge("NON_TROVO_LA_VALUTAZIONE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(lista);
		return response;

	}

	@PostMapping(value = "/cerca/", produces = "application/json")
	public BaseResponseDto<ArrayList<ValutazioniCorsiDto>> filtra(@RequestBody Map<String, String> value) {
		BaseResponseDto<ArrayList<ValutazioniCorsiDto>> response = new BaseResponseDto<ArrayList<ValutazioniCorsiDto>>();
		response.setResponse(this.valutazioniCorsiService.dtoFilter(value.get("cerca")));

		return response;

	}

}
