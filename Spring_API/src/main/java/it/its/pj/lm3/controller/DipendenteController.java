package it.its.pj.lm3.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.pj.lm3.dto.BaseResponseDto;
import it.its.pj.lm3.dto.DipendenteDto;
import it.its.pj.lm3.services.interfaces.DipendenteService;

@RestController
@RequestMapping(value = "api/dipendenti")
public class DipendenteController {
	
	@Autowired
	DipendenteService dipendenteService;
	
	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<DipendenteDto>> selTutti() {

		BaseResponseDto<List<DipendenteDto>> response = new BaseResponseDto<>();

		List<DipendenteDto> dipendenti = dipendenteService.parseDto();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (dipendenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(dipendenti);
		return response;
	}
	
	@GetMapping(value = "/id/{dipendentiId}")
	public BaseResponseDto<DipendenteDto> selById(@PathVariable("dipendentiId") int dipendentiId)

	{
		BaseResponseDto<DipendenteDto> response = new BaseResponseDto<>();

		DipendenteDto dipendente = dipendenteService.selById(dipendentiId);

		if (dipendente == null) {
			response.setResponse(null);
			response.setMessagge("NON_TROVO_IL_DIPENDENTE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(dipendente);
		return response;

	}
	
	@GetMapping(value = "/listaDocenti")
	public BaseResponseDto<List<DipendenteDto>> doc(){
		BaseResponseDto<List<DipendenteDto>> response = new BaseResponseDto<>();

		List<DipendenteDto> docenti = dipendenteService.docentiInterni();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (docenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(docenti);
		return response;
	}
	
	@GetMapping(value = "/alunni", produces = "application/json")
	public BaseResponseDto<List<DipendenteDto>> selAlunni() {

		BaseResponseDto<List<DipendenteDto>> response = new BaseResponseDto<>();

		List<DipendenteDto> dipendenti = dipendenteService.allAlunni();

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (dipendenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(dipendenti);
		return response;
	}
	
	@GetMapping(value = "/{pagine}...{quantita}" , produces = "application/json")
	public BaseResponseDto<List<DipendenteDto>> selTuttiPage(@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<List<DipendenteDto>> response = new BaseResponseDto<>();

		List<DipendenteDto> dipendenti = dipendenteService.parseDtoPage(pagine, quantita);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (dipendenti.isEmpty()) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(dipendenti);
		return response;
	}
	
	@GetMapping(value = "/getPages/{pagine}...{quantita}" , produces = "application/json")
	public BaseResponseDto<Integer> getPages(@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<Integer> response = new BaseResponseDto<>();

		Integer numero = dipendenteService.getPages(pagine, quantita);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (numero == 0) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(numero);
		return response;
	}

	@PostMapping(value = "/cerca/{pagina}...{quantita}", produces = "application/json")
	public BaseResponseDto<ArrayList<DipendenteDto>> filtra(
			@RequestBody Map<String, String> value,
			@PathVariable("pagina") int pagina,
			@PathVariable("quantita") int quantita
	){
		BaseResponseDto<ArrayList<DipendenteDto>> response = new BaseResponseDto<ArrayList<DipendenteDto>>();
		
		response.setResponse(this.dipendenteService.dtoFilter(value.get("cerca"), pagina, quantita));
		
		return response;
	}
	
	@PostMapping(value = "/getPagesFilter/{pagina}...{quantita}" , produces = "application/json")
	public BaseResponseDto<Integer> getPages(@RequestBody Map<String,String> dipendente, @PathVariable("pagina") int pagina, @PathVariable("quantita") int quantita) {

		BaseResponseDto<Integer> response = new BaseResponseDto<>();

		Integer numero = dipendenteService.getPagesFilter(dipendente.get("cerca"), pagina, quantita);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (numero == 0) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(numero);
		return response;
	}
	
	@GetMapping(value = "/totDoppioni")
	public BaseResponseDto<DipendenteDto> doppioniTotali()

	{
		BaseResponseDto<DipendenteDto> response = new BaseResponseDto<>();

		List<DipendenteDto> lista = dipendenteService.dtoDoppioni();

		if (lista == null) {
			response.setResponse(null);
			response.setMessagge("NON_TROVO_IL_DIPENDENTE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(lista);
		return response;

	}
	
	@GetMapping(value = "/doppioni/{pagine}..{quantita}")
	public BaseResponseDto<DipendenteDto> doppioni(@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita)

	{
		BaseResponseDto<DipendenteDto> response = new BaseResponseDto<>();

		List<DipendenteDto> lista = dipendenteService.dtoDoppPages(pagine, quantita);

		if (lista == null) {
			response.setResponse(null);
			response.setMessagge("NON_TROVO_IL_DIPENDENTE");
			return response;
		}

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");
		response.setResponse(lista);
		return response;

	}
	
	@GetMapping(value = "/getPagesDopp/{pagine}...{quantita}" , produces = "application/json")
	public BaseResponseDto<Integer> getDoppPages(@PathVariable("pagine") int pagine, @PathVariable("quantita") int quantita) {

		BaseResponseDto<Integer> response = new BaseResponseDto<>();

		Integer numero = dipendenteService.numberDopp(pagine,quantita);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (numero == 0) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(numero);
		return response;
	}
	@PostMapping(value = "/cercaInt/{pagina}...{quantita}", produces = "application/json")
	public BaseResponseDto<ArrayList<DipendenteDto>> filtraInt(
			@RequestBody Map<String, String> value,
			@PathVariable("pagina") int pagina,
			@PathVariable("quantita") int quantita
	){
		BaseResponseDto<ArrayList<DipendenteDto>> response = new BaseResponseDto<ArrayList<DipendenteDto>>();
		
		response.setResponse(this.dipendenteService.dtoFilterPage((value.get("cerca")), pagina, quantita));
		
		return response;
	}
	
	@PostMapping(value = "/getPagesFilterInt/{pagina}...{quantita}" , produces = "application/json")
	public BaseResponseDto<Integer> getPagesFilterInt(@RequestBody Map<String,String> dipendente, @PathVariable("pagina") int pagina, @PathVariable("quantita") int quantita) {

		BaseResponseDto<Integer> response = new BaseResponseDto<>();

		Integer numero = dipendenteService.numFilter(dipendente.get("cerca"), pagina, quantita);

		response.setTimestamp(new Date());
		response.setStatus(HttpStatus.OK.value());
		response.setMessagge("SERVIZIO_ELABORATO_CORRETTAMENTE");

		if (numero == 0) {
			response.setResponse(null);
			return response;
		}

		response.setResponse(numero);
		return response;
	}
	
	
}
