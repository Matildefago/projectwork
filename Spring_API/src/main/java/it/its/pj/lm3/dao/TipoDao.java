package it.its.pj.lm3.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "tipi")
@Data
public class TipoDao {
	
	@Id
	@Column(name = "tipoId")
	private int tipoId;
	
	@Column(name = "descrizione")
	private String descrizione;

}
