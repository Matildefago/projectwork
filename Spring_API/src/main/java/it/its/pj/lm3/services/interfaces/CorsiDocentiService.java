package it.its.pj.lm3.services.interfaces;

import java.util.List;

import it.its.pj.lm3.dao.CorsiDocentiDao;
import it.its.pj.lm3.dto.CorsiDocentiDto;
import it.its.pj.lm3.dto.DipendenteDto;

public interface CorsiDocentiService {

	public List<CorsiDocentiDao> selTuttiDoc();

	public boolean insCorsiDocenti(CorsiDocentiDto corsiDocenti);
	
	public void dtoToDao(CorsiDocentiDto dto, CorsiDocentiDao dao);
	
	public List<CorsiDocentiDao> getAllDoc(int num);
	
	public List<CorsiDocentiDto> getDtoDoc(int num);
	
	public List<CorsiDocentiDao> getCorsiEsterni(int num);
	
	public List<CorsiDocentiDto> getDtoEsterni(int num);
	
	public List<CorsiDocentiDao> getCorsiInterni(int num);
	
	public List<CorsiDocentiDto> getDtoInterni(int num);
	
	public List<CorsiDocentiDao> pagesEsterni(int pagine, int quantita);
	
	public List<CorsiDocentiDto> dtoPagesEsterni(int pagine, int quantita);
	
	public int getNumberEsterni(int pagine, int quantita);
	
	public List<CorsiDocentiDao> pagesInterni(int pagine, int quantita);
	
	public List<CorsiDocentiDto> dtoPagesInterni(int pagine, int quantita);
	
	public int getNumberInterni(int pagine, int quantita);
	
	public void delete(int id);
	
	public boolean trovaId(int id);
	
	public List<CorsiDocentiDto> dtoAll();
	
	public List<DipendenteDto> filtraDocInterni(String valore, int pagina, int q);
	
	public int dtoFilterPages(int quantita, String filter);
	
	
	
	
	
} 
