package it.its.pj.lm3.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.pj.lm3.dao.TipologiaDao;
import it.its.pj.lm3.dto.TipologiaDto;
import it.its.pj.lm3.repository.TipologiaRepository;
import it.its.pj.lm3.services.interfaces.TipologiaService;

@Service
@Transactional
public class TipologiaServiceImpl implements TipologiaService{
	
	@Autowired
	TipologiaRepository tipologiaRepository;

	@Override
	public List<TipologiaDto> parseDto() {
		List<TipologiaDto> dto = new ArrayList<TipologiaDto>();
		List<TipologiaDao> dao = this.selTutti();
		for (TipologiaDao d : dao) {
			dto.add(new TipologiaDto(d.getTipologiaId(), d.getDescrizione()));
		}
		return dto;
	}

	@Override
	public List<TipologiaDao> selTutti() {
		return tipologiaRepository.findAll();
	}

}