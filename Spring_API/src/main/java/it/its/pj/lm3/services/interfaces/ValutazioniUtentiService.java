package it.its.pj.lm3.services.interfaces;

import java.util.List;

import it.its.pj.lm3.dao.ValutazioniUtentiDao;
import it.its.pj.lm3.dto.ValutazioniUtentiDto;

public interface ValutazioniUtentiService {

	public List<ValutazioniUtentiDao> selTuttiUtenti();

	public void dtoToDao(ValutazioniUtentiDto dto, ValutazioniUtentiDao dao);

	public boolean insValUtenti(ValutazioniUtentiDto valutazioniCorsi);

	public boolean findId(int id);

	public void deleteAss(int valUtCo);

	public ValutazioniUtentiDto selById(int valUtCo);

	public void daoToDto(ValutazioniUtentiDto dto, ValutazioniUtentiDao dao);
	
	public List<ValutazioniUtentiDao> getVal(int idDip, int idCors);
	
	public List<ValutazioniUtentiDto> dtoVal(int idDip, int idCors);
	
	//Filter
	 public List<ValutazioniUtentiDao> gFilter(String filter);
	 
	 public List<ValutazioniUtentiDto> dtoFilter(String filter);
	 
//	 public int getPagesFilter(String filter,int pagine, int quantita);

}
