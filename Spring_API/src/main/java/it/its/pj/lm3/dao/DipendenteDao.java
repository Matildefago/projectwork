package it.its.pj.lm3.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "dipendenti")
@Data
public class DipendenteDao {
	
	@Id
	@Column(name = "dipendenteId")
	private int dipendenteId;
	
	@Column(name = "matricola")
	private int matricola;
	
	@Column(name = "cognome")
	private String cognome;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "sesso")
	private String sesso;
	//DUBBIO
	@Column(name = "dataNascita")
	private Date dataNascita;
	
	@Column(name = "luogoNascita")
	private String luogoNascita;
	
	@Column(name = "statoCivile")
	private String statoCivile;
	
	@Column(name = "conseguitoPresso")
	private String conseguitoPresso;
	//DUBBIO
	@Column(name = "annoConseguimento")
	private int annoConsenguimento;
	
	@Column(name = "tipoDipendente")
	private String tipoDipendente;
	
	@Column(name = "qualifica")
	private String qualifica;
	
	@Column(name = "livello")
	private String livello;
	
	@Column(name = "dataAssunzione")
	private Date dataAssunzione;
	
	@Column(name = "responsabileRisorsa")
	private int responsabileRisorsa;
	
	@Column(name = "responsabileArea")
	private int responsabileArea;
	
	@Column(name = "dataFineRapporto")
	private Date dataFineRapporto;
	
	@Column(name = "dataScadenzaContratto")
	private Date dataScadenzaContratto;
	
	@Column(name = "titoloStudio")
	private String titoloStudio;
	
	//CHIAVE ESTERNA DA CONFIGURARE QUANDO AVREMO LA TABELLA SOCIETA
	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "societa", referencedColumnName = "societaId")
	@JsonBackReference
	private SocietaDao societa;
}
