package it.its.pj.lm3.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.its.pj.lm3.dao.CorsoDao;
import it.its.pj.lm3.dto.CorsoDto;
import it.its.pj.lm3.repository.CorsoRepository;
import it.its.pj.lm3.services.interfaces.CorsoService;

@Service
@Transactional
public class CorsoServiceImpl implements CorsoService {

	@Autowired
	CorsoRepository corsoRepository;

	@Override
	public List<CorsoDao> selTuttiPage(int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina, quantita);
		Page<CorsoDao> resp = corsoRepository.findAll(p);
//		int pages = resp.getTotalPages(); useless
		List<CorsoDao> res = new ArrayList<CorsoDao>();
		for (CorsoDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<CorsoDto> parseDtoPage(int pagina, int quantita) {
		List<CorsoDto> dto = new ArrayList<CorsoDto>();
		List<CorsoDao> dao = this.selTuttiPage(pagina, quantita);
		for (CorsoDao d : dao) {
			dto.add(new CorsoDto(d.getCorsoId(), d.getDescrizione(), d.getEdizione(), d.getPrevisto(), d.getErogato(),
					d.getDurata(), d.getNote(), d.getLuogo(), d.getEnte(), d.getDataErogazione(), d.getDataChiusura(),
					d.getDataCensimento(), d.getInterno(), d.getSocieta(), d.getTipo(), d.getTipologia(),
					d.getMisura()));
		}
		return dto;
	}

	@Override
	public CorsoDto selById(int corsoId) {
		try {
			CorsoDto dto = new CorsoDto();
			daoToDto(dto, corsoRepository.getOne(corsoId));
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void delCorso(int corsoId) {
		corsoRepository.deleteById(corsoId);

	}

	@Override
	public boolean insCorso(CorsoDto corso) {
		CorsoDao dao = new CorsoDao();
		dtoToDao(corso, dao);
		try {
			corsoRepository.saveAndFlush(dao);

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	@Override
	public boolean trovaId(int corsoId) {
		boolean trovato = false;
		List<CorsoDto> lista = this.parseDto();
		for (CorsoDto d : lista) {
			if (d.getCorsoId() == corsoId) {
				trovato = true;
				break;
			}
		}
		return trovato;
	}

	@Override
	public void dtoToDao(CorsoDto dto, CorsoDao dao) {
		dao.setDescrizione(dto.getDescrizione() != null ? dto.getDescrizione() : dao.getDescrizione());
		dao.setEdizione(dto.getEdizione() != null ? dto.getEdizione() : dao.getEdizione());
		dao.setPrevisto(dto.getPrevisto() != 0 ? dto.getPrevisto() : dto.getPrevisto());
		dao.setErogato(dto.getErogato() != 0 ? dto.getErogato() : dto.getErogato());
		dao.setDurata(dto.getDurata() != 0 ? dto.getDurata() : dao.getDurata());
		dao.setNote(dto.getNote() != null ? dto.getNote() : dao.getNote());
		dao.setLuogo(dto.getLuogo() != null ? dto.getLuogo() : dao.getLuogo());
		dao.setEnte(dto.getEnte() != null ? dto.getEnte() : dao.getEnte());
		dao.setDataErogazione(dto.getDataErogazione() != null ? dto.getDataErogazione() : dao.getDataErogazione());
		dao.setDataChiusura(dto.getDataChiusura() != null ? dto.getDataChiusura() : dao.getDataChiusura());
		dao.setDataCensimento(dto.getDataCensimento() != null ? dto.getDataCensimento() : dao.getDataCensimento());
		dao.setInterno(dto.getInterno() != 0 ? dto.getInterno() : dto.getInterno());
		dao.setSocieta(dto.getSocieta() != null ? dto.getSocieta() : dao.getSocieta());
		dao.setTipo(dto.getTipo() != null ? dto.getTipo() : dao.getTipo());
		dao.setTipologia(dto.getTipologia() != null ? dto.getTipologia() : dao.getTipologia());
		dao.setMisura(dto.getMisura() != null ? dto.getMisura() : dao.getMisura());

	}

	@Override
	public void daoToDto(CorsoDto dto, CorsoDao dao) {
		dto.setCorsoId(dao.getCorsoId());
		dto.setDescrizione(dao.getDescrizione());
		dto.setPrevisto(dao.getPrevisto());
		dto.setErogato(dao.getErogato());
		dto.setDurata(dao.getDurata());
		dto.setNote(dao.getNote());
		dto.setLuogo(dao.getLuogo());
		dto.setEnte(dao.getEnte());
		dto.setDataErogazione(dao.getDataErogazione());
		dto.setDataChiusura(dao.getDataChiusura());
		dto.setDataCensimento(dao.getDataCensimento());
		dto.setInterno(dao.getInterno());
		dto.setSocieta(dao.getSocieta());
		dto.setTipo(dao.getTipo());
		dto.setTipologia(dao.getTipologia());
		dto.setMisura(dao.getMisura());

	}

	@Override
	public boolean replaceCorso(CorsoDto corso) {
		CorsoDao dao = corsoRepository.getOne(corso.getCorsoId());
		dtoToDao(corso, dao);
		try {
			corsoRepository.save(dao);

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	@Override
	public List<CorsoDto> parseDto() {
		List<CorsoDto> dto = new ArrayList<CorsoDto>();
		List<CorsoDao> dao = this.selTutti();
		for (CorsoDao d : dao) {
			dto.add(new CorsoDto(d.getCorsoId(), d.getDescrizione(), d.getEdizione(), d.getPrevisto(), d.getErogato(),
					d.getDurata(), d.getNote(), d.getLuogo(), d.getEnte(), d.getDataErogazione(), d.getDataChiusura(),
					d.getDataCensimento(), d.getInterno(), d.getSocieta(), d.getTipo(), d.getTipologia(),
					d.getMisura()));
		}
		return dto;
	}

	@Override
	public List<CorsoDao> selTutti() {
		return corsoRepository.findAll();
	}

	@Override
	public int getPages(int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita);
		Page<CorsoDao> resp = corsoRepository.findAll(p);
		int pages = resp.getTotalPages();
		return pages;
	}

	@Override
	public List<CorsoDao> gFilter(String filter, int pagina, int quantita) {

		Pageable p = PageRequest.of(pagina, quantita);
		Page<CorsoDao> resp = corsoRepository.findByAll(filter, p);
//		System.out.println("test: " + resp.toString());

		List<CorsoDao> res = new ArrayList<CorsoDao>();
		for (CorsoDao d : resp) {
			res.add(d);
		}
		return res;
	}

	@Override
	public List<CorsoDto> dtoFilter(String filter, int pagina, int quantita) {
		List<CorsoDto> dto = new ArrayList<CorsoDto>();
		List<CorsoDao> dao = this.gFilter(filter, pagina, quantita);
		
		for (CorsoDao d : dao) {
			CorsoDto temp = new CorsoDto();
			temp.setCorsoId(d.getCorsoId());
			temp.setSocieta(d.getSocieta());
			temp.setTipo(d.getTipo());
			temp.setTipologia(d.getTipologia());
			temp.setDescrizione(d.getDescrizione());
			temp.setEdizione(d.getEdizione());
			temp.setPrevisto(d.getPrevisto());
			temp.setErogato(d.getErogato());
			temp.setDurata(d.getDurata());
			temp.setMisura(d.getMisura());
			temp.setNote(d.getNote());
			temp.setLuogo(d.getLuogo());
			temp.setEnte(d.getEnte());
			temp.setDataErogazione(d.getDataErogazione());
			temp.setDataChiusura(d.getDataChiusura());
			temp.setDataCensimento(d.getDataCensimento());
			temp.setInterno(d.getInterno());
					
			dto.add(temp);
				
			}
			
		return dto;
	}

	@Override
	public int getPagesFilter(String filter, int pagine, int quantita) {
		Pageable p = PageRequest.of(pagine, quantita );
		Page<CorsoDao> resp = corsoRepository.findByAll(filter, p);
		int pages = resp.getTotalPages(); 
			
		return pages;
	}

}
