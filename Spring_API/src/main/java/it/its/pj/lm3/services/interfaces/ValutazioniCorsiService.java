package it.its.pj.lm3.services.interfaces;

import java.util.List;

import it.its.pj.lm3.dao.ValutazioniCorsiDao;
import it.its.pj.lm3.dto.ValutazioniCorsiDto;

public interface ValutazioniCorsiService {

	public List<ValutazioniCorsiDao> selTuttiCorsi();

	public void dtoToDao(ValutazioniCorsiDto dto, ValutazioniCorsiDao dao);

	public boolean insValCorsi(ValutazioniCorsiDto valutazioniCorsi);

	public boolean findId(int id);

	public void deleteAss(int valCoCo);

	public ValutazioniCorsiDto selById(int valCoCo);

	public void daoToDto(ValutazioniCorsiDto dto, ValutazioniCorsiDao dao);

	public List<ValutazioniCorsiDao> getInfo(int num);

	public List<ValutazioniCorsiDto> dtoInfo(int num);

	// Filter
	public List<ValutazioniCorsiDao> gFilter(String filter);

	public List<ValutazioniCorsiDto> dtoFilter(String filter);

}
