import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TableModule } from "primeng/table";
import { ButtonModule } from "primeng/button";
import { DialogModule } from "primeng/dialog";
import { TabViewModule } from "primeng/tabview";
import { ScrollPanelModule } from "primeng/scrollpanel";

import { DocentiRoutingModule } from "./docenti-routing.module";
import { DocentiComponent } from "./docenti/docenti.component";
import { NewComponent } from "./docenti/new/new.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { EditComponent } from "./docenti/edit/edit.component";
import { InfoDocentiEstComponent } from "./docenti/info-docenti-est/info-docenti-est.component";
import { InputTextModule } from "primeng/inputtext";
import { InfoDocentiIntComponent } from './docenti/info-docenti-int/info-docenti-int.component';

@NgModule({
  declarations: [
    DocentiComponent,
    NewComponent,
    EditComponent,
    InfoDocentiEstComponent,
    InfoDocentiIntComponent
  ],
  imports: [
    CommonModule,
    DocentiRoutingModule,
    TableModule,
    ButtonModule,
    DialogModule,
    FormsModule,
    ScrollPanelModule,
    ReactiveFormsModule,
    TabViewModule,
    InputTextModule,
  ],
})
export class DocentiModule {}
