import {
  Component,
  Input,
  AfterViewInit,
  AfterContentInit,
  AfterViewChecked,
  AfterContentChecked,
  Output,
  EventEmitter,
} from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { DocentiApiService } from "../../../../api/docenti-api.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.css"],
})
export class EditComponent implements AfterContentChecked {
  @Input()
  public display1: boolean;

  @Input()
  set soggetto(item: any) {
    this._soggetto = item;
    if (item) {
      this.buildForm();
    }
  }

  get soggetto() {
    return this._soggetto;
  }

  @Output()
  onDialogClose: EventEmitter<any> = new EventEmitter<any>();

  private _soggetto: any;
  public formGroup: FormGroup;
  constructor(
    public fb: FormBuilder,
    public docenti: DocentiApiService,
    public router: Router
  ) {}

  ngAfterContentChecked(): void {
    //console.log("INIT");
  }

  buildForm() {
    this.formGroup = this.fb.group({
      // matricola: [this.soggetto.matricola, [Validators.required]],
      // nome: [this.soggetto.nome, [Validators.required]],
      // cognome: [this.soggetto.cognome, [Validators.required]],
      titolo: [this.soggetto.titolo, [Validators.required]],
      ragioneSociale: [this.soggetto.ragioneSociale, [Validators.required]],
      indirizzo: [this.soggetto.indirizzo],
      localita: [this.soggetto.localita, [Validators.required]],
      provincia: [this.soggetto.provincia, [Validators.required]],
      nazione: [this.soggetto.nazione, [Validators.required]],
      telefono: [this.soggetto.telefono],
      fax: [this.soggetto.fax],
      partitaIva: [this.soggetto.partitaIva],
      referente: [this.soggetto.referente],
    });
  }

  conferma() {
    const soggetto = this.formGroup.value;
    soggetto["docenteId"] = this.soggetto.docenteId;
    console.log(soggetto.docenteId);

    this.docenti.replace(soggetto).subscribe((resp) => {
      this.exit();
    });

    /*  const contenitore = this.formGroup.value;
    this.docenti.replace(contenitore).subscribe((resp) => {
     
      this.docenti = resp;
      console.log(resp);
      
    }); */
  }

  exit() {
    this.display1 = false;
    this.onDialogClose.emit(this.display1);
  }
}
