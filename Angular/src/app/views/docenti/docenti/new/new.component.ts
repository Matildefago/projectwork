import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { DocentiApiService } from "../../../../api/docenti-api.service";

@Component({
  selector: "app-new",
  templateUrl: "./new.component.html",
  styleUrls: ["./new.component.css"],
})
export class NewComponent implements OnInit {
  @Input()
  public display: boolean;

  @Output()
  onClick: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  user: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  onDialogClose: EventEmitter<any> = new EventEmitter<any>();

  public formGroup: FormGroup;
  constructor(
    public fb: FormBuilder,
    public docenti: DocentiApiService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.formGroup = this.fb.group({
      // matricola: ["", [Validators.required]],
      // nome: ["", [Validators.required]],
      // cognome: ["", [Validators.required]],
      titolo: ["", [Validators.required]],
      ragioneSociale: ["", [Validators.required]],
      indirizzo: [""],
      localita: ["", [Validators.required]],
      provincia: ["", [Validators.required]],
      nazione: ["", [Validators.required]],
      telefono: [""],
      fax: [""],
      partitaIva: [""],
      referente: [""],
    });
  }

  conferma() {
    this.docenti.add(this.formGroup.value).subscribe((resp) => {
      this.display = false;
      this.onClick.emit(this.display);
      this.user.emit(this.formGroup.value);
      this.buildForm();
      console.log("questo è il boolean", this.display);
    });
  }

  exit() {
    this.display = false;
    this.onDialogClose.emit(this.display);
  }
}
