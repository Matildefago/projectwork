import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoDocentiEstComponent } from './info-docenti-est.component';

describe('InfoDocentiEstComponent', () => {
  let component: InfoDocentiEstComponent;
  let fixture: ComponentFixture<InfoDocentiEstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoDocentiEstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoDocentiEstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
