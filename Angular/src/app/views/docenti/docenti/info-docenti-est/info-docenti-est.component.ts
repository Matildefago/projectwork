import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { CorsiApiService } from "../../../../api/corsi-api.service";
import { CorsiDocentiService } from "../../../../api/corsi-docenti.service";
import { threadId } from "worker_threads";

@Component({
  selector: "app-info-docenti-est",
  templateUrl: "./info-docenti-est.component.html",
  styleUrls: ["./info-docenti-est.component.css"],
})
export class InfoDocentiEstComponent implements OnInit {
  @Input()
  public display3: boolean;

  @Output()
  onClick: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onDialogClose: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  set soggetto(item: any) {
    this._soggetto = item;
    if (item) {
      this.getCorso();
      this.creaColonne();
    }
  }

  get soggetto() {
    return this._soggetto;
  }

  colsDocente: any[];
  private _soggetto: any;
  corsi: any[];
  colsCorsi: any[];
  corsoAssociato: any[];

  constructor(
    public corsiService: CorsiApiService,
    public corsiDocenti: CorsiDocentiService
  ) {}

  ngOnInit(): void {}

  creaColonne() {
    this.colsDocente = [
      { field: "titolo", header: "Titolo" },
      { field: "ragioneSociale", header: "Ragione Sociale" },
      { field: "indirizzo", header: "Indirizzo" },
      { field: "localita", header: "Località" },
      { field: "provincia", header: "Provincia" },
      { field: "nazione", header: "Nazione" },
      { field: "telefono", header: "Telefono" },
      { field: "fax", header: "Fax" },
      { field: "partitaIva", header: "Partita Iva" },
      { field: "referente", header: "Referente" },
    ];

    this.colsCorsi = [
      { field: "descrizione", header: "Descrizione" },
      { field: "ente", header: "Ente" },
      { field: "edizione", header: "Edizione" },
      { field: "luogo", header: "Luogo" },
    ];
  }

  getCorso() {
    this.corsi = [];
    this.corsiDocenti
      .getCorsiByEsterno(this.soggetto.docenteId)
      .subscribe((resp) => {
        this.corsi = resp.response;
        if (this.corsi.length > 0) {
          this.corsoAssociato = [];
          this.corsi.forEach((element) => {
            this.corsoAssociato.push(element.corso);
          });

          console.log("CORSO", this.corsoAssociato[0]);
        } else {
          this.corsi = null;
          this.corsoAssociato = null;
        }
      });
  }

  exit() {
    this.display3 = false;

    this.onDialogClose.emit(this.display3);
  }
}
