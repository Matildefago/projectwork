import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TableModule } from "primeng/table";
import { ButtonModule } from "primeng/button";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DialogModule } from "primeng/dialog";
import { TabViewModule } from "primeng/tabview";
import { PaginatorModule } from "primeng/paginator";
import { InputTextModule } from "primeng/inputtext";
import { ScrollPanelModule } from "primeng/scrollpanel";

import { DocentiRoutingModule } from "./docenti-routing.module";
import { DocentiComponent } from "./docenti.component";
import { EditComponent } from "./edit/edit.component";
import { NewComponent } from "./new/new.component";
import { InfoDocentiIntComponent } from "./info-docenti-int/info-docenti-int.component";
import { InfoDocentiEstComponent } from "./info-docenti-est/info-docenti-est.component";

@NgModule({
  declarations: [
    DocentiComponent,
    EditComponent,
    NewComponent,
    InfoDocentiIntComponent,
    InfoDocentiEstComponent,
  ],
  imports: [
    CommonModule,
    DocentiRoutingModule,
    ScrollPanelModule,
    TableModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    DialogModule,
    TabViewModule,
    PaginatorModule,
    InputTextModule,
  ],
})
export class DocentiModule {}
