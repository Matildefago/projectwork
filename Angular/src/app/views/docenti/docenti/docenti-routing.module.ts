import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { DocentiComponent } from "./docenti.component";
import { NewComponent } from "./new/new.component";

const routes: Routes = [
  { path: "", component: DocentiComponent },
  { path: "docenti/new", component: NewComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocentiRoutingModule {}
