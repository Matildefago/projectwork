import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoDocentiIntComponent } from './info-docenti-int.component';

describe('InfoDocentiIntComponent', () => {
  let component: InfoDocentiIntComponent;
  let fixture: ComponentFixture<InfoDocentiIntComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoDocentiIntComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoDocentiIntComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
