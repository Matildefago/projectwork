import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { CorsiDocentiService } from '../../../../api/corsi-docenti.service';

@Component({
  selector: "app-info-docenti-int",
  templateUrl: "./info-docenti-int.component.html",
  styleUrls: ["./info-docenti-int.component.css"],
})
export class InfoDocentiIntComponent implements OnInit {
  @Input()
  public display2: boolean;
  @Output()
  onClick: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  onDialogClose: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  set soggetto(item: any) {
    this._soggetto = item;
    if (item) {
      this.date();
     this.build();
     this.getCorso();
    }
  }

  get soggetto() {
    return this._soggetto;
  }

  _soggetto: any;
  ragioneSociale: any;
  colsAlunni: any [];
  colsCorso: any[];
  corsi: any[];
  corsoInterno: any[];
  data: any;
  dataN: String;
  dataF: String;
  dataA: String;
  dataS:String;

  constructor(public corsiDocService: CorsiDocentiService) {}

  ngOnInit(): void {}

  date(){
    this.data = new Date(this.soggetto.dataNascita);
    if(this.data.getDate() == 1){
      this.dataN = this.data.getDate() + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }else{
      this.dataN = (this.data.getDate()-1) + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }

    this.data = new Date(this.soggetto.dataAssunzione);
    if(this.data.getDate() == 1){
      this.dataA = this.data.getDate() + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }else{
      this.dataA = (this.data.getDate()-1) + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }

    this.data = new Date(this.soggetto.dataScadenzaContratto);
    if(this.data.getDate() == 1){
      this.dataS = this.data.getDate() + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }else{
      this.dataS = (this.data.getDate()-1) + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }

    this.data = new Date(this.soggetto.dataFineRapporto);
    if(this.data.getDate() == 1){
      this.dataF = this.data.getDate() + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }else{
      this.dataF = (this.data.getDate()-1) + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }
  }

  exit() {
    this.display2 = false;
    this.onDialogClose.emit(this.display2);
  }

  stringSoc(soggetto: any): String{
    if(!soggetto){
      return;
    }
      this.ragioneSociale = JSON.stringify(soggetto.societa.ragioneSociale);
      return this.ragioneSociale.replace(/"/g, '');
    }

  build() {
    this.colsAlunni = [
      { field: "matricola", header: "Matricola" },
      { field: "nome", header: "Nome" },
      { field: "cognome", header: "Cognome" },
      { field: "sesso", header: "Sesso" },
      { field: "luogoNascita", header: "Luogo di Nascita" },
      { field: "statoCivile", header: "Stato Civile" },
      { field: "titoloStudio", header: "Titolo" },
      { field: "qualifica", header: "Qualifica" },
      { field: "conseguitoPresso", header: "Conseguito Presso" },
      { field: "responsabileRisorsa", header: "Responsabile Risorsa" },
      { field: "responsabileArea", header: "Responsabile Area" },
    ];

    this.colsCorso = [
      { field: "descrizione", header: "Descrizione" },
      { field: "ente", header: "Ente" },
      { field: "edizione", header: "Edizione" },
      { field: "luogo", header: "Luogo" },
    ];
  }

  getCorso(){
    this.corsi = [];
    this.corsiDocService.getCorsiByInterno(this.soggetto.dipendenteId).subscribe((resp)=>{
      this.corsi = resp.response;
      if(this.corsi.length > 0){
        this.corsoInterno = [];
        this.corsi.forEach(element => {
          this.corsoInterno.push(element.corso);
        });
      
        console.log("CORSO" , this.corsoInterno[0]);
      }
      else{
        this.corsoInterno = null;
      }
    })
  }
}
