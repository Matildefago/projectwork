import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  AfterContentChecked,
} from "@angular/core";
import { DocentiApiService } from "../../../api/docenti-api.service";
import { ApiService } from "../../../api/api.service";
import { Router } from "@angular/router";
import * as $ from "jquery";
import { FormGroup, FormBuilder } from "@angular/forms";
import { CorsiDocentiService } from "../../../api/corsi-docenti.service";
import { AlunniApiService } from "../../../api/alunni-api.service";

@Component({
  selector: "app-docenti",
  templateUrl: "./docenti.component.html",
  styleUrls: ["./docenti.component.css"],
})
export class DocentiComponent implements OnInit {
  @Input()
  utente: any;

  @Output()
  onClick: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onEdit: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onSend: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onInfo: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onPageChange: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onDialogCloseConf: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  public displayConf: boolean;

  public display: boolean = false;
  public display1: boolean = false;
  public display2: boolean = false;
  public display3: boolean = false;
  colsInterni: any[];
  colsEsterni: any[];
  public lista: any[];
  public lista2: any[];
  public selectedItem: any;
  public id: number;
  public soggetto: any;
  public quantita: number = 8;
  public pagine: number = 0;
  public numeroPagine: any;
  public formGroup: FormGroup;
  public cerca: any;
  public filtro: boolean;
  public filtroInt: boolean;
  public listaTot: any[];
  numPageDocInt: any;
  assDocInt: any;
  listaDocInterni: any[];
  pagineDocInt: any = 0;
  quantitaDocInt: any = 5;
  docenteInt: any;
  totaleDocentiInt: any;
  dipendenti: any[] = [];
  associazioni: any[];

  constructor(
    public docentiService: DocentiApiService,
    public apiCorsiDocenti: CorsiDocentiService,
    public api: ApiService,
    public router: Router,
    public fb: FormBuilder,
    public alunniService: AlunniApiService
  ) {}

  ngOnInit(): void {
    this.buildForm();
    this.colsEsterni = [
      { field: "titolo", header: "Titolo" },
      { field: "ragioneSociale", header: "Ragione Sociale" },
      { field: "localita", header: "Località" },
      { field: "referente", header: "Referente" },
    ];

    this.colsInterni = [
      { field: "matricola", header: "Matricola" },
      { field: "nome", header: "Nome" },
      { field: "cognome", header: "Cognome" },
      { field: "livello", header: "Livello" },
    ];

    this.loadPage(this.pagine, this.quantita);
    this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);
    this.totDocenti();
    this.totDocInt();
  }

  totDocenti() {
    this.docentiService.getAll().subscribe((resp) => {
      this.listaTot = resp.response;
      // console.log("num pagine", this.numeroPagine);
    });
  }

  load() {
    this.docentiService.getAll().subscribe((resp) => {
      this.lista = resp.response;
    });

    this.docentiService.getAllDip().subscribe((resp) => {
      this.lista2 = resp.response;
      // console.log(resp.response);
    });
  }

  buildForm() {
    this.formGroup = this.fb.group({
      cerca: [""],
    });
  }

  loadPage(pagine: number, quantita: number) {
    this.docentiService.getNumberOfPage(pagine, quantita).subscribe((resp) => {
      this.numeroPagine = resp.response;
    });

    this.docentiService.getAllPage(pagine, quantita).subscribe((resp) => {
      this.lista = resp.response;
    });
    this.totDocenti();
  }

  /* paginate(event:any){
    this.pagine = this.pagine + 1;
    console.log("cambio pagina", event);
  } */

  cercaGlobale() {
    this.docentiService
      .filter(this.formGroup.value, this.pagine, this.quantita)
      .subscribe((resp) => {
        if (this.formGroup.value == "") {
          this.filtro = false;
        } else {
          this.lista = resp.response;
          this.filtro = true;
        }
      });
    this.docentiService
      .getNumberOfPageFilter(this.formGroup.value, this.pagine, this.quantita)
      .subscribe((resp) => {
        this.numeroPagine = resp.response;
      });
  }
  loadFilter(pagine: number, quantita: number) {
    this.docentiService
      .filter(this.formGroup.value, pagine, quantita)
      .subscribe((resp) => {
        this.lista = resp.response;
      });
    this.docentiService
      .getNumberOfPageFilter(this.formGroup.value, pagine, quantita)
      .subscribe((resp) => {
        this.numeroPagine = resp.response;
      });
    this.totDocenti();
  }

  //Filter Docenti Interni
  cercaGlobaleInterni() {
    this.alunniService
      .filtroInt(this.formGroup.value, this.pagineDocInt, this.quantitaDocInt)
      .subscribe((resp) => {
        if (this.formGroup.value == "") {
          this.filtroInt = false;
        } else {
          this.dipendenti = resp.response;
          console.log("Filtro", this.dipendenti);
          this.filtroInt = true;
          console.log("form", this.formGroup.value);
        }
      });
    this.alunniService
      .getNumberOfPageFilterInt(
        this.formGroup.value,
        this.pagineDocInt,
        this.quantitaDocInt
      )
      .subscribe((resp) => {
        this.numPageDocInt = resp.response;
        console.log("numero pagine", this.numPageDocInt);
      });
  }
  // loadFilterInterni(pagineDocInt: number, quantitaDocInt: number) {
  //   this.alunniService
  //     .filtroInt(this.formGroup.value, pagineDocInt, quantitaDocInt)
  //     .subscribe((resp) => {
  //       this.dipendenti = resp.response;
  //     });
  //   this.alunniService
  //     .getNumberOfPageFilterInt(
  //       this.formGroup.value,
  //       pagineDocInt,
  //       quantitaDocInt
  //     )
  //     .subscribe((resp) => {
  //       this.numPageDocInt = resp.response;
  //     });
  //   this.totDocInt();
  // }

  select() {
    // console.log(this.selectedItem);
  }

  elimina(event: any) {
    // console.log(event);
    this.docentiService.deleteById(event.docenteId).subscribe((resp) => {
      if (this.filtro) {
        this.loadFilter(this.pagine, this.quantita);
      } else {
        this.loadPage(this.pagine, this.quantita);
        this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);
      }
    });

    this.exit();
  }

  showDialog() {
    this.display = true;
    console.log(this.display);
  }

  getDocente(event: any) {
    this.soggetto = event;
    this.onEdit.emit(this.soggetto);
    // console.log("docente edit:", this.soggetto);
  }

  confirmDoc(event: any) {
    this.soggetto = event;
  }

  showDialog1(event: any) {
    this.getDocente(event);
    this.display1 = true;
  }
  showDialogConf(event: any) {
    this.confirmDoc(event);
    this.displayConf = true;
  }

  onDialogClose(event: any) {
    this.display = event;
    this.filtro = false;
    this.loadPage(this.pagine, this.quantita);
    this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);
  }

  onDialogClose1(event: any) {
    this.display1 = event;
    if (this.filtro) {
      this.loadFilter(this.pagine, this.quantita);
    } else {
      this.loadPage(this.pagine, this.quantita);
      this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);
    }
    // console.log("dialogclose1", event);
  }

  showNew(event: any) {
    console.log("user", event);
    this.lista.push(event);
  }

  getDocenteInfo(event: any) {
    this.soggetto = event;
    this.onSend.emit(this.soggetto);
    // console.log("docente edit:", this.soggetto);
  }

  showDialog3(event: any) {
    this.getDocenteInfo(event);
    this.display3 = true;
  }

  onDialogClose3(event: any) {
    this.display3 = event;
    this.loadPage(this.pagine, this.quantita);
  }

  getDocenteInt(item: any) {
    this.docenteInt = item;
    this.onInfo.emit(this.docenteInt);
  }

  showDialogInt(event: any) {
    this.getDocenteInt(event);
    this.display2 = true;
  }

  onDialogCloseInt(event: any) {
    this.display2 = event;
    this.loadPage(this.pagine, this.quantita);
    this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);
  }

  next() {
    if (this.pagine + 1 > this.numeroPagine - 1) {
    } else {
      this.pagine = this.pagine + 1;
    }
    if (this.filtro) {
      this.loadFilter(this.pagine, this.quantita);
    } else {
      this.loadPage(this.pagine, this.quantita);
    }
  }

  prev() {
    if (this.pagine - 1 < 0) {
    } else {
      this.pagine = this.pagine + -1;
    }
    if (this.filtro) {
      this.loadFilter(this.pagine, this.quantita);
    } else {
      this.loadPage(this.pagine, this.quantita);
    }
  }

  reset() {
    this.pagine = 0;
    if (this.filtro) {
      this.loadFilter(this.pagine, this.quantita);
    } else {
      this.loadPage(this.pagine, this.quantita);
    }
  }

  last() {
    this.pagine = this.numeroPagine - 1;
    if (this.filtro) {
      this.loadFilter(this.pagine, this.quantita);
    } else {
      this.loadPage(this.pagine, this.quantita);
    }
  }

  isLastPage(): boolean {
    if (this.pagine == this.numeroPagine - 1) {
      return true;
    } else {
      return false;
    }
  }

  isFirstPage(): boolean {
    if (this.pagine == 0) {
      return true;
    } else {
      return false;
    }
  }

  nextInt() {
    if (this.pagineDocInt + 1 > this.numPageDocInt - 1) {
    } else {
      this.pagineDocInt = this.pagineDocInt + 1;
    }
    if (this.filtroInt) {
      this.cercaGlobaleInterni();
    } else {
      this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);
    }
  }

  prevInt() {
    if (this.pagineDocInt - 1 < 0) {
    } else {
      this.pagineDocInt = this.pagineDocInt + -1;
    }
    if (this.filtroInt) {
      this.cercaGlobaleInterni();
    } else {
      this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);
    }
  }

  resetInt() {
    this.pagineDocInt = 0;
    if (this.filtroInt) {
      this.cercaGlobaleInterni();
    } else {
      this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);
    }
  }

  lastInt() {
    this.pagineDocInt = this.numPageDocInt - 1;
    if (this.filtroInt) {
      this.cercaGlobaleInterni();
    } else {
      this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);
    }
  }

  isLastPageInt(): boolean {
    if (this.pagineDocInt == this.numPageDocInt - 1) {
      return true;
    } else {
      return false;
    }
  }

  isFirstPageInt(): boolean {
    if (this.pagineDocInt == 0) {
      return true;
    } else {
      return false;
    }
  }

  exit() {
    this.displayConf = false;
    this.onDialogCloseConf.emit(this.display3);
    this.loadPage(this.pagine, this.quantita);
    this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);
  }

  totDocInt() {
    this.alunniService.totDoppioni().subscribe((resp) => {
      this.totaleDocentiInt = resp.response;
    });
  }

  loadDocentiInt(pagine: number, quantita: number) {
    this.alunniService.doppioniPages(pagine, quantita).subscribe((resp) => {
      this.numPageDocInt = resp.response;
    });

    this.alunniService.doppioni(pagine, quantita).subscribe((resp) => {
      this.dipendenti = resp.response;
      console.log("dooc interni", this.dipendenti);

      /*    this.associazioni = [];
      if (this.associazioni) {
        this.dipendenti.forEach((element) => {
          this.apiCorsiDocenti.getCorsiByInterno(element.dipendenteId).subscribe((resp) => {
            this.associazioni.push(resp.response.corso);
            this.associazioni.push(element);
          });
        });
        console.log("ASSOCIAZIONE", this.associazioni);
      } */
    });
  }
}
