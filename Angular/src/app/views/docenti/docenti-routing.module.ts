import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocentiComponent } from './docenti/docenti.component';
import { NewComponent } from './docenti/new/new.component';


const routes: Routes = [
  {
    path: '',
    component: DocentiComponent,
    data: {
      title: 'Docenti'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocentiRoutingModule { }
