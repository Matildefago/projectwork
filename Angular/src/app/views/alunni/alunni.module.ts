import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AlunniRoutingModule } from "./alunni-routing.module";
import { AlunniComponent } from "./alunni/alunni.component";
import { TableModule } from "primeng/table";
import { ButtonModule } from "primeng/button";
import { DialogModule } from "primeng/dialog";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { InfoAlunniComponent } from "./alunni/info-alunni/info-alunni.component";
import { ScrollPanelModule } from "primeng/scrollpanel";

import { TabViewModule } from "primeng/tabview";

@NgModule({
  declarations: [AlunniComponent, InfoAlunniComponent],
  imports: [
    CommonModule,
    AlunniRoutingModule,
    TableModule,
    ButtonModule,
    DialogModule,
    FormsModule,
    ScrollPanelModule,
    ReactiveFormsModule,
    TabViewModule,
  ],
})
export class AlunniModule {}
