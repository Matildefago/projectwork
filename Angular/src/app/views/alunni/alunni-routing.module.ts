import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AlunniComponent } from "./alunni/alunni.component";

const routes: Routes = [
  {
    path: "",
    component: AlunniComponent,
    data: {
      title: "Alunni",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlunniRoutingModule {}
