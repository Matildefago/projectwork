import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { AlunniApiService } from "../../../api/alunni-api.service";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: "app-alunni",
  templateUrl: "./alunni.component.html",
  styleUrls: ["./alunni.component.css"],
})
export class AlunniComponent implements OnInit {
  constructor(
    private api: AlunniApiService,
    public router: Router,
    public fb: FormBuilder
  ) {}

  public filtro: boolean;
  public formGroup: FormGroup;
  public display: boolean = false;
  colsAlunni: any[];
  public lista: any[];
  public listaTot: any[];
  public selectedItem: any;
  public soggetto: any;
  public quantita: number = 8;
  public pagine: number = 0;
  public numeroPagine: any;
  public ragioneSociale: String;

  @Output()
  onSend: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  onInfo: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit(): void {
    this.colsAlunni = [
      { field: "matricola", header: "Matricola" },
      { field: "nome", header: "Nome" },
      { field: "cognome", header: "Cognome" },
      { field: "qualifica", header: "Qualifica" },
    ];

    this.formGroup = this.fb.group({
      cerca: [""],
    });

    this.load(this.pagine, this.quantita);
    this.totDipendenti();
  }

  load(pagine: number, quantita: number) {
    this.api.getNumberOfPage(pagine, quantita).subscribe((resp) => {
      this.numeroPagine = resp.response;
      // console.log("num pagine", this.numeroPagine);
    });

    this.api.getAllPage(pagine, quantita).subscribe((resp) => {
      this.lista = resp.response;
      console.log(resp.response);
    });
  }

  totDipendenti() {
    this.api.getAll().subscribe((resp) => {
      this.listaTot = resp.response;
      // console.log("num pagine", this.numeroPagine);
    });
  }

  /* senditem() {
    this.onSend.emit(this.selectedItem);
    this.showDialog();
  } */

  showDialog(event: any) {
    this.display = true;
    this.get(event);
  }

  onDialogClose(event: any) {
    this.display = event;
    this.load(this.pagine, this.quantita);
    // console.log("event", event);
  }
  get(event: any) {
    this.selectedItem = event;
    console.log(this.selectedItem);
    this.onInfo.emit(this.selectedItem);
  }

  next() {
    if (this.pagine + 1 > this.numeroPagine - 1) {
    } else {
      this.pagine = this.pagine + 1;
    }
    if (this.filtro) {
      this.loadFilter(this.pagine, this.quantita);
    } else {
      this.load(this.pagine, this.quantita);
    }
  }

  prev() {
    if (this.pagine - 1 < 0) {
    } else {
      this.pagine = this.pagine + -1;
    }
    if (this.filtro) {
      this.loadFilter(this.pagine, this.quantita);
    } else {
      this.load(this.pagine, this.quantita);
    }
  }

  reset() {
    this.pagine = 0;
    if (this.filtro) {
      this.loadFilter(this.pagine, this.quantita);
    } else {
      this.load(this.pagine, this.quantita);
    }
  }

  last() {
    this.pagine = this.numeroPagine - 1;
    if (this.filtro) {
      this.loadFilter(this.pagine, this.quantita);
    } else {
      this.load(this.pagine, this.quantita);
    }
  }

  isLastPage(): boolean {
    if (this.pagine == this.numeroPagine - 1) {
      return true;
    } else {
      return false;
    }
  }

  isFirstPage(): boolean {
    if (this.pagine == 0) {
      return true;
    } else {
      return false;
    }
  }

  stringSoc(corso: any): String {
    if (!corso) {
      return;
    }
    this.ragioneSociale = JSON.stringify(corso.societa.ragioneSociale);
    return this.ragioneSociale.replace(/"/g, "");
  }
  cercaGlobale() {
    console.log("value", this.formGroup.value);
    this.api
      .filtro(this.formGroup.value, this.pagine, this.quantita)
      .subscribe((resp) => {
        if (this.formGroup.value == "") {
          this.filtro = false;
        } else {
          this.lista = resp.response;
          // console.log("lista", this.lista);
          this.filtro = true;
        }
      });
    this.api
      .getNumberOfPageFilter(this.formGroup.value, this.pagine, this.quantita)
      .subscribe((resp) => {
        this.numeroPagine = resp.response;
      });
  }
  loadFilter(pagine: number, quantita: number) {
    this.api
      .filtro(this.formGroup.value, pagine, quantita)
      .subscribe((resp) => {
        this.lista = resp.response;
      });
    this.api
      .getNumberOfPageFilter(this.formGroup.value, pagine, quantita)
      .subscribe((resp) => {
        this.numeroPagine = resp.response;
      });
    this.totDipendenti();
  }
}
