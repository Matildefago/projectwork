import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AlunniRoutingModule } from "./alunni-routing.module";
import { AlunniComponent } from "./alunni.component";
import { TableModule } from "primeng/table";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ButtonModule } from "primeng/button";
import { DialogModule } from "primeng/dialog";
import { ScrollPanelModule } from "primeng/scrollpanel";

import { TabViewModule } from "primeng/tabview";
import { InfoAlunniComponent } from "./info-alunni/info-alunni.component";

@NgModule({
  declarations: [AlunniComponent, InfoAlunniComponent],
  imports: [
    CommonModule,
    AlunniRoutingModule,
    TableModule,
    ButtonModule,
    FormsModule,
    ScrollPanelModule,

    ReactiveFormsModule,
    DialogModule,
    TabViewModule,
  ],
})
export class AlunniModule {}
