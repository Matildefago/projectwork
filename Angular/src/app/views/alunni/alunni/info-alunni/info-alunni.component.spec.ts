import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoAlunniComponent } from './info-alunni.component';

describe('InfoAlunniComponent', () => {
  let component: InfoAlunniComponent;
  let fixture: ComponentFixture<InfoAlunniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoAlunniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoAlunniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
