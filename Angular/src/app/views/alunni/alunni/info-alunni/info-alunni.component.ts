import { Component, OnInit, EventEmitter, Input, Output } from "@angular/core";
import { AlunniApiService } from "../../../../api/alunni-api.service";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";
import { build$ } from 'protractor/built/element';
import { CorsiUtentiService } from '../../../../api/corsi-utenti.service';
import { CorsiDocentiService } from '../../../../api/corsi-docenti.service';

@Component({
  selector: "app-info-alunni",
  templateUrl: "./info-alunni.component.html",
  styleUrls: ["./info-alunni.component.css"],
})
export class InfoAlunniComponent implements OnInit {
  @Input()
  public display: boolean;
  @Output()
  onClick: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  onDialogClose: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  set soggetto(item: any) {
    this._soggetto = item;
    if (item) {
      this.date();
      this.getAssociazioni();
      this.getCorso();
      this.build();
    }
  }

  get soggetto() {
    return this._soggetto;
  }

  colsAlunni: any[];
  private _soggetto: any;
  private ragioneSociale: any;
  public listaCorsi: any[];
  public associazioni: any[];
  colsCorsi : any[];
  listaCorsiDocenti: any[];
  corsoDocente: any[];
  corsoInterno: any[];
  data: any;
  dataN: String;
  dataA: String;
  dataS: String;
  dataF: String;

  constructor(public alunni: AlunniApiService, public router: Router, private associaService: CorsiUtentiService, private corsiDocenti: CorsiDocentiService) {}

  ngOnInit(): void {}

  viewitem() {
    console.log("alunni info:", this.soggetto);
  }
  onInfo(event: any) {
    console.log("item", event);
  }
  exit() {
    this.display = false;
    this.onDialogClose.emit(this.display);
  }

  stringSoc(soggetto: any): String{
    if(!soggetto){
      return;
    }
      this.ragioneSociale = JSON.stringify(soggetto.societa.ragioneSociale);
      return this.ragioneSociale.replace(/"/g, '');
    }

  build() {
    this.colsAlunni = [
      { field: "matricola", header: "Matricola" },
      { field: "nome", header: "Nome" },
      { field: "cognome", header: "Cognome" },
      { field: "sesso", header: "Sesso" },
      { field: "luogoNascita", header: "Luogo di Nascita" },
      { field: "statoCivile", header: "Stato Civile" },
      { field: "titoloStudio", header: "Titolo" },
      { field: "qualifica", header: "Qualifica" },
      { field: "conseguitoPresso", header: "Conseguito Presso" },
      { field: "responsabileRisorsa", header: "Responsabile Risorsa" },
      { field: "responsabileArea", header: "Responsabile Area" },
    ];

    this.colsCorsi = [
      { field: "descrizione", header: "Descrizione" },
      { field: "ente", header: "Ente" },
      { field: "edizione", header: "Edizione" },
      { field: "luogo", header: "Luogo" },
    ];
  }

  date(){
    this.data = new Date(this.soggetto.dataNascita);
    if(this.data.getDate() == 1){
      this.dataN = this.data.getDate() + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }else{
      this.dataN = (this.data.getDate()-1) + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }

    this.data = new Date(this.soggetto.dataAssunzione);
    if(this.data.getDate() == 1){
      this.dataA = this.data.getDate() + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }else{
      this.dataA = (this.data.getDate()-1) + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }

    this.data = new Date(this.soggetto.dataScadenzaContratto);
    if(this.data.getDate() == 1){
      this.dataS = this.data.getDate() + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }else{
      this.dataS = (this.data.getDate()-1) + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }

    this.data = new Date(this.soggetto.dataFineRapporto);
    if(this.data.getDate() == 1){
      this.dataF = this.data.getDate() + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }else{
      this.dataF = (this.data.getDate()-1) + "/" + (this.data.getMonth()+1) + "/" + this.data.getFullYear();
    }
  }

  getAssociazioni(){
   
    this.associaService.getCorsoByDip(this.soggetto.dipendenteId).subscribe((resp)=>{
      this.associazioni = resp.response;
      if(this.associazioni){
        this.listaCorsi = [];
        this.associazioni.forEach(element => {
          this.listaCorsi.push(element.corso);
        });
        console.log("Associazioni:", this.associazioni);
        console.log("Dipedenti:", this.listaCorsi);
      }else{
        this.listaCorsi = null;
        this.associazioni = null;
      }
    })
  }

  getCorso(){
    this.listaCorsiDocenti = [];
    this.corsiDocenti.getCorsiByInterno(this.soggetto.dipendenteId).subscribe((resp)=>{
      this.corsoDocente = resp.response;
      if(this.corsoDocente.length > 0){
        this.corsoInterno = [];
        this.corsoDocente.forEach(element => {
          this.corsoInterno.push(element.corso);
        });
      
        console.log("CORSO" , this.corsoInterno[0]);
      }
      else{
        this.corsoDocente = null;
        this.corsoInterno = null;
      }
    })
  }
}
