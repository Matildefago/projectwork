import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlunniComponent } from './alunni.component';

describe('AlunniComponent', () => {
  let component: AlunniComponent;
  let fixture: ComponentFixture<AlunniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlunniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlunniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
