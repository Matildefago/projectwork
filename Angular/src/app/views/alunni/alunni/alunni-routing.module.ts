import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AlunniComponent } from "./alunni.component";
import { InfoAlunniComponent } from "./info-alunni/info-alunni.component";

const routes: Routes = [
  { path: "", component: AlunniComponent },
  { path: "dipendenti/info", component: InfoAlunniComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlunniRoutingModule {}
