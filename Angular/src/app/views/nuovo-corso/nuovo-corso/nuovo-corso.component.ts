import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { CorsiApiService } from "../../../api/corsi-api.service";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "app-nuovo-corso",
  templateUrl: "./nuovo-corso.component.html",
  styleUrls: ["./nuovo-corso.component.css"],
})
export class NuovoCorsoComponent implements OnInit {
  @Output()
  nCorso: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onSend: EventEmitter<any> = new EventEmitter<any>();

  public descrizioneMis: any[];

  public displayAssociazione: boolean = false;

  public luoghiCorsi: any[];

  public argomenti: any[];

  public listaCorsi: any[];

  public dipendenti: any[];

  public formGroup: FormGroup;

  public stato: boolean = false;
  public stato1: boolean = false;
  public listaMisure: any[];
  public listaTipi: any[];
  public listaTipologie: any[];
  public misura: any;
  public tipologie: any[];
  public societa: any[];
  public listaSocieta: any[];
  public corsoCreato: any;
  public corsoSend: any;

  constructor(
    public corso: CorsiApiService,
    public fb: FormBuilder,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.getSocieta();
    this.getTipologie();
    this.getMisure();
    this.getTipi();
    this.buildForm();

    this.luoghiCorsi = [
      { label: "-Seleziona-", value: null },
      //sedi corvallis
      { label: "Bari", value: "Bari" },
      { label: "Brescia", value: "Brescia" },
      { label: "Bologna", value: "Bologna" },
      { label: "Caltagirone", value: "Caltagirone" },
      { label: "Catania", value: "Catania" },
      { label: "Firenze", value: "Firenze" },
      { label: "Genova", value: "Genova" },
      { label: "Livorno", value: "Livorno" },
      { label: "Modena", value: "Modena" },
      { label: "Milano", value: "Milano" },
      { label: "Padova", value: "Padova" },
      { label: "Palermo", value: "Palermo" },
      { label: "Roma", value: "Roma" },
      { label: "Torino", value: "Torino" },
      { label: "Trieste", value: "Trieste" },
      { label: "Venezia", value: "Venezia" },
      { label: "Verona", value: "Verona" }
    ]
  }

  getCorsoInfo(event: any) {
    this.corso = event;
    this.onSend.emit(this.corso);
    // console.log("corso info:", this.corso);
  }

  showDialogAssociazione(event: any) {
    this.getCorsoInfo(event);
    this.displayAssociazione = true;
  }
  onDialogCloseAssociazione(event: any) {
    this.displayAssociazione = event;
    // console.log("event", event);
  }

  buildForm() {
    this.formGroup = this.fb.group({
      tipologia: [""],
      tipo: [""],
      previsto: [""],
      erogato: [""],
      durata: [""],
      misura: [""],
      luogo: [""],
      descrizione: [""],
      edizione: [""],
      inizioFine: [""],
      dataCensimento: [""],
      interno: [""],
      note: [""],
      ente: [""],
      societa: [""],
      dataErogazione: [""],
      dataChiusura: [""],
    });
  }

  getMisure() {
    this.descrizioneMis = [{ label: "-Seleziona-", value: null }];
    this.corso.getMisure().subscribe((resp) => {
      this.listaMisure = resp.response;
      // console.log(this.listaMisure);
      if (this.listaMisure) {
        this.listaMisure.forEach((element) => {
          this.descrizioneMis.push({
            label: element.descrizione,
            value: { element },
          });
        });
      }
    });
  }

  getTipi() {
    this.argomenti = [{ label: "-Seleziona-", value: null }];
    this.corso.getTipo().subscribe((resp) => {
      this.listaTipi = resp.response;
      // console.log(this.listaTipi);
      if (this.listaTipi) {
        this.listaTipi.forEach((element) => {
          this.argomenti.push({
            label: element.descrizione,
            value: { element },
          });
        });
      }
    });
  }

  getTipologie() {
    this.tipologie = [{ label: "-Seleziona-", value: null }];
    this.corso.getTipologia().subscribe((resp) => {
      this.listaTipologie = resp.response;
      // console.log(this.listaTipologie);
      if (this.listaTipologie) {
        this.listaTipologie.forEach((element) => {
          this.tipologie.push({
            label: element.descrizione,
            value: { element },
          });
        });
      }
    });
  }

  getSocieta() {
    this.societa = [{ label: "-Seleziona-", value: null }];
    this.corso.getSocieta().subscribe((resp) => {
      this.listaSocieta = resp.response;
      // console.log(this.listaSocieta);
      if (this.listaSocieta) {
        this.listaSocieta.forEach((element) => {
          this.societa.push({
            label: element.ragioneSociale,
            value: { element },
          });
        });
      }
    });
  }

  svuotaCampi() {
    this.buildForm();
  }

  creaCorso() {
    /*  this.corso.add(this.formGroup.value).subscribe((resp) => {
      this.nCorso.emit(this.formGroup.value);
      this.buildForm();
    }); 
 */
    this.corsoCreato = this.formGroup.value;
    console.log("corso creato:", this.corsoCreato);

    if (this.corsoCreato.previsto == "Previsto") {
      this.corsoCreato.previsto = 1;
      this.corsoCreato.erogato = 0;
    } else {
      this.corsoCreato.erogato = 1;
      this.corsoCreato.previsto = 0;
    }

    if (this.corsoCreato.interno) {
      this.corsoCreato.interno = 1;
    } else {
      this.corsoCreato.interno = 0;
    }

    if (this.corsoCreato) {
      this.corsoCreato.tipo = this.corsoCreato.tipo.value.element;
      this.corsoCreato.tipologia = this.corsoCreato.tipologia.value.element;
      this.corsoCreato.misura = this.corsoCreato.misura.value.element;
      this.corsoCreato.societa = this.corsoCreato.societa.value.element;
      this.corsoCreato.luogo = this.corsoCreato.luogo.value;
      this.corsoCreato.dataErogazione = this.corsoCreato.inizioFine[0];
      this.corsoCreato.dataChiusura = this.corsoCreato.inizioFine[1];
    }

    this.corso.add(this.corsoCreato).subscribe((resp) => {
      // console.log("subscribe: ", this.corsoCreato);
      if (resp.response) {
        this.nCorso.emit(this.corsoCreato);
        this.buildForm();
        this.corso.getAll().subscribe((resp) => {
          this.listaCorsi = resp.response;
          this.corsoSend = this.listaCorsi[this.listaCorsi.length - 1];
          // console.log("ESISTE", this.corsoSend);
          this.showDialogAssociazione(this.corsoSend);
        });
      }
    });
  }

  conferma() {
    this.corsoCreato = this.formGroup.value;
    // console.log("corso creato:", this.corsoCreato);

    if (this.corsoCreato.previsto == "Previsto") {
      this.corsoCreato.previsto = 1;
      this.corsoCreato.erogato = 0;
    } else {
      this.corsoCreato.erogato = 1;
      this.corsoCreato.previsto = 0;
    }

    if (this.corsoCreato.interno) {
      this.corsoCreato.interno = 1;
    } else {
      this.corsoCreato.interno = 0;
    }

    if (this.corsoCreato) {
      this.corsoCreato.tipo = this.corsoCreato.tipo.value.element;
      this.corsoCreato.tipologia = this.corsoCreato.tipologia.value.element;
      this.corsoCreato.misura = this.corsoCreato.misura.value.element;
      this.corsoCreato.societa = this.corsoCreato.societa.value.element;
      this.corsoCreato.luogo = this.corsoCreato.luogo.value;
      this.corsoCreato.dataErogazione = this.corsoCreato.inizioFine[0];
      this.corsoCreato.dataChiusura = this.corsoCreato.inizioFine[1];
    }

    this.corso.add(this.corsoCreato).subscribe((resp) => {
      // console.log("subscribe: ", this.corsoCreato);
      this.router.navigate(["/corsi"]);
    });
  }
}
