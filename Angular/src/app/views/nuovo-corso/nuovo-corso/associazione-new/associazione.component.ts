import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { AlunniApiService } from "../../../../api/alunni-api.service";
import { FormBuilder } from "@angular/forms";
import { CorsiApiService } from "../../../../api/corsi-api.service";
import { Router } from "@angular/router";
import { CorsiUtentiService } from "../../../../api/corsi-utenti.service";
import { DocentiApiService } from "../../../../api/docenti-api.service";
import { CorsiDocentiService } from "../../../../api/corsi-docenti.service";

@Component({
  selector: "app-associazione",
  templateUrl: "./associazione.component.html",
  styleUrls: ["./associazione.component.css"],
})
export class AssociazioneComponent implements OnInit {
  public dipendenti: any[];
  public listaDipendenti: any[];
  public variabile: String;
  public dipendente: any;
  public associazione: any;
  public listaAssociati: any[];
  public control = false;
  listaDocenti: any[];
  docenti: any[];
  associazioneDocenti: any;

  @Input()
  public displayAssociazione: boolean;

  @Output()
  onClick: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  onDialogCloseAssociazione: EventEmitter<any> = new EventEmitter<any>();
  formGroup: any;
  formGroupDocenti: any;

  @Input()
  set corso(item: any) {
    this._corso = item;
    if (item) {
      this.getDocenti();
      this.getDipendenti();
      this.buildFormVuoto();
      this.buildFormDocenti();
      console.log("SONO ENTRATO");
    }
  }

  get corso() {
    return this._corso;
  }

  public _corso: any;

  constructor(
    public dipService: AlunniApiService,
    public fb: FormBuilder,
    public corsoService: CorsiApiService,
    public router: Router,
    public associaService: CorsiUtentiService,
    public docentiService: DocentiApiService,
    public corsiDocService: CorsiDocentiService
  ) {}

  ngOnInit(): void {
    /* this.getDipendenti();
  this.buildFormVuoto(); */
  }

  getDipendenti() {
    if (this.dipendenti) {
      return this.dipendenti;
    } else {
      this.dipendenti = [];
      this.variabile = "";
      this.dipService.getAll().subscribe((resp) => {
        this.listaDipendenti = resp.response;

        this.listaDipendenti.forEach((element) => {
          if (this.listaDipendenti) {
            this.variabile =
              "" +
              element.matricola +
              " " +
              element.nome +
              " " +
              element.cognome +
              "";
            this.dipendenti.push({
              label: this.variabile,
              value: { element },
            });
          }
        });
      });
    }
  }

  getDocenti() {
    if (this.docenti) {
      return this.docenti;
    } else {
      this.docenti = [];
      this.variabile = "";
      this.docentiService.getAll().subscribe((resp) => {
        if (resp.response != null) {
          this.listaDocenti = resp.response;
          console.log("LISTA DOCENTI", this.listaDocenti);
          this.listaDocenti.forEach((element) => {
            if (this.listaDocenti) {
              this.variabile =
                "" + element.titolo + " " + element.ragioneSociale + " ";
              this.docenti.push({
                label: this.variabile,
                value: { element },
              });
            }
          });
        }
      });
    }
  }

  buildFormVuoto() {
    if (this.corso) {
      this.formGroup = this.fb.group({
        corso: [this.corso],
        dipendente: [""],
        durata: [""],
      });
    }
  }

  buildFormDocenti() {
    if (this.corso) {
      this.formGroupDocenti = this.fb.group({
        corso: [this.corso],
        docente: [""],
        interno: [""],
      });
    }
  }

  conferma2() {
    console.log("VALORE: ", this.formGroup.value);
    this.associazione = this.formGroup.value;
    this.listaAssociati = this.formGroup.value.dipendente;
    console.log("LISTA", this.listaAssociati);
    if (this.listaAssociati) {
      this.listaAssociati.forEach((element) => {
        if (this.associazione) {
          if (element && element != 0) {
            this.associazione.dipendente = element.value.element;
            this.associazione.corso = this.corso;
            this.associazione.durata = this.associazione.durata;
            this.associaService
              .associaUtenti(this.associazione)
              .subscribe((resp) => {
                console.log("ASS", this.associazione);
              });
          }
        }
      });
    }

    if (this.formGroupDocenti.value.docente) {
      this.associazioneDocenti = this.formGroupDocenti.value;
      if (this.associazioneDocenti.interno) {
        this.associazioneDocenti.interno = 1;
      } else {
        this.associazioneDocenti.interno = 0;
      }
      if (this.associazioneDocenti.interno == 1) {
        this.associazioneDocenti.docente = this.associazioneDocenti.docente.value.element.dipendenteId;
      } else {
        this.associazioneDocenti.docente = this.associazioneDocenti.docente.value.element.docenteId;
      }
      this.corsiDocService
        .associaDocenti(this.associazioneDocenti)
        .subscribe((resp) => {
          console.log("ASSOCIAZIONE DOC", this.associazioneDocenti);
        });
    }

    this.exit();
  }
  exit() {
    this.displayAssociazione = false;
    this.onDialogCloseAssociazione.emit(this.displayAssociazione);
    this.router.navigate(["/corsi"]);
  }
}
