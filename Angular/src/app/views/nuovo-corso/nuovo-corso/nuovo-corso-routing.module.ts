import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NuovoCorsoComponent } from './nuovo-corso.component';

const routes: Routes = [{ path: '', component: NuovoCorsoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NuovoCorsoRoutingModule { }
