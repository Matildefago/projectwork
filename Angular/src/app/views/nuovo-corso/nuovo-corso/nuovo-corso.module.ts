import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { NuovoCorsoRoutingModule } from "./nuovo-corso-routing.module";
import { NuovoCorsoComponent } from "./nuovo-corso.component";

import { RadioButtonModule } from "primeng/radiobutton";
import { InputTextModule } from "primeng/inputtext";
import { DropdownModule } from "primeng/dropdown";
import { AutoCompleteModule } from "primeng/autocomplete";
import { InputTextareaModule } from "primeng/inputtextarea";
import { CalendarModule } from "primeng/calendar";
import { CheckboxModule } from "primeng/checkbox";
import { MultiSelectModule } from "primeng/multiselect";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { InputSwitchModule } from "primeng/inputswitch";
import { ListboxModule } from "primeng/listbox";
import { AssociazioneComponent } from './associazione-new/associazione.component';
import { DialogModule } from 'primeng/dialog';
import {TabViewModule} from 'primeng/tabview';

@NgModule({
  declarations: [NuovoCorsoComponent,  AssociazioneComponent],
  imports: [
    CommonModule,
    NuovoCorsoRoutingModule,
    RadioButtonModule,
    InputTextModule,
    DropdownModule,
    AutoCompleteModule,
    InputTextareaModule,
    CalendarModule,
    CheckboxModule,
    MultiSelectModule,
    FormsModule,
    ReactiveFormsModule,
    ListboxModule,
    InputSwitchModule,
    DialogModule,
    TabViewModule
  ],
})
export class NuovoCorsoModule {}
