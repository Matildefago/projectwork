import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { NuovoCorsoComponent } from "./nuovo-corso/nuovo-corso.component";

const routes: Routes = [
  {
    path: "",
    component: NuovoCorsoComponent,
    data: {
      title: "NuovoCorso",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NuovoCorsoRoutingModule {}
