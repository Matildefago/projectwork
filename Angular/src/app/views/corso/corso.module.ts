import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CorsoRoutingModule } from "./corso-routing.module";
import { CorsoComponent } from "./corso/corso.component";
import { TableModule } from "primeng/table";
import { ButtonModule } from "primeng/button";
import { DialogModule } from "primeng/dialog";
import { TabViewModule } from "primeng/tabview";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { InfoCorsoComponent } from "./corso/info-corso/info-corso.component";

import { DropdownModule } from "primeng/dropdown";
import { ScrollPanelModule } from "primeng/scrollpanel";
import { ListboxModule } from "primeng/listbox";
import { CheckboxModule } from "primeng/checkbox";
import { AssociazioneComponent } from "./corso/associazione/associazione.component";
import {SpinnerModule} from 'primeng/spinner';
@NgModule({
  declarations: [CorsoComponent, InfoCorsoComponent, AssociazioneComponent],
  imports: [
    CommonModule,
    CorsoRoutingModule,
    TableModule,
    ButtonModule,
    DialogModule,
    ScrollPanelModule,
    CheckboxModule,
    TabViewModule,
    ListboxModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
    SpinnerModule,
  ],
})
export class CorsoModule {}
