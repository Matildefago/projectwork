import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CorsoComponent } from "./corso/corso.component";

const routes: Routes = [
  {
    path: "",
    component: CorsoComponent,
    data: {
      title: "Corso",
    },
  },

  // { path: 'corso', loadChildren: () => import('./corso/corso.module').then(m => m.CorsoModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CorsoRoutingModule {}
