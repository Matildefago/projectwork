import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { RouterLink, Router } from "@angular/router";
import { ApiService } from "../../../api/api.service";
import { CorsiApiService } from "../../../api/corsi-api.service";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: "app-corso",
  templateUrl: "./corso.component.html",
  styleUrls: ["./corso.component.css"],
})
export class CorsoComponent implements OnInit {
  public display: boolean = false;
  public displayAssociazione: boolean = false;
  public numeroPagine: number;
  public lista: any[];
  cols: any[];
  public filtro: boolean;
  public pagine: number = 0;
  public quantita: number = 8;
  public corso: any;
  public tipologia: String;
  public ragioneSociale: String;
  public listaTot: any[];
  public formGroup: FormGroup;

  @Output()
  onSend: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onDialogCloseConf: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  public displayConf: boolean;

  @Output()
  onEdit: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private api: ApiService,
    private corsiService: CorsiApiService,
    public router: Router,
    public fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.loadPage(this.pagine, this.quantita);
    this.totCorsi();

    this.formGroup = this.fb.group({
      cerca: [""],
    });

    this.cols = [
      { field: "descrizione", header: "Descrizione" },
      { field: "luogo", header: "Luogo" },
      { field: "ente", header: "Ente" },
    ];
  }

  totCorsi() {
    this.corsiService.getAll().subscribe((resp) => {
      this.listaTot = resp.response;
    });
  }

  string(corso: any): String {
    if (!corso) {
      return;
    }
    this.tipologia = JSON.stringify(corso.tipologia.descrizione);
    this.tipologia = this.tipologia.replace(/"/g, "");
    return this.tipologia;
  }

  stringSoc(corso: any): String {
    if (!corso) {
      return;
    }
    this.ragioneSociale = JSON.stringify(corso.societa.ragioneSociale);
    return this.ragioneSociale.replace(/"/g, "");
  }

  loadPage(pagine: number, quantita: number) {
    this.corsiService.getNumberOfPage(pagine, quantita).subscribe((resp) => {
      this.numeroPagine = resp.response;
      // console.log("num pagine", this.numeroPagine);
    });

    this.corsiService.getAllPage(pagine, quantita).subscribe((resp) => {
      this.lista = resp.response;
      // console.log(resp.response);
    });
    this.totCorsi();
  }

  showDialog(event: any) {
    this.getCorsoInfo(event);
    this.display = true;
    // console.log(this.display);
  }

  onDialogClose(event: any) {
    this.display = event;
    this.loadPage(this.pagine, this.quantita);
    // console.log("event", event);
  }

  showDialogAssociazione(event: any) {
    this.getCorsoInfo(event);
    this.displayAssociazione = true;
  }
  onDialogCloseAssociazione(event: any) {
    this.displayAssociazione = event;
    this.loadPage(this.pagine, this.quantita);
    // console.log("event", event);
  }

  elimina(event: any) {
    // console.log(event);
    this.corsiService.deleteById(event.corsoId).subscribe((resp) => {
      this.loadPage(this.pagine, this.quantita);
    });

    this.exit();
  }

  getCorsoInfo(event: any) {
    this.corso = event;
    this.onSend.emit(this.corso);
    // console.log("corso info:", this.corso);
  }

  getCorsoEdit(event: any) {
    this.corso = event;
    this.onEdit.emit(this.corso);
    // console.log("edit:", this.corso);
    this.router.navigate(["editCorso"], { state: { corso: this.corso } });
  }

  next() {
    if (this.pagine + 1 > this.numeroPagine - 1) {
    } else {
      this.pagine = this.pagine + 1;
    }
    if (this.filtro) {
      this.loadFilter(this.pagine, this.quantita);
    } else {
      this.loadPage(this.pagine, this.quantita);
    }
  }

  prev() {
    if (this.pagine - 1 < 0) {
    } else {
      this.pagine = this.pagine + -1;
    }
    if (this.filtro) {
      this.loadFilter(this.pagine, this.quantita);
    } else {
      this.loadPage(this.pagine, this.quantita);
    }
  }

  reset() {
    this.pagine = 0;
    if (this.filtro) {
      this.loadFilter(this.pagine, this.quantita);
    } else {
      this.loadPage(this.pagine, this.quantita);
    }
  }

  last() {
    this.pagine = this.numeroPagine - 1;
    if (this.filtro) {
      this.loadFilter(this.pagine, this.quantita);
    } else {
      this.loadPage(this.pagine, this.quantita);
    }
  }

  isLastPage(): boolean {
    if (this.pagine == this.numeroPagine - 1) {
      return true;
    } else {
      return false;
    }
  }

  isFirstPage(): boolean {
    if (this.pagine == 0) {
      return true;
    } else {
      return false;
    }
  }

  showDialogConf(event: any) {
    this.getCorsoInfo(event);
    this.displayConf = true;
  }

  exit() {
    this.displayConf = false;
    this.onDialogCloseConf.emit(this.displayConf);
  }
  cercaGlobale() {
    console.log("value", this.formGroup.value);
    this.corsiService
      .filtro(this.formGroup.value, this.pagine, this.quantita)
      .subscribe((resp) => {
        if (this.formGroup.value == "") {
          this.filtro = false;
        } else {
          // console.log("form", this.formGroup.value);
          this.lista = resp.response;
          console.log("lista", this.lista);
          this.filtro = true;
        }
      });
    this.corsiService
      .getNumberOfPageFilter(this.formGroup.value, this.pagine, this.quantita)
      .subscribe((resp) => {
        this.numeroPagine = resp.response;
        // console.log(resp.response);
      });
  }
  loadFilter(pagine: number, quantita: number) {
    this.corsiService
      .filtro(this.formGroup.value, pagine, quantita)
      .subscribe((resp) => {
        this.lista = resp.response;
      });
    this.corsiService
      .getNumberOfPageFilter(this.formGroup.value, pagine, quantita)
      .subscribe((resp) => {
        this.numeroPagine = resp.response;
      });
    this.totCorsi();
  }
}
