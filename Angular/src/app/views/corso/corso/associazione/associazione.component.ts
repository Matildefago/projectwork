import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { AlunniApiService } from "../../../../api/alunni-api.service";
import { FormBuilder } from "@angular/forms";
import { CorsiApiService } from "../../../../api/corsi-api.service";
import { CorsiUtentiService } from "../../../../api/corsi-utenti.service";
import { Router } from "@angular/router";
import { DocentiApiService } from "../../../../api/docenti-api.service";
import { CorsiDocentiService } from "../../../../api/corsi-docenti.service";

@Component({
  selector: "app-associazione",
  templateUrl: "./associazione.component.html",
  styleUrls: ["./associazione.component.css"],
})
export class AssociazioneComponent implements OnInit {
  public dipendenti: any[];
  public listaDipendenti: any[];
  public listaDocenti: any[];
  public docenti: any[];
  public variabile: String;
  public dipendente: any;
  public associazione: any;
  public durata: any;
  listaAssociati: any[];
  public control = false;
  public associazioneDocenti: any;
  public listaDoppioni: any[];
  public doppione = false;
  showError: boolean = false;

  @Input()
  public displayAssociazione: boolean;
  @Output()
  onClick: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  onDialogCloseAssociazione: EventEmitter<any> = new EventEmitter<any>();
  formGroup: any;
  formGroupDocenti: any;

  @Input()
  set corso(item: any) {
    this._corso = item;
    if (item) {
      this.getDipendenti();
      this.getDocenti();
      this.buildFormVuoto();
      this.buildFormDocenti();
      console.log("SONO ENTRATO");
    }
  }

  get corso() {
    return this._corso;
  }

  public _corso: any;

  constructor(
    public dipService: AlunniApiService,
    public fb: FormBuilder,
    public corsoService: CorsiApiService,
    public associaService: CorsiUtentiService,
    public router: Router,
    public docentiService: DocentiApiService,
    public corsiDocService: CorsiDocentiService
  ) {}

  ngOnInit(): void {
    /* this.getDipendenti();
  this.buildFormVuoto(); */
  }

  getDipendenti() {
    if (this.dipendenti) {
      return this.dipendenti;
    } else {
      this.dipendenti = [];
      this.variabile = "";
      this.dipService.getAll().subscribe((resp) => {
        this.listaDipendenti = resp.response;

        this.listaDipendenti.forEach((element) => {
          if (this.listaDipendenti) {
            this.variabile =
              "" +
              element.matricola +
              " " +
              element.nome +
              " " +
              element.cognome +
              "";
            this.dipendenti.push({
              label: this.variabile,
              value: { element },
            });
          }
        });
      });
    }
  }

  getDocenti() {
    if (this.docenti) {
      return this.docenti;
    } else {
      this.docenti = [];
      this.variabile = "";
      this.docentiService.getAll().subscribe((resp) => {
        if (resp.response != null) {
          this.listaDocenti = resp.response;
          this.listaDocenti.forEach((element) => {
            if (this.listaDocenti) {
              this.variabile =
                "" + element.titolo + " " + element.ragioneSociale + " ";
              this.docenti.push({
                label: this.variabile,
                value: { element },
              });
            }
          });
        }
      });
    }
  }

  buildFormVuoto() {
    if (this.corso) {
      this.formGroup = this.fb.group({
        corso: [this.corso],
        dipendente: [""],
        durata: [""],
      });
    }
  }

  buildFormDocenti() {
    if (this.corso) {
      this.formGroupDocenti = this.fb.group({
        corso: [this.corso],
        docente: [""],
        interno: [this.control],
      });
    }
  }

  conferma() {
    this.associaService.getDipByCorso(this.corso.corsoId).subscribe((resp) => {
      this.listaDoppioni = resp.response;
      this.associazione = this.formGroup.value;
      this.listaAssociati = this.formGroup.value.dipendente;
      if (this.listaAssociati) {
        this.listaAssociati.forEach((element) => {
          if (this.associazione) {
            if (element && element != 0) {
              this.associazione.dipendente = element.value.element;
              this.associazione.corso = this.corso;
              this.associazione.durata = this.associazione.durata;
              if (this.listaDoppioni) {
                this.doppione = false;
                this.listaDoppioni.forEach((element) => {
                  if (
                    element.dipendente.dipendenteId ==
                    this.associazione.dipendente.dipendenteId
                  ) {
                    this.doppione = true;
                    return;
                  }
                });
                if (!this.doppione) {
                  this.associaService
                    .associaUtenti(this.associazione)
                    .subscribe((resp) => {
                      // console.log("ASS", resp.response);
                    });
                }
              }
            }
          }
        });
      }
    });

    if (this.corso) {
      // console.log("CORSO DEL DOCENTE", this.corso);
      this.corsiDocService
        .getDocByCorso(this.corso.corsoId)
        .subscribe((resp) => {
          if (resp.response.length > 0) {
            this.showError = true;
          } else {
            console.log("DOCENTE UNDEF", this.formGroupDocenti.value.docente);
            if (this.formGroupDocenti.value.docente) {
              this.associazioneDocenti = this.formGroupDocenti.value;
              console.log("doc err", this.formGroupDocenti.value);

              if (this.associazioneDocenti.interno == true) {
                this.associazioneDocenti.interno = 1;
                if(this.associazioneDocenti.docente.value.element.dipendenteId){
                  this.associazioneDocenti.docente = this.formGroupDocenti.value.docente.value.element.dipendenteId;
                console.log(
                  "DipDip",
                  this.associazioneDocenti.docente
                );
                }
                this.exit()
              } else {
                this.associazioneDocenti.interno = 0;
                if(this.associazioneDocenti.docente.value.element.docenteId){
                  this.associazioneDocenti.docente = this.formGroupDocenti.value.docente.value.element.docenteId;
                console.log(
                  "DOCDOC",
                  this.associazioneDocenti.docente 
                );
                }
              }
              this.corsiDocService
                .associaDocenti(this.associazioneDocenti)
                .subscribe((resp) => {
                  this.exit()
                  console.log("ASSOCIAZIONE DOC", this.associazioneDocenti);
                });
            }
          }
        });
    }

  
  }
  exit() {
    this.displayAssociazione = false;
    this.onDialogCloseAssociazione.emit(this.displayAssociazione);
    this.control = false;
    this.showError = false;
  }

  console() {
    console.log("BOOL", this.control);
  }
}
