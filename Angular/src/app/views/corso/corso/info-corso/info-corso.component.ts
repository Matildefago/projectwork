import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { CorsiApiService } from "../../../../api/corsi-api.service";
import { CorsiUtentiService } from "../../../../api/corsi-utenti.service";
import { CorsiDocentiService } from "../../../../api/corsi-docenti.service";
import { DocentiApiService } from "../../../../api/docenti-api.service";
import { AlunniApiService } from "../../../../api/alunni-api.service";

@Component({
  selector: "app-info-corso",
  templateUrl: "./info-corso.component.html",
  styleUrls: ["./info-corso.component.css"],
})
export class InfoCorsoComponent implements OnInit {
  @Input()
  public display: boolean;

  @Output()
  onDialogClose: EventEmitter<any> = new EventEmitter<any>();
  //Confirm Dialog
  // @Output()
  // onDialogCloseConf: EventEmitter<any> = new EventEmitter<any>();
  // @Input()
  // public displayConf: boolean;

  edit: boolean[] = [];
  val: number[];
  cols: any[];
  tipologia: String;
  ragioneSociale: String;
  misura: String;
  tipo: String;
  public associazioni: any[];
  public listaDip: any[];
  public colsAlunni: any[];
  data: any;
  dataE: String;
  dataCh: String;
  dataCe: String;

  docente: any[];
  dipendente: any[];
  associazioneDocente: any[];
  colsDocente: any[];

  @Input()
  set corso(item: any) {
    this._corso = item;
    if (item) {
      this.date();
      this.getDocenti();
      this.creaColonne();
      this.getAssociazioni();
    }
  }

  get corso() {
    return this._corso;
  }

  public _corso: any;

  constructor(
    private corsiService: CorsiApiService,
    private associaService: CorsiUtentiService,
    private corsiDocService: CorsiDocentiService,
    private docentiService: DocentiApiService,
    private dipendentiService: AlunniApiService
  ) {}

  ngOnInit(): void {}

  creaColonne() {
    this.cols = [
      { field: "descrizione", header: "Descrizione" },
      { field: "edizione", header: "Edizione" },
      { field: "previsto", header: "Previsto" },
      { field: "erogato", header: "Erogato" },
      { field: "durata", header: "Durata" },
      { field: "note", header: "Note" },
      { field: "luogo", header: "Luogo" },
      { field: "ente", header: "Ente" },
      { field: "interno", header: "Interno" },
    ];

    this.colsAlunni = [
      { field: "matricola", header: "Matricola" },
      { field: "nome", header: "Nome" },
      { field: "cognome", header: "Cognome" },
    ];

    this.colsDocente = [
      { field: "titolo", header: "Titolo" },
      { field: "ragioneSociale", header: "Ragione Sociale" },
      { field: "localita", header: "Località" },
      { field: "referente", header: "Referente" },
    ];
  }

  exit() {
    this.display = false;
    this.onDialogClose.emit(this.display);
    this.docente = null;
    this.dipendente = null;
  }
  //Confirm Dialog
  // showDialogConf(event: any) {
  //   this.displayConf = true;
  // }

  // exitConf() {
  //   this.displayConf = false;
  //   this.onDialogCloseConf.emit(this.displayConf);
  // }

  stringTipologia(corso: any): String {
    if (!corso) {
      return;
    }
    this.tipologia = JSON.stringify(corso.tipologia.descrizione);
    this.tipologia = this.tipologia.replace(/"/g, "");
    return this.tipologia;
  }

  stringSoc(corso: any): String {
    if (!corso) {
      return;
    }
    this.ragioneSociale = JSON.stringify(corso.societa.ragioneSociale);
    return this.ragioneSociale.replace(/"/g, "");
  }

  stringMis(corso: any): String {
    if (!corso) {
      return;
    }
    this.misura = JSON.stringify(corso.misura.descrizione);
    this.misura = this.misura.replace(/"/g, "");
    return this.misura;
  }

  stringTipo(corso: any): String {
    if (!corso) {
      return;
    }
    this.tipo = JSON.stringify(corso.tipo.descrizione);
    return this.tipo.replace(/"/g, "");
  }

  getAssociazioni() {
    this.listaDip = [];
    this.associaService.getDipByCorso(this.corso.corsoId).subscribe((resp) => {
      this.associazioni = resp.response;
      if (this.associazioni) {
        this.associazioni.forEach((element) => {
          this.listaDip.push(element.dipendente);
          this.edit.push(false);
        });
      }
    });
  }

  deleteAssociazione(item: any) {
    if (item) {
      var ass = this.select(item);
      this.associaService
        .eliminaAss(ass.corsiUtentiId, this.corso.corsoId)
        .subscribe((resp) => {
          this.getAssociazioni();
        });
    }
  }

  select(item: any) {
    return item;
  }

  date() {
    this.data = new Date(this.corso.dataErogazione);
    if (this.data.getDate() == 1) {
      this.dataE =
        this.data.getDate() +
        "/" +
        (this.data.getMonth() + 1) +
        "/" +
        this.data.getFullYear();
    } else {
      this.dataE =
        this.data.getDate() -
        1 +
        "/" +
        (this.data.getMonth() + 1) +
        "/" +
        this.data.getFullYear();
    }

    this.data = new Date(this.corso.dataCensimento);
    if (this.data.getDate() == 1) {
      this.dataCe =
        this.data.getDate() +
        "/" +
        (this.data.getMonth() + 1) +
        "/" +
        this.data.getFullYear();
    } else {
      this.dataCe =
        this.data.getDate() -
        1 +
        "/" +
        (this.data.getMonth() + 1) +
        "/" +
        this.data.getFullYear();
    }

    this.data = new Date(this.corso.dataChiusura);
    if (this.data.getDate() == 1) {
      this.dataCh =
        this.data.getDate() +
        "/" +
        (this.data.getMonth() + 1) +
        "/" +
        this.data.getFullYear();
    } else {
      this.dataCh =
        this.data.getDate() -
        1 +
        "/" +
        (this.data.getMonth() + 1) +
        "/" +
        this.data.getFullYear();
    }
  }

  getDocenti() {
    this.corsiDocService.getDocByCorso(this.corso.corsoId).subscribe((resp) => {
      this.associazioneDocente = resp.response;
      // console.log(this.associazioneDocente[0]);
      if (this.associazioneDocente[0]) {
        if (this.associazioneDocente[0].interno == 0) {
          this.docente = [];

          this.docentiService
            .getById(this.associazioneDocente[0].docente)
            .subscribe((resp) => {
              if (resp.response != null) {
                this.docente[0] = resp.response;
                // console.log("DOCENTE", this.docente);
              }
            });
        } else {
          this.dipendente = [];
          this.dipendentiService
            .getById(this.associazioneDocente[0].docente)
            .subscribe((resp) => {
              if (resp.response != null) {
                this.dipendente[0] = resp.response;
                // console.log("Dipendente", this.dipendente);
              }
            });
        }
      } else {
        this.dipendente = null;
        this.docente = null;
      }
    });
  }
  deleteDoc(item: any) {
    if (item) {
      var ass = this.select(item);
      this.corsiDocService.delete(ass.corsiDocentiId).subscribe((resp) => {
        this.getDocenti();
      });
    }
  }

  enableModifica(i: number) {
    this.edit[i] = true;
  }

  modificaDurata(item: any, i: number, ans: any) {
    if (ans == "y") {
      if (item) {
        var ass = this.select(item);
        this.associaService.modificaDurata(ass).subscribe((resp) => {
          this.getAssociazioni();
        });
      }
      this.edit[i] = false;
    }
    if (ans == "n") {
      this.edit[i] = false;
      this.getAssociazioni();
    }
  }
}
