import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CorsoRoutingModule } from "./corso-routing.module";
import { CorsoComponent } from "./corso.component";
import { TableModule } from "primeng/table";
import { ButtonModule } from "primeng/button";
import { DialogModule } from "primeng/dialog";
import { TabViewModule } from "primeng/tabview";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ScrollPanelModule } from "primeng/scrollpanel";
import { DropdownModule } from "primeng/dropdown";

import { ListboxModule } from "primeng/listbox";
import { CheckboxModule } from "primeng/checkbox";

import { InfoCorsoComponent } from "./info-corso/info-corso.component";
import { AssociazioneComponent } from "./associazione/associazione.component";
import {SpinnerModule} from 'primeng/spinner';

@NgModule({
  declarations: [CorsoComponent, InfoCorsoComponent, AssociazioneComponent],
  imports: [
    CommonModule,
    CorsoRoutingModule,
    TableModule,
    DropdownModule,
    ListboxModule,
    CheckboxModule,
    ButtonModule,
    DialogModule,
    TabViewModule,
    ScrollPanelModule,
    FormsModule,
    ReactiveFormsModule,
    SpinnerModule,
  ],
})
export class CorsoModule {}
