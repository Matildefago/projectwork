import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValDocenteComponent } from './val-docente.component';

describe('ValDocenteComponent', () => {
  let component: ValDocenteComponent;
  let fixture: ComponentFixture<ValDocenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValDocenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValDocenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
