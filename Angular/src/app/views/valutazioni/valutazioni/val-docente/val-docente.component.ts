import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ValDocentiService } from "../../../../api/val-docenti.service";
import { ValUtentiService } from "../../../../api/val-utenti.service";
import { CorsiUtentiService } from '../../../../api/corsi-utenti.service';

@Component({
  selector: "app-val-docente",
  templateUrl: "./val-docente.component.html",
  styleUrls: ["./val-docente.component.css"],
})
export class ValDocenteComponent implements OnInit {
  val1: number[] = [0, 0, 0, 0];
  val1m: number = 0;

  val2: number[] = [0, 0, 0, 0];
  val2m: number = 0;

  val3: number[] = [0, 0, 0, 0];
  val3m: number = 0;

  media: number = 0;
  _docenteEst: any;
  _docenteInt: any;
  formGroup: any;
  formGroupDefEst: any;
  formGroupDefInt: any;
  array: any[] = [];
  oggetto: any;
  interno: boolean = false;
  tot: number;
  showError: boolean = false;

  @Input()
  public display2: boolean;
  @Output()
  onDialogClose: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  set docenteEst(item: any) {
    this._docenteEst = item;
    if (item) {
      this.getInternoEsterno();
      if (!this.interno) {
        this.contaDip();
        this.buildForm();
        this.buildFormEst();
      } else {
        this.contaDip();
        this.buildForm();
        this.buildFormInt();
      }
      // console.log("SOGGETTO", item);
    }
  }

  get docenteEst() {
    return this._docenteEst;
  }

  constructor(
    public fb: FormBuilder,
    public valDocService: ValDocentiService,
    public valUteService: ValUtentiService,
    public gDip: CorsiUtentiService
  ) {}

  ngOnInit(): void {}

  buildForm() {
    this.formGroup = this.fb.group({
      criterio1: ["Completezza nelle Conoscenze"],
      criterio2: ["Chiarezza Espositiva"],
      criterio3: ["Disponibilità del Docente"],
      valore1: [this.val1m],
      valore2: [this.val2m],
      valore3: [this.val3m],
      valori: [""],
    });
  }

  getInternoEsterno() {
    if (this.docenteEst[0].docenteId) {
      this.interno = false;
      console.log("INTERNO", this.interno);
    } else {
      this.interno = true;
      // console.log("INTERNO", this.interno);
    }
  }

  buildFormEst() {
    this.formGroupDefEst = this.fb.group({
      corso: [this.docenteEst[1]],
      docente: [this.docenteEst[0]],
      criterio: [""],
      valore: [""],
    });
  }

  buildFormInt() {
    this.formGroupDefInt = this.fb.group({
      corso: [this.docenteEst[1]],
      dipendente: [this.docenteEst[0]],
      criterio: [""],
      valore: [""],
    });
  }

  exit() {
    this.display2 = false;
    this.onDialogClose.emit(this.display2);
    this.docenteEst = null;
    this.val1 = [0, 0, 0, 0];
    this.val2 = [0, 0, 0, 0];
    this.val3 = [0, 0, 0, 0];
    this.val1m = 0;
    this.val2m = 0;
    this.val3m = 0;
    this.media = 0;
    this.showError = false;
  }

  update(array: number[], result: number): number {
    var t = 0;
    array.forEach((val) => {
      t += val;
    });

    result = (array[0] * 1 + array[1] * 2 + array[2] * 3 + array[3] * 4) / t;

    if (isNaN(result)) {
      result = 0;
    }

    return result;
  }

  update1() {
    this.val1m = this.update(this.val1, this.val1m);
    this.val1m = Math.round(this.val1m * Math.pow(10, 2)) / Math.pow(10, 2);
    this.mediaT();
  }
  update2() {
    this.val2m = this.update(this.val2, this.val2m);
    this.val2m = Math.round(this.val2m * Math.pow(10, 2)) / Math.pow(10, 2);
    this.mediaT();
  }
  update3() {
    this.val3m = this.update(this.val3, this.val3m);
    this.val3m = Math.round(this.val3m * Math.pow(10, 2)) / Math.pow(10, 2);
    this.mediaT();
  }

  mediaT() {
    if (this.val1m == 0 || this.val2m == 0 || this.val3m == 0) {
      this.media = 0;
    } else {
      this.media = (this.val1m + this.val2m + this.val3m) / 3;
      this.media = Math.round(this.media * Math.pow(10, 2)) / Math.pow(10, 2);
    }
  }

  contaDip(){
    this.gDip.getDipByCorso(this.docenteEst[1].corsoId).subscribe((resp)=>{
      this.tot = resp.response.length;
    });
  }

  checkVal(): boolean {
    var in1 = 0;
    for (let index = 0; index < this.val1.length; index++) {
      in1 += this.val1[index];

    }
    if (in1 > this.tot) {//controlla se il primo parametro ha piu o meno voti dei partecipanti
      return false;
    } else {
      var in2 = 0;
      for (let index = 0; index < this.val2.length; index++) {
        in2 += this.val2[index];

      }
      if (in2 > this.tot) {//controlla se il secondo parametro ha piu o meno voti dei partecipanti
        return false;
      } else {
        var in3 = 0;
        for (let index = 0; index < this.val3.length; index++) {
          in3 += this.val3[index];

        }
        if (in3 > this.tot) {//controlla se il terzo parametro ha piu o meno voti dei partecipanti
          return false;
        } else {
              return true;
            }
          }
        }
      }


  conferma() {
    var conferma:boolean  = this.checkVal();
    if (!conferma) {
      this.showError = true;
    } else {
    if (this.formGroup.value) {
      this.formGroup.value.valore1 = this.val1m;
      this.formGroup.value.valore2 = this.val2m;
      this.formGroup.value.valore3 = this.val3m;
      this.array = [
        {
          label: this.formGroup.value.criterio1,
          value: this.formGroup.value.valore1,
        },
        {
          label: this.formGroup.value.criterio2,
          value: this.formGroup.value.valore2,
        },
        {
          label: this.formGroup.value.criterio3,
          value: this.formGroup.value.valore3,
        },
      ];
      if (!this.interno) {
        this.oggetto = this.formGroupDefEst.value;
        this.array.forEach((element) => {
          this.oggetto.criterio = element.label;
          this.oggetto.valore = element.value;
          this.valDocService.add(this.oggetto).subscribe((resp) => {
            // console.log("RESP", resp);
          });
        });
        this.exit();
      } else {
        this.oggetto = this.formGroupDefInt.value;
        this.array.forEach((element) => {
          this.oggetto.criterio = element.label;
          this.oggetto.valore = element.value;
          this.valUteService.add(this.oggetto).subscribe((resp) => {
            //  console.log("RESP", resp);
          });
        });
        this.exit();
      }
    }
  }
  }
}
