import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ValutazioniRoutingModule } from "./valutazioni-routing.module";
import { ValutazioniComponent } from "./valutazioni.component";
import { DropdownModule } from "primeng/dropdown";
import { TableModule } from "primeng/table";
import { ButtonModule } from "primeng/button";
import { DialogModule } from "primeng/dialog";
import { TabViewModule } from "primeng/tabview";
import { SliderModule } from "primeng/slider";
import { ScrollPanelModule } from "primeng/scrollpanel";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { InputTextModule } from "primeng/inputtext";
import { ValCorsoComponent } from "./val-corso/val-corso.component";
import { ListboxModule } from "primeng/listbox";
import { ValDocenteComponent } from "./val-docente/val-docente.component";
import { ValUtenteComponent } from "./val-utente/val-utente.component";
import { SpinnerModule } from "primeng/spinner";
import { InfoValUtenteComponent } from './info-val-utente/info-val-utente.component';
import { InfoValDocenteComponent } from './info-val-docente/info-val-docente.component';
import { InfoValCorsoComponent } from './info-val-corso/info-val-corso.component';

@NgModule({
  declarations: [
    ValutazioniComponent,
    ValCorsoComponent,
    ValDocenteComponent,
    ValUtenteComponent,
    InfoValUtenteComponent,
    InfoValDocenteComponent,
    InfoValCorsoComponent,
  ],
  imports: [
    CommonModule,
    ValutazioniRoutingModule,
    DropdownModule,
    ListboxModule,
    TableModule,
    ButtonModule,
    ScrollPanelModule,
    DialogModule,
    TabViewModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    SliderModule,
    SpinnerModule,
  ],
})
export class ValutazioniModule {}
