import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoValCorsoComponent } from './info-val-corso.component';

describe('InfoValCorsoComponent', () => {
  let component: InfoValCorsoComponent;
  let fixture: ComponentFixture<InfoValCorsoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoValCorsoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoValCorsoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
