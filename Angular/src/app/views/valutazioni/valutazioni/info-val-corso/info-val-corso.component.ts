import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ValCorsiService } from "../../../../api/val-corsi.service";

@Component({
  selector: "app-info-val-corso",
  templateUrl: "./info-val-corso.component.html",
  styleUrls: ["./info-val-corso.component.css"],
})
export class InfoValCorsoComponent implements OnInit {
  public _corso: any;
  public valutazione: any[];
  colsInfo: any[] = [];
  colsVal: any[] = [];

  @Input()
  public displayInfoCorso: boolean;
  @Output()
  onDialogClose: EventEmitter<any> = new EventEmitter<any>();
  //Confirm Dialog
  @Output()
  onDialogCloseConf: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  public displayConf: boolean;

  @Input()
  set corso(item: any) {
    this._corso = item;
    if (item) {
      this.getValutazione(this.corso.corsoId);
    }
  }

  get corso() {
    return this._corso;
  }

  constructor(public service: ValCorsiService) {}

  ngOnInit(): void {}

  getValutazione(id: any) {
    this.service.info(id).subscribe((resp) => {
      this.valutazione = resp.response;
    });

    this.colsVal = [
      { field: "criterio", header: "Criterio" },
      { field: "valore", header: "Valore" },
    ];
  }
  exit() {
    this.displayInfoCorso = false;
    this.getValutazione(this.corso.corsoId);
    this.onDialogClose.emit(this.displayInfoCorso);
    /* this.valutazione = []; */
  }
  //Confirm Dialog
  showDialogConf() {
    this.displayConf = true;
  }

  exitConf() {
    this.displayConf = false;
    this.onDialogCloseConf.emit(this.displayInfoCorso);
    this.displayInfoCorso = true;
  }

  elimina() {
    if (this.valutazione) {
      this.valutazione.forEach((element) => {
        this.service
          .elimina(element.valutazioniCorsiId)
          .subscribe((resp) => {});
      });
      this.exit();
    }
  }
}
