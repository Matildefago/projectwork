import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { ValUtentiService } from "../../../api/val-utenti.service";

import { ValDocentiService } from "../../../api/val-docenti.service";
import { ValCorsiService } from "../../../api/val-corsi.service";
import { CorsiUtentiService } from "../../../api/corsi-utenti.service";
import { CorsiDocentiService } from "../../../api/corsi-docenti.service";
import { FormBuilder, FormGroup } from "@angular/forms";
import { CorsiApiService } from "../../../api/corsi-api.service";
import { AlunniApiService } from "../../../api/alunni-api.service";
import { DocentiApiService } from "../../../api/docenti-api.service";

@Component({
  selector: "app-valutazioni",
  templateUrl: "./valutazioni.component.html",
  styleUrls: ["./valutazioni.component.css"],
})
export class ValutazioniComponent implements OnInit {
  //Dialogs
  public display3: boolean = false;
  public display2: boolean = false;
  public display1: boolean = false;
  public displayInfoUtente: boolean = false;
  public displayInfoCorso: boolean = false;
  public displayInfoDocente: boolean = false;

  colsValutazioniUtenti: any[];
  colsValutazioniDocenti: any[];
  colsValutazioniCorsi: any[];
  public utenti: any[];
  public docenti: any[];
  public corsi: any[];
  public nome: String;
  public utente: any;

  public listaCorsiUtenti: any[];
  public listaDipendenti: any[];
  public listaCorsi: any[];
  //paging Utente
  public numPageUtente: any;
  public listaTotUtenti: any[];
  public pagineUtente: number = 0;
  public quantitaUtente: number = 8;
  //paging Corsi
  public pagineCorsi: number = 0;
  public quantitaCorsi: number = 8;
  public numPageCorsi: any;
  //paging  Docenti Esterni
  public pagineDocEst: number = 0;
  public quantitaDocEst: number = 8;
  public numPageDocEst: any;
  //Paging Docenti Interni
  public pagineDocInt: number = 0;
  public quantitaDocInt: number = 8;
  public numPageDocInt: any;

  public colsDocenti: any[] = [];
  public colsAlunni: any[];
  public utenteStringa: "utente";
  public corsoStringa: "corso";
  public docEstStringa: "docEst";
  public docIntStringa: "docInt";
  public soggetto: any;
  public soggettoInfo: any;
  public descrizione: any;
  public totUtenti: any;
  public totCorsi: any;

  public corso: any;
  public colsCorso: any[];
  public tipologia: any;
  public ragioneSociale: any;
  //Lista Docenti
  public assDocEst: any[];
  public listaDocEsterni: any[];
  public assDocInt: any[];
  public listaDocInterni: any[];
  //Info Docenti
  public docenteEst: any[];
  public docenteInt: any[];
  public str: "esterno";
  //filtro
  public formGroup: FormGroup;
  public filtro: boolean;

  @Output()
  onSend: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  onInfo: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    public apiUtenti: ValUtentiService,
    public apiDocenti: ValDocentiService,
    public apiCorsi: ValCorsiService,
    public apiCorsiUtenti: CorsiUtentiService,
    public apiCorsiDocenti: CorsiDocentiService,
    public corsiService: CorsiApiService,
    public alunniService: AlunniApiService,
    public docentiService: DocentiApiService,
    public fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.totaleUtenti();
    this.totaleCorsi();
    this.loadDocentiEst(this.pagineDocEst, this.quantitaDocEst);
    this.loadUtenti(this.pagineUtente, this.quantitaUtente);
    this.loadCorsi(this.pagineCorsi, this.quantitaCorsi);
    this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);

    this.formGroup = this.fb.group({
      cerca: [""],
    });
  }

  totaleUtenti() {
    this.apiCorsiUtenti.getAll().subscribe((resp) => {
      this.totUtenti = resp.response;
    });
  }

  totaleCorsi() {
    this.corsiService.getAll().subscribe((resp) => {
      this.totCorsi = resp.response;
    });
  }

  stringDescr(item: any): String {
    if (!item) {
      return;
    }
    this.descrizione = JSON.stringify(item.corso.descrizione);
    return this.descrizione.replace(/"/g, "");
  }

  loadUtenti(pagine: number, quantita: number) {
    this.apiCorsiUtenti.getNumberOfPage(pagine, quantita).subscribe((resp) => {
      this.numPageUtente = resp.response;
      console.log("num pagine utente", this.numPageUtente);
    });

    this.apiCorsiUtenti.getAllPage(pagine, quantita).subscribe((resp) => {
      this.colsAlunni = [
        { field: "matricola", header: "Matricola" },
        { field: "nome", header: "Nome" },
        { field: "cognome", header: "Cognome" },
      ];
      this.listaDipendenti = [];
      this.listaTotUtenti = resp.response;
      this.listaTotUtenti.forEach((element) => {
        this.listaDipendenti.push(element.dipendente);
      });
    });
  }

  loadCorsi(pagine: number, quantita: number) {
    this.corsiService.getNumberOfPage(pagine, quantita).subscribe((resp) => {
      this.numPageCorsi = resp.response;
    });

    this.corsiService.getAllPage(pagine, quantita).subscribe((resp) => {
      this.colsCorso = [
        { field: "descrizione", header: "Descrizione" },
        { field: "luogo", header: "Luogo" },
        { field: "ente", header: "Ente" },
      ];
      this.listaCorsi = resp.response;
      // console.log(resp.response);
    });
    this.totaleCorsi();
  }

  string(corso: any): String {
    if (!corso) {
      return;
    }
    // console.log("corso", corso);
    this.tipologia = JSON.stringify(corso.tipologia.descrizione);

    this.tipologia = this.tipologia.replace(/"/g, "");
    return this.tipologia;
  }

  stringSoc(corso: any): String {
    if (!corso) {
      return;
    }
    this.ragioneSociale = JSON.stringify(corso.societa.ragioneSociale);
    return this.ragioneSociale.replace(/"/g, "");
  }

  loadDocentiEst(pagine: number, quantita: number) {
    this.apiCorsiDocenti.numberEsterni(pagine, quantita).subscribe((resp) => {
      this.numPageDocEst = resp.response;
      this.colsDocenti = [
        { field: "titolo", header: "Titolo" },
        { field: "ragioneSociale", header: "Ragione Sociale" },
        { field: "localita", header: "Località" },
        { field: "referente", header: "Referente" },
      ];
    });
    this.apiCorsiDocenti.getPagesEsterni(pagine, quantita).subscribe((resp) => {
      this.assDocEst = [];
      this.assDocEst = resp.response;
      this.getDocenti();
      console.log("ASSSS", this.assDocEst);
    });
  }

  getDocenti() {
    this.listaDocEsterni = [];
    if (this.assDocEst) {
      this.assDocEst.forEach((element) => {
        this.docentiService.getById(element.docente).subscribe((resp) => {
          if (resp.response == null) {
            this.apiCorsiDocenti
              .delete(element.corsiDocentiId)
              .subscribe((resp) => {
                console.log("ELIMINATO", resp.messagge);
              });
          } else {
            this.listaDocEsterni.push(resp.response);
          }
        });
      });
    }
  }

  loadDocentiInt(pagine: number, quantita: number) {
    this.apiCorsiDocenti.numberInterni(pagine, quantita).subscribe((resp) => {
      this.numPageDocInt = resp.response;
    });

    this.apiCorsiDocenti.getPagesInterni(pagine, quantita).subscribe((resp) => {
      this.assDocInt = resp.response;
      this.listaDocInterni = [];
      if (this.assDocInt) {
        this.assDocInt.forEach((element) => {
          this.alunniService.getById(element.docente).subscribe((resp) => {
            if (resp.response != null) {
              this.listaDocInterni.push(resp.response);
            }
          });
        });
      }
    });
  }

  inviaUtente(item: any) {
    this.soggetto = item;
    this.onSend.emit(this.soggetto);
  }

  inviaCorso(item: any) {
    this.corso = item;
    this.onSend.emit(this.corso);
  }

  inviaDocenteEst(item: any, index: any) {
    this.docenteEst = [];
    this.docenteEst.push(item);
    this.docenteEst.push(this.assDocEst[index].corso);
    this.onSend.emit(this.docenteEst);
  }

  inviaDocenteInt(item: any, index: any) {
    this.docenteEst = [];
    this.docenteEst.push(item);
    this.docenteEst.push(this.assDocInt[index].corso);
    this.onSend.emit(this.docenteEst);
  }

  inviaInfo(item: any) {
    this.soggettoInfo = item;
    this.onInfo.emit(this.soggettoInfo);
  }

  showDialog1(item: any) {
    console.log("item", item);
    this.inviaUtente(item);
    this.display1 = true;
    // console.log(this.display2);
  }

  onDialogClose1(event: any) {
    this.display1 = event;
    this.loadUtenti(this.pagineUtente, this.quantitaUtente);
  }

  showDialog2(item: any, index: any, str: string) {
    if (str == "interno") {
      this.display2 = true;
      this.inviaDocenteInt(item, index);
      // console.log(this.display2);
    } else {
      this.display2 = true;
      this.inviaDocenteEst(item, index);
      // console.log(this.display2);
    }
  }
  onDialogClose2(event: any) {
    this.display2 = event;
    /* this.loadDocentiEst(this.pagineDocEst, this.quantitaDocEst);
    this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt); */
  }

  showDialogInfoDoc(item: any, index: any, str: string) {
    if (str == "interno") {
      this.displayInfoDocente = true;
      this.inviaDocenteInt(item, index);
      // console.log(this.displayInfoDocente);
    } else {
      this.displayInfoDocente = true;
      this.inviaDocenteEst(item, index);
      console.log(this.displayInfoDocente);
    }
  }
  onDialogCloseInfoDoc(event: any) {
    this.displayInfoDocente = event;
  }

  showDialog3(item: any) {
    console.log("itemCorso", item);
    this.inviaCorso(item);
    this.display3 = true;
    // console.log(this.display3);
  }
  onDialogClose3(event: any) {
    this.display3 = event;
    this.loadCorsi(this.pagineCorsi, this.quantitaCorsi);
  }
  nextDocInt() {
    if (this.pagineDocInt + 1 > this.numPageDocInt - 1) {
    } else {
      this.pagineDocInt = this.pagineDocInt + 1;
    }
    this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);
  }

  nextDocEst() {
    if (this.pagineDocEst + 1 > this.numPageDocEst - 1) {
    } else {
      this.pagineDocEst = this.pagineDocEst + 1;
    }
    this.loadDocentiEst(this.pagineDocEst, this.quantitaDocEst);
  }

  nextCorso() {
    if (this.pagineCorsi + 1 > this.numPageCorsi - 1) {
    } else {
      this.pagineCorsi = this.pagineCorsi + 1;
    }
    this.loadCorsi(this.pagineCorsi, this.quantitaCorsi);
  }

  nextUte() {
    if (this.pagineUtente + 1 > this.numPageUtente - 1) {
    } else {
      this.pagineUtente = this.pagineUtente + 1;
    }
    if (this.filtro) {
      this.cercaGlobaleUtente(this.pagineUtente, this.quantitaUtente);
    } else {
      this.loadUtenti(this.pagineUtente, this.quantitaUtente);
    }
  }

  prevUte() {
    if (this.pagineUtente - 1 < 0) {
    } else {
      this.pagineUtente = this.pagineUtente + -1;
    }
    if (this.filtro) {
      this.cercaGlobaleUtente(this.pagineUtente, this.quantitaUtente);
    } else {
      this.loadUtenti(this.pagineUtente, this.quantitaUtente);
    }
  }
  prevCorso() {
    if (this.pagineCorsi - 1 < 0) {
    } else {
      this.pagineCorsi = this.pagineCorsi + -1;
    }
    this.loadCorsi(this.pagineCorsi, this.quantitaCorsi);
  }
  prevDocEst() {
    if (this.pagineDocEst - 1 < 0) {
    } else {
      this.pagineDocEst = this.pagineDocEst + -1;
    }
    this.loadDocentiEst(this.pagineDocEst, this.quantitaDocEst);
  }
  prevDocInt() {
    if (this.pagineDocInt - 1 < 0) {
    } else {
      this.pagineDocInt = this.pagineDocInt + -1;
    }
    this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);
  }

  resetUte() {
    this.pagineUtente = 0;
    if (this.filtro) {
      this.cercaGlobaleUtente(this.pagineUtente, this.quantitaUtente);
    } else {
      this.loadUtenti(this.pagineUtente, this.quantitaUtente);
    }
  }
  resetCorso() {
    this.pagineCorsi = 0;
    this.loadCorsi(this.pagineCorsi, this.quantitaCorsi);
  }
  resetDocEst() {
    this.pagineDocEst = 0;
    this.loadDocentiEst(this.pagineDocEst, this.quantitaDocEst);
  }

  resetDocInt() {
    this.pagineDocInt = 0;
    this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);
  }

  lastUte() {
    this.pagineUtente = this.numPageUtente - 1;
    if (this.filtro) {
      this.cercaGlobaleUtente(this.pagineUtente, this.quantitaUtente);
    } else {
      this.loadUtenti(this.pagineUtente, this.quantitaUtente);
    }
  }
  lastCorso() {
    this.pagineCorsi = this.numPageCorsi - 1;
    this.loadCorsi(this.pagineCorsi, this.quantitaCorsi);
  }
  lastDocEst() {
    this.pagineDocEst = this.numPageDocEst - 1;
    this.loadDocentiEst(this.pagineDocEst, this.quantitaDocEst);
  }
  lastDocInt() {
    this.pagineDocInt = this.numPageDocInt - 1;
    this.loadDocentiInt(this.pagineDocInt, this.quantitaDocInt);
  }

  isLastPageUte(): boolean {
    if (this.pagineUtente == this.numPageUtente - 1) {
      return true;
    } else {
      return false;
    }
  }

  isLastPageCorso(): boolean {
    if (this.pagineCorsi == this.numPageCorsi - 1) {
      return true;
    } else {
      return false;
    }
  }
  isLastPageDocEst(): boolean {
    if (this.pagineDocEst == this.numPageDocEst - 1) {
      return true;
    } else {
      return false;
    }
  }

  isLastPageDocInt(): boolean {
    if (this.pagineDocInt == this.numPageDocInt - 1) {
      return true;
    } else {
      return false;
    }
  }

  isFirstPageUte(): boolean {
    if (this.pagineUtente == 0) {
      return true;
    } else {
      return false;
    }
  }

  isFirstPageCorso(): boolean {
    if (this.pagineCorsi == 0) {
      return true;
    } else {
      return false;
    }
  }

  isFirstPageDocEst(): boolean {
    if (this.pagineDocEst == 0) {
      return true;
    } else {
      return false;
    }
  }

  isFirstPageDocInt(): boolean {
    if (this.pagineDocInt == 0) {
      return true;
    } else {
      return false;
    }
  }

  showDialogInfoUtente(item: any) {
    console.log("item", item);
    this.inviaInfo(item);
    this.displayInfoUtente = true;
    console.log(this.displayInfoUtente);
  }

  onDialogCloseInfoUtente(event: any) {
    this.displayInfoUtente = event;
  }

  showDialogInfoCorso(item: any) {
    console.log("item", item);
    this.inviaCorso(item);
    this.displayInfoCorso = true;
    // console.log(this.displayInfoCorso);
  }

  onDialogCloseInfoCorso(event: any) {
    this.displayInfoCorso = event;
  }
  cercaGlobaleUtente(pagine: number, quantita: number) {
    console.log("value", this.formGroup.value);
    if (this.formGroup.value) {
      this.apiCorsiUtenti
        .filter(pagine, quantita, this.formGroup.value)
        .subscribe((resp) => {
          if (this.formGroup.value == "") {
            this.filtro = false;
          } else {
            this.listaDipendenti = [];
            this.listaTotUtenti = resp.response;
            this.listaTotUtenti.forEach((element) => {
              this.listaDipendenti.push(element.dipendente);
            });
            console.log("lista", this.listaDipendenti);
            this.filtro = true;
          }
        });
    }

    this.apiCorsiUtenti
      .getNumberFiltro(
        this.pagineUtente,
        this.quantitaUtente,
        this.formGroup.value
      )
      .subscribe((resp) => {
        this.numPageUtente = resp.response;
        console.log(resp.response);
      });
  }
  cercaGlobaleDocente() {
    console.log("value", this.formGroup.value);
    this.apiDocenti.filtro(this.formGroup.value).subscribe((resp) => {
      if (this.formGroup.value == "") {
        this.filtro = false;
      } else {
        this.listaCorsiUtenti = resp.response;
        console.log("lista", this.listaCorsiUtenti);
        this.filtro = true;
      }
    });
  }
  cercaGlobaleCorso() {
    console.log("value", this.formGroup.value);
    this.apiCorsi.filtro(this.formGroup.value).subscribe((resp) => {
      if (this.formGroup.value == "") {
        this.filtro = false;
      } else {
        this.listaCorsiUtenti = resp.response;
        console.log("lista", this.listaCorsiUtenti);
        this.filtro = true;
      }
    });
  }
}
