import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { isGeneratedFile } from "@angular/compiler/src/aot/util";
import { ValCorsiService } from "../../../../api/val-corsi.service";
import { CorsiUtentiService } from '../../../../api/corsi-utenti.service';

@Component({
  selector: "app-val-corso",
  templateUrl: "./val-corso.component.html",
  styleUrls: ["./val-corso.component.css"],
})
export class ValCorsoComponent implements OnInit {
  val1: number[] = [0, 0, 0, 0];
  val1m: number = 0;

  val2: number[] = [0, 0, 0, 0];
  val2m: number = 0;

  val3: number[] = [0, 0, 0, 0];
  val3m: number = 0;

  val4: number[] = [0, 0, 0, 0];
  val4m: number = 0;

  val5: number[] = [0, 0, 0, 0];
  val5m: number = 0;

  media: number = 0;

  _corso: any;
  tot:any;
  formGroup: any;
  formGroupDef: any;
  array: any[] = [];
  oggetto: any;
  showError: boolean = false;

  @Input()
  set corso(item: any) {
    this._corso = item;
    if (item) {
      this.buildForm();
      this.buildFormDef();
      this.contaDip();
    }
  }

  get corso() {
    return this._corso;
  }

  @Input()
  public display3: boolean;
  @Output()
  onDialogClose: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    public fb: FormBuilder,
    public valCorsiService: ValCorsiService,
    public gDip: CorsiUtentiService
  ) { }

  ngOnInit(): void { }

  contaDip(){
    this.gDip.getDipByCorso(this.corso.corsoId).subscribe((resp)=>{
      this.tot = resp.response.length;
    });
  }

  exit() {
    this.display3 = false;
    this.onDialogClose.emit(this.display3);
    this.val1 = [0, 0, 0, 0];
    this.val2 = [0, 0, 0, 0];
    this.val3 = [0, 0, 0, 0];
    this.val4 = [0, 0, 0, 0];
    this.val5 = [0, 0, 0, 0];
    this.val1m = 0;
    this.val2m = 0;
    this.val3m = 0;
    this.val4m = 0;
    this.val5m = 0;
    this.media = 0;
    this.formGroupDef.value.criterio = [""];
    this.formGroupDef.value.valore = [""];
    this.showError = false;
  }

  update(array: number[], result: number): number {
    var t = 0;
    array.forEach((val) => {
      t += val;
    });

    result = (array[0] * 1 + array[1] * 2 + array[2] * 3 + array[3] * 4) / t;

    if (isNaN(result)) {
      result = 0;
    }

    return result;
  }

  update1() {
    this.val1m = this.update(this.val1, this.val1m);
    this.val1m = Math.round(this.val1m * Math.pow(10, 2)) / Math.pow(10, 2);
    this.mediaT();
  }
  update2() {
    this.val2m = this.update(this.val2, this.val2m);
    this.val2m = Math.round(this.val2m * Math.pow(10, 2)) / Math.pow(10, 2);
    this.mediaT();
  }
  update3() {
    this.val3m = this.update(this.val3, this.val3m);
    this.val3m = Math.round(this.val3m * Math.pow(10, 2)) / Math.pow(10, 2);
    this.mediaT();
  }
  update4() {
    this.val4m = this.update(this.val4, this.val4m);
    this.val4m = Math.round(this.val4m * Math.pow(10, 2)) / Math.pow(10, 2);
    this.mediaT();
  }
  update5() {
    this.val5m = this.update(this.val5, this.val5m);
    this.val5m = Math.round(this.val5m * Math.pow(10, 2)) / Math.pow(10, 2);
    this.mediaT();
  }

  mediaT() {
    if (
      this.val1m == 0 ||
      this.val2m == 0 ||
      this.val3m == 0 ||
      this.val4m == 0 ||
      this.val5m == 0
    ) {
      this.media = 0;
    } else {
      this.media =
        (this.val1m + this.val2m + this.val3m + this.val4m + this.val5m) / 4;
      this.media = Math.round(this.media * Math.pow(10, 2)) / Math.pow(10, 2);
    }
  }

  buildForm() {
    this.formGroup = this.fb.group({
      corso: [this.corso],
      criterio1: ["Utilità del Corso per l'ambito lavorativo"],
      criterio2: ["Scelta nella selezione degli argomenti"],
      criterio3: ["Durata del Corso"],
      criterio4: ["Efficacia delle Esercitazioni"],
      criterio5: ["Qualità del Materiale Didattico"],
      valore1: [this.val1m],
      valore2: [this.val2m],
      valore3: [this.val3m],
      valore4: [this.val4m],
      valore5: [this.val5m],
      valori: [""],
    });
  }

  buildFormDef() {
    this.formGroupDef = this.fb.group({
      corso: [this.corso],
      criterio: [""],
      valore: [""],
    });
  }

  checkVal(): boolean {
    var in1 = 0;
    for (let index = 0; index < this.val1.length; index++) {
      in1 += this.val1[index];

    }
    if (in1 > this.tot) {//controlla se il primo parametro ha piu o meno voti dei partecipanti
      return false;
    } else {
      var in2 = 0;
      for (let index = 0; index < this.val2.length; index++) {
        in2 += this.val2[index];

      }
      if (in2 > this.tot) {//controlla se il secondo parametro ha piu o meno voti dei partecipanti
        return false;
      } else {
        var in3 = 0;
        for (let index = 0; index < this.val3.length; index++) {
          in3 += this.val3[index];

        }
        if (in3 > this.tot) {//controlla se il terzo parametro ha piu o meno voti dei partecipanti
          return false;
        } else {
          var in4 = 0;
          for (let index = 0; index < this.val4.length; index++) {
            in4 += this.val4[index];

          }
          if (in4 > this.tot) {//controlla se il quarto parametro ha piu o meno voti dei partecipanti
            return false;
          } else {
            var in5 = 0;
            for (let index = 0; index < this.val5.length; index++) {
              in5 += this.val5[index];

            } if (in1 > this.tot) {//controlla se il quinto parametro ha piu o meno voti dei partecipanti
              return false;
            } else {
              return true;
            }
          }
        }
      }
    }
  }


  conferma() {
    var conferma:boolean  = this.checkVal();
    if (!conferma) {
      this.showError = true;
    } else {
      if (this.formGroup.value) {
        this.formGroup.value.valore1 = this.val1m;
        this.formGroup.value.valore2 = this.val2m;
        this.formGroup.value.valore3 = this.val3m;
        this.formGroup.value.valore4 = this.val4m;
        this.formGroup.value.valore5 = this.val5m;
        this.array = [
          {
            label: this.formGroup.value.criterio1,
            value: this.formGroup.value.valore1,
          },
          {
            label: this.formGroup.value.criterio2,
            value: this.formGroup.value.valore2,
          },
          {
            label: this.formGroup.value.criterio3,
            value: this.formGroup.value.valore3,
          },
          {
            label: this.formGroup.value.criterio4,
            value: this.formGroup.value.valore4,
          },
          {
            label: this.formGroup.value.criterio5,
            value: this.formGroup.value.valore5,
          },
        ];
        this.oggetto = this.formGroupDef.value;
        this.array.forEach((element) => {
          this.oggetto.criterio = element.label;
          this.oggetto.valore = element.value;
          this.valCorsiService.add(this.oggetto).subscribe((resp) => {
            this.exit();
          });
        });

      }
    }
  }
}
