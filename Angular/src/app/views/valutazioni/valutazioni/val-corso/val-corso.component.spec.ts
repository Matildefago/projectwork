import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValCorsoComponent } from './val-corso.component';

describe('ValCorsoComponent', () => {
  let component: ValCorsoComponent;
  let fixture: ComponentFixture<ValCorsoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValCorsoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValCorsoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
