import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ValUtentiService } from "../../../../api/val-utenti.service";

@Component({
  selector: "app-info-val-utente",
  templateUrl: "./info-val-utente.component.html",
  styleUrls: ["./info-val-utente.component.css"],
})
export class InfoValUtenteComponent implements OnInit {
  public _soggettoInfo: any;
  public valutazione: any[];
  colsInfo: any[] = [];
  colsVal: any[] = [];
  array: any[] = [];

  @Input()
  public displayInfoUtente: boolean;
  @Output()
  onDialogCloseInfo: EventEmitter<any> = new EventEmitter<any>();
  //Confirm Dialog
  @Output()
  onDialogCloseConf: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  public displayConf: boolean;

  @Input()
  set soggettoInfo(item: any) {
    this._soggettoInfo = item;
    if (this._soggettoInfo) {
      this.getValutazione(
        this.soggettoInfo.dipendente.dipendenteId,
        this.soggettoInfo.corso.corsoId
      );
    }
  }

  get soggettoInfo() {
    return this._soggettoInfo;
  }

  constructor(public service: ValUtentiService) {}

  ngOnInit(): void {}

  getValutazione(idDip: any, idCors: any) {
    console.log("AOOOO");
    this.service.getInfo(idDip, idCors).subscribe((resp) => {
      this.valutazione = [];
      this.array = resp.response;
      if (this.array) {
        this.array.forEach((element) => {
          if (
            element.criterio != "Completezza nelle Conoscenze" ||
            element.criterio != "Disponibilità del Docente" ||
            element.criterio != "Chiarezza Espositiva"
          ) {
            this.valutazione.push(element);
          }
        });
      }
      // console.log("VAL", this.valutazione);
    });

    this.colsVal = [
      { field: "criterio", header: "Criterio" },
      { field: "valore", header: "Valore" },
    ];
  }

  exit() {
    this.displayInfoUtente = false;
    this.getValutazione(
      this.soggettoInfo.dipendente.dipendenteId,
      this.soggettoInfo.corso.corsoId
    );
    this.onDialogCloseInfo.emit(this.displayInfoUtente);
   /*  this.valutazione = null; */
  }

  elimina() {
    if (this.valutazione) {
      this.valutazione.forEach((element) => {
        this.service
          .elimina(element.valutazioniUtentiId)
          .subscribe((resp) => {});
      });
      this.exit();
    }
  }
  showDialogConf() {
    this.displayConf = true;
  }

  exitConf() {
    this.displayConf = false;
    this.onDialogCloseConf.emit(this.displayInfoUtente);
  }
}
