import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoValUtenteComponent } from './info-val-utente.component';

describe('InfoValUtenteComponent', () => {
  let component: InfoValUtenteComponent;
  let fixture: ComponentFixture<InfoValUtenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoValUtenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoValUtenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
