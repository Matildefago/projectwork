import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ValUtentiService } from "../../../../api/val-utenti.service";
import { ValDocentiService } from "../../../../api/val-docenti.service";

@Component({
  selector: "app-info-val-docente",
  templateUrl: "./info-val-docente.component.html",
  styleUrls: ["./info-val-docente.component.css"],
})
export class InfoValDocenteComponent implements OnInit {
  _docenteEst: any;
  interno: boolean = false;
  valutazione: any;
  colsVal: any[] = [];
  array: any[] = [];

  @Input()
  public displayInfoDocente: boolean;
  @Output()
  onDialogClose: EventEmitter<any> = new EventEmitter<any>();
  //Confirm Dialog
  @Output()
  onDialogCloseConf: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  public displayConf: boolean;

  @Input()
  set docenteEst(item: any) {
    this._docenteEst = item;
    if (item) {
      this.getInternoEsterno();
      if (!this.interno) {
        this.getValutazioneEst(
          this.docenteEst[0].docenteId,
          this.docenteEst[1].corsoId
        );
      } else {
        this.getValutazioneInt(
          this.docenteEst[0].dipendenteId,
          this.docenteEst[1].corsoId
        );
      }
    }
  }

  get docenteEst() {
    return this._docenteEst;
  }

  constructor(
    public valUteService: ValUtentiService,
    public valDocService: ValDocentiService
  ) {}

  getInternoEsterno() {
    if (this.docenteEst[0].docenteId) {
      this.interno = false;
      // console.log("INTERNO", this.interno);
    } else {
      this.interno = true;
      // console.log("INTERNO", this.interno);
    }
  }

  getValutazioneInt(idDip: any, idCors: any) {
    this.valUteService.getInfo(idDip, idCors).subscribe((resp) => {
      this.array = resp.response;
      this.valutazione = [];
      if (this.array) {
        this.array.forEach((element) => {
          if (
            element.criterio == "Completezza nelle Conoscenze" ||
            element.criterio == "Disponibilità del Docente" ||
            element.criterio == "Chiarezza Espositiva"
          ) {
            this.valutazione.push(element);
          }
        });
      }
    });

    this.colsVal = [
      { field: "criterio", header: "Criterio" },
      { field: "valore", header: "Valore" },
    ];
  }

  getValutazioneEst(idDoc: any, idCors: any) {
    this.valDocService.info(idDoc, idCors).subscribe((resp) => {
      this.valutazione = resp.response;
      // console.log("VAL", this.valutazione);
    });

    this.colsVal = [
      { field: "criterio", header: "Criterio" },
      { field: "valore", header: "Valore" },
    ];
  }

  exit() {
    this.displayInfoDocente = false;
    this.getInternoEsterno();
    if (!this.interno) {
      this.getValutazioneEst(
        this.docenteEst[0].docenteId,
        this.docenteEst[1].corsoId
      );
    } else {
      this.getValutazioneInt(
        this.docenteEst[0].dipendenteId,
        this.docenteEst[1].corsoId
      );
    }
    this.onDialogClose.emit(this.displayInfoDocente);
    this.valutazione = [];
  }

  //Confirm Dialog
  showDialogConf() {
    this.displayConf = true;
  }

  exitConf() {
    this.displayConf = false;
    this.onDialogCloseConf.emit(this.displayInfoDocente);
  }

  elimina() {
    if (this.valutazione) {
      if (!this.interno) {
        this.valutazione.forEach((element) => {
          this.valDocService
            .elimina(element.valutazioniDocentiId)
            .subscribe((resp) => {});
        });
        this.exit();
      } else {
        this.valutazione.forEach((element) => {
          this.valUteService
            .elimina(element.valutazioniUtentiId)
            .subscribe((resp) => {});
        });
        this.exit();
      }
    }
  }

  ngOnInit(): void {}
}
