import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoValDocenteComponent } from './info-val-docente.component';

describe('InfoValDocenteComponent', () => {
  let component: InfoValDocenteComponent;
  let fixture: ComponentFixture<InfoValDocenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoValDocenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoValDocenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
