import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValUtenteComponent } from './val-utente.component';

describe('ValUtenteComponent', () => {
  let component: ValUtenteComponent;
  let fixture: ComponentFixture<ValUtenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValUtenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValUtenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
