import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ValUtentiService } from "../../../../api/val-utenti.service";

@Component({
  selector: "app-val-utente",
  templateUrl: "./val-utente.component.html",
  styleUrls: ["./val-utente.component.css"],
})
export class ValUtenteComponent implements OnInit {
  val1: number = 0;
  val2: number = 0;
  val3: number = 0;
  val4: number = 0;
  val5: number = 0;
  _soggetto: any;
  formGroup: any;
  oggetto: any;
  formGroupDef: any;
  array: any[];
  media: number = 0;

  @Input()
  public display1: boolean;
  @Output()
  onDialogClose: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  set soggetto(item: any) {
    this._soggetto = item;
    if (item) {
      this.buildForm();
      this.buildFormDef();
      console.log("SOGGETTO", item);
    }
  }

  get soggetto() {
    return this._soggetto;
  }

  constructor(public fb: FormBuilder, public service: ValUtentiService) {}

  ngOnInit(): void {}

  buildForm() {
    this.formGroup = this.fb.group({
      corso: [this.soggetto.corso],
      dipendente: [this.soggetto.dipendente],
      criterio1: ["Impegno"],
      criterio2: ["Logica"],
      criterio3: ["Apprendimento"],
      criterio4: ["Velocità"],
      criterio5: ["Ordine e precisione"],
      valore1: [""],
      valore2: [""],
      valore3: [""],
      valore4: [""],
      valore5: [""],
    });
  }

  buildFormDef() {
    this.formGroupDef = this.fb.group({
      corso: [this.soggetto.corso],
      dipendente: [this.soggetto.dipendente],
      criterio: [""],
      valore: [""],
    });
  }

  calcoloMedia() {
    this.media =
      (this.val1 + this.val2 + this.val3 + this.val4 + this.val5) / 5;
  }

  conferma() {
    console.log("VALORE: ", this.formGroup.value);
    if (this.formGroup.value) {
      this.array = [
        {
          label: this.formGroup.value.criterio1,
          value: this.formGroup.value.valore1,
        },
        {
          label: this.formGroup.value.criterio2,
          value: this.formGroup.value.valore2,
        },
        {
          label: this.formGroup.value.criterio3,
          value: this.formGroup.value.valore3,
        },
        {
          label: this.formGroup.value.criterio4,
          value: this.formGroup.value.valore4,
        },
        {
          label: this.formGroup.value.criterio5,
          value: this.formGroup.value.valore5,
        },
      ];
      this.oggetto = this.formGroupDef.value;
      this.array.forEach((element) => {
        this.oggetto.criterio = element.label;
        this.oggetto.valore = element.value;
        this.service.add(this.oggetto).subscribe((resp) => {
          // console.log("RESP", resp);
        });
      });
      this.exit();
    }
  }
  exit() {
    this.display1 = false;
    this.onDialogClose.emit(this.display1);
    this.val1 = 0;
    this.val2 = 0;
    this.val3 = 0;
    this.val4 = 0;
    this.val5 = 0;
    this.media = 0;
  }
}
