import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ValutazioniRoutingModule } from "./valutazioni-routing.module";
import { ValutazioniComponent } from "./valutazioni/valutazioni.component";
import { DropdownModule } from "primeng/dropdown";
import { TableModule } from "primeng/table";
import { ButtonModule } from "primeng/button";
import { DialogModule } from "primeng/dialog";
import { TabViewModule } from "primeng/tabview";
import { SliderModule } from "primeng/slider";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { InputTextModule } from "primeng/inputtext";
import { ScrollPanelModule } from "primeng/scrollpanel";
import { ValCorsoComponent } from "./valutazioni/val-corso/val-corso.component";
import { ValDocenteComponent } from "./valutazioni/val-docente/val-docente.component";
import { ValUtenteComponent } from "./valutazioni/val-utente/val-utente.component";
import { ListboxModule } from "primeng/listbox";
import { SpinnerModule } from "primeng/spinner";
import { InfoValUtenteComponent } from "./valutazioni/info-val-utente/info-val-utente.component";
import { InfoValDocenteComponent } from "./valutazioni/info-val-docente/info-val-docente.component";
import { InfoValCorsoComponent } from "./valutazioni/info-val-corso/info-val-corso.component";

@NgModule({
  declarations: [
    ValutazioniComponent,
    ValCorsoComponent,
    ValDocenteComponent,
    ValUtenteComponent,
    InfoValUtenteComponent,
    InfoValDocenteComponent,
    InfoValCorsoComponent,
  ],
  imports: [
    CommonModule,
    ValutazioniRoutingModule,
    DropdownModule,
    TableModule,
    ButtonModule,
    ScrollPanelModule,
    DialogModule,
    TabViewModule,
    SliderModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    ListboxModule,
    SpinnerModule,
  ],
})
export class ValutazioniModule {}
