import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditCorsoComponent } from './edit-corso.component';

const routes: Routes = [{ path: '', component: EditCorsoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditCorsoRoutingModule { }
