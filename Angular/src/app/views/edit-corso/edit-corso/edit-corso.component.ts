import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { CorsiApiService } from "../../../api/corsi-api.service";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { element } from "protractor";

@Component({
  selector: "app-edit-corso",
  templateUrl: "./edit-corso.component.html",
  styleUrls: ["./edit-corso.component.css"],
})
export class EditCorsoComponent implements OnInit {
  @Input()
  set corso(item: any) {
    this._corso = item;
    if (item) {
      this.getPrevistoErogato();
      this.transformDate();
      this.getLuogo();
      this.getSocieta();
      this.getMisure();
      this.getTipi();
      this.getTipologie();
      this.buildFormVuoto();
      this.buildForm();
    }
  }

  get corso() {
    return this._corso;
  }

  public descrizioneMis: any[];
  public argomenti: any[];
  public listaMisure: any[];
  public listaTipi: any[];
  public listaTipologie: any[];
  public societa: any[];
  public listaSocieta: any[];
  public _corso: any;
  public tipo: any[];
  public luogo: any[];
  public erogato: boolean;
  public stato: string;
  public tipologie: any[];
  public luoghiCorsi: any[];
  public corsoEdit: any;
  public temp: any;
  public radio = "radio";

  public dataCens: any;
  public dataInzio: any;
  public dataFine: any;
  previousVal: any;
  currentVal: any;

  public formGroup: FormGroup;

  constructor(
    public corsoApi: CorsiApiService,
    public fb: FormBuilder,
    public router: Router,
    public route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    // console.log("corso", history.state.corso);

    this.corso = history.state.corso;
    this.luogo = history.state.corso.luogo;
    // console.log("luogo", this.luogo);

    // this.getTipologie();
  }

  buildFormVuoto() {
    this.formGroup = this.fb.group({
      tipologia: [""],
      tipo: ["0"],
      previsto: [""],
      erogato: [""],
      durata: [""],
      misura: ["0"],
      luogo: [""],
      descrizione: [""],
      edizione: [""],
      dataCensimento: [""],
      interno: [""],
      note: [""],
      ente: [""],
      societa: ["0"],
      dataErogazione: [""],
      dataChiusura: [""],
    });
  }

  buildForm() {
    this.formGroup = this.fb.group({
      tipologia: [this.corso.tipologia],
      tipo: [this.corso.tipo],
      previsto: [this.corso.previsto],
      erogato: [this.corso.erogato],
      durata: [this.corso.durata],
      misura: [this.corso.misura],
      luogo: [this.corso.luogo],
      descrizione: [this.corso.descrizione],
      edizione: [this.corso.edizione],
      dataCensimento: [this.dataCens],
      interno: [this.corso.interno],
      note: [this.corso.note],
      ente: [this.corso.ente],
      societa: [this.corso.societa],
      dataErogazione: [this.dataInzio],
      dataChiusura: [this.dataFine],
    });
  }

  getPrevistoErogato() {
    if (this.corso.previsto == 1) {
      this.radio = "Previsto";
    }

    if (this.corso.erogato == 1) {
      this.radio = "Erogato";
    }
  }

  getMisure() {
    this.descrizioneMis = [];
    this.corsoApi.getMisure().subscribe((resp) => {
      this.listaMisure = resp.response;
      // console.log(this.listaMisure);
      if (this.listaMisure && this.corso) {
        this.listaMisure.forEach((element) => {
          if (element.misuraId == this.corso.misura.misuraId) {
            if (!this.descrizioneMis[0]) {
              this.descrizioneMis[0] = {
                label: this.corso.misura.descrizione,
                value: this.corso.misura,
              };
            } else {
              this.temp = this.descrizioneMis[0];
              // console.log("temp", this.temp);
              this.descrizioneMis[0] = {
                label: this.corso.misura.descrizione,
                value: this.corso.misura,
              };
              this.descrizioneMis.push(this.temp);
            }
          } else {
            this.descrizioneMis.push({
              label: element.descrizione,
              value: { element },
            });
          }
        });
      }
    });
  }

  getTipi() {
    this.argomenti = [];
    this.corsoApi.getTipo().subscribe((resp) => {
      this.listaTipi = resp.response;
      // console.log(this.listaTipi);
      if (this.listaTipi && this.corso) {
        this.listaTipi.forEach((element) => {
          if (element.tipoId == this.corso.tipo.tipoId) {
            if (!this.argomenti[0]) {
              this.argomenti[0] = {
                label: this.corso.tipo.descrizione,
                value: this.corso.tipo,
              };
            } else {
              this.temp = this.argomenti[0];
              // console.log("temp", this.temp);
              this.argomenti[0] = {
                label: this.corso.tipo.descrizione,
                value: this.corso.tipo,
              };
              this.argomenti.push(this.temp);
            }
          } else {
            this.argomenti.push({
              label: element.descrizione,
              value: { element },
            });
          }
        });
      }
    });
  }

  getTipologie() {
    this.tipologie = [];
    this.corsoApi.getTipologia().subscribe((resp) => {
      this.listaTipologie = resp.response;
      // console.log(this.listaTipologie);
      if (this.listaTipologie && this.corso) {
        this.listaTipologie.forEach((element) => {
          if (element.tipologiaId == this.corso.tipologia.tipologiaId) {
            if (!this.tipologie[0]) {
              this.tipologie[0] = {
                label: this.corso.tipologia.descrizione,
                value: this.corso.tipologia,
              };
            } else {
              this.temp = this.tipologie[0];
              // console.log("temp", this.temp);
              this.tipologie[0] = {
                label: this.corso.tipologia.descrizione,
                value: this.corso.tipologia,
              };
              this.tipologie.push(this.temp);
            }
          } else {
            this.tipologie.push({
              label: element.descrizione,
              value: { element },
            });
          }
        });
      }
    });
  }

  getSocieta() {
    this.societa = [];
    this.corsoApi.getSocieta().subscribe((resp) => {
      this.listaSocieta = resp.response;
      // console.log(this.listaSocieta);
      if (this.listaSocieta && this.corso) {
        this.listaSocieta.forEach((element) => {
          if (element.societaId == this.corso.societa.societaId) {
            if (!this.societa[0]) {
              this.societa[0] = {
                label: this.corso.societa.ragioneSociale,
                value: this.corso.societa,
              };
            } else {
              this.temp = this.societa[0];
              // console.log("temp", this.temp);
              this.societa[0] = {
                label: this.corso.societa.ragioneSociale,
                value: this.corso.societa,
              };
              this.societa.push(this.temp);
            }
          } else {
            this.societa.push({
              label: element.ragioneSociale,
              value: { element },
            });
          }
        });
      }
    });
  }

  getLuogo() {
    this.luoghiCorsi = [
      { label: "-Seleziona-", value: null },
      //sedi corvallis
      { label: "Bari", value: "Bari" },
      { label: "Brescia", value: "Brescia" },
      { label: "Bologna", value: "Bologna" },
      { label: "Caltagirone", value: "Caltagirone" },
      { label: "Catania", value: "Catania" },
      { label: "Firenze", value: "Firenze" },
      { label: "Genova", value: "Genova" },
      { label: "Livorno", value: "Livorno" },
      { label: "Modena", value: "Modena" },
      { label: "Milano", value: "Milano" },
      { label: "Padova", value: "Padova" },
      { label: "Palermo", value: "Palermo" },
      { label: "Roma", value: "Roma" },
      { label: "Torino", value: "Torino" },
      { label: "Trieste", value: "Trieste" },
      { label: "Venezia", value: "Venezia" },
      { label: "Verona", value: "Verona" },
    ];
    this.luoghiCorsi.forEach((element) => {
      if (this.corso) {
        if (element.value == this.corso.luogo) {
          this.luoghiCorsi[0] = {
            label: this.corso.luogo,
            value: this.corso.luogo,
          };
        }
      }
    });
  }

  svuotaCampi() {
    this.buildForm();
  }

  transformDate() {
    this.dataCens = new Date(this.corso.dataCensimento);
    this.dataInzio = new Date(this.corso.dataErogazione);
    this.dataFine = new Date(this.corso.dataChiusura);
  }

  conferma() {
    this.corsoEdit = this.formGroup.value;
    this.corsoEdit["corsoId"] = this.corso.corsoId;
    // console.log("corso", this.corsoEdit);

    if (this.corsoEdit) {
      if (this.corsoEdit.previsto == "Previsto") {
        this.corsoEdit.previsto = 1;
        this.corsoEdit.erogato = 0;
      }

      if (this.corsoEdit.erogato == "Erogato") {
        this.corsoEdit.erogato = 1;
        this.corsoEdit.previsto = 0;
      }

      if (this.corsoEdit.interno) {
        this.corsoEdit.interno = 1;
      } else {
        this.corsoEdit.interno = 0;
      }

      if (this.corsoEdit.tipo != this.corso.tipo) {
        this.corsoEdit.tipo = this.corsoEdit.tipo.value.element;
      } else {
        this.corsoEdit.tipo = this.corso.tipo;
      }
      if (this.corsoEdit.tipologia != this.corso.tipologia) {
        this.corsoEdit.tipologia = this.corsoEdit.tipologia.value.element;
      } else {
        this.corsoEdit.tipologia = this.corso.tipologia;
      }
      if (this.corsoEdit.misura != this.corso.misura) {
        this.corsoEdit.misura = this.corsoEdit.misura.value.element;
      } else {
        this.corsoEdit.misura = this.corso.misura;
      }
      if (this.corsoEdit.societa != this.corso.societa) {
        this.corsoEdit.societa = this.corsoEdit.societa.value.element;
      } else {
        this.corsoEdit.societa = this.corso.societa;
      }

      if (this.corsoEdit.luogo != this.corso.luogo) {
        this.corsoEdit.luogo = this.corsoEdit.luogo.value.element;
      } else {
        this.corsoEdit.luogo = this.corso.luogo;
      }

      this.corsoApi.replace(this.corsoEdit).subscribe((resp) => {
        // console.log("RESP", resp);
        this.router.navigate(["/corsi"]);
      });
    }
  }
}
