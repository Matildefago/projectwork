import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { EditCorsoRoutingModule } from "./edit-corso-routing.module";
import { EditCorsoComponent } from "./edit-corso.component";
import { RadioButtonModule } from "primeng/radiobutton";
import { InputTextModule } from "primeng/inputtext";
import { DropdownModule } from "primeng/dropdown";
import { AutoCompleteModule } from "primeng/autocomplete";
import { InputTextareaModule } from "primeng/inputtextarea";
import { CalendarModule } from "primeng/calendar";
import { CheckboxModule } from "primeng/checkbox";
import { MultiSelectModule } from "primeng/multiselect";
import { InputSwitchModule } from "primeng/inputswitch";
import { ListboxModule } from "primeng/listbox";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { DialogModule } from "primeng/dialog";

@NgModule({
  declarations: [EditCorsoComponent],
  imports: [
    CommonModule,
    EditCorsoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CheckboxModule,
    MultiSelectModule,
    InputSwitchModule,
    ListboxModule,
    RadioButtonModule,
    InputTextModule,
    DropdownModule,
    AutoCompleteModule,
    DialogModule,
    InputTextareaModule,
    CalendarModule,
  ],
})
export class EditCorsoModule {}
