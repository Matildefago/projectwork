import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { EditCorsoComponent } from "./edit-corso/edit-corso.component";

const routes: Routes = [
  {
    path: "",
    component: EditCorsoComponent,
    data: {
      title: "editCorso",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditCorsoRoutingModule {}
