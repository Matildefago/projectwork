import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { FullCalendarModule } from "primeng/fullcalendar";

import { DashboardComponent } from "./dashboard.component";
import { DashboardRoutingModule } from "./dashboard-routing.module";

@NgModule({
  imports: [FormsModule, DashboardRoutingModule, FullCalendarModule],
  declarations: [DashboardComponent],
})
export class DashboardModule {}
