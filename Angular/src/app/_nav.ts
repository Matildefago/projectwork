import { INavData } from "@coreui/angular";

export const navItems: INavData[] = [
  {
    name: "Benvenuto",
    url: "",
    icon: "icon-home",
  },
  {
    name: "Dipendenti",
    url: "/alunni",
    icon: "icon-compass",
    // children: [
    //   {
    //     name: " Lista Dipendenti",
    //     url: "/alunni",
    //     icon: "icon-puzzle",
    //   },
    //   {
    //     name: "Lista Docenti",
    //     url: "/docenti",
    //     icon: "icon-puzzle",
    //   },
    // ],
  },
  {
    name: "Docenti",
    url: "/docenti",
    icon: "icon-compass",
  },
  {
    name: "Corso",
    url: "/corso",
    icon: "icon-compass",
    children: [
      {
        name: "Lista Corsi",
        url: "/corsi",
        icon: "icon-puzzle",
      },
      {
        name: "Nuovo Corso",
        url: "/nuovoCorso",
        icon: "icon-puzzle",
      },
    ],
  },
  {
    name: "Valutazioni",
    url: "/valutazioni",
    icon: "icon-compass",
    children: [
      {
        name: "Lista Valutazioni",
        url: "/valutazioni",
        icon: "icon-puzzle",
      },
    ],
  },
];
