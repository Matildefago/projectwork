import { TestBed } from '@angular/core/testing';

import { DocentiApiService } from './docenti-api.service';

describe('DocentiApiService', () => {
  let service: DocentiApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DocentiApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
