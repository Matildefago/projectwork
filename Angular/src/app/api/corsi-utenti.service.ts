import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CorsiUtentiService {

  constructor(private api: ApiService) { }

  public associaUtenti(item: any):Observable<any>{
    return this.api.post('corsiUtenti/inserisci' , item);
  }

  public getDipByCorso(item: any):Observable<any>{
    return this.api.get('corsiUtenti/corso/' + item);
  }

  public eliminaAss(id: any, item:any):Observable<any>{
    return this.api.delete('corsiUtenti/elimina/' + id + '..' + item);
  }

  public modificaDurata(item:any):Observable<any>{
    return this.api.put('corsiUtenti/modifica', item);
  }

  public getCorsoByDip(item: any):Observable<any>{
    return this.api.get('corsiUtenti/dipendente/' + item);
  }

  public getAll():Observable<any>{
    return this.api.get('corsiUtenti/Ute');
  }

  public getAllPage(pagine:number, quantita:number):Observable<any> {
    return this.api.get('corsiUtenti/utenti/' + pagine + '..' + quantita);
  }

  public getNumberOfPage(pagine:number, quantita:number):Observable<any> {
    return this.api.get('corsiUtenti/pagesUte/' + pagine + '..' + quantita);
  }

  public filter(pagine:number, quantita:number,item: any):Observable<any> {
    return this.api.post('corsiUtenti/cerca/' + pagine + '...' + quantita, item);
  }

  public getNumberFiltro(pagine:number, quantita:number,item: any):Observable<any> {
    return this.api.post('corsiUtenti/cercaPagine/' + pagine + '...' + quantita, item);
  }

}
