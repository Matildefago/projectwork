import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class CorsiApiService {
  constructor(private api: ApiService) {}
  private readonly path = "corsi";
  private readonly pathMisura = "misure";
  private readonly pathTipo = "tipi";
  private readonly pathTipologia = "tipologie";
  private readonly pathTSocieta = "societa";

  public getAll(): Observable<any> {
    return this.api.get(this.path);
  }

  public deleteById(id: any): Observable<any> {
    return this.api.delete(this.path + "/elimina/" + id);
  }

  public add(item: any): Observable<any> {
    return this.api.post(this.path + "/inserisci", item);
  }

  public getById(id: any): Observable<any> {
    return this.api.get(this.path + "/id/" + id);
  }

  public replace(item: any): Observable<any> {
    return this.api.put(this.path + "/modifica", item);
  }

  public getAllPage(pagine: number, quantita: number): Observable<any> {
    return this.api.get(this.path + "/" + pagine + "..." + quantita);
  }

  public getNumberOfPage(pagine: number, quantita: number): Observable<any> {
    return this.api.get(this.path + "/getPages/" + pagine + "..." + quantita);
  }

  public getMisure(): Observable<any> {
    return this.api.get(this.pathMisura);
  }

  public getTipo(): Observable<any> {
    return this.api.get(this.pathTipo);
  }

  public getTipologia(): Observable<any> {
    return this.api.get(this.pathTipologia);
  }

  public getSocieta(): Observable<any> {
    return this.api.get(this.pathTSocieta);
  }
  public filtro(
    item: string,
    pagina: number,
    quantita: number
  ): Observable<any> {
    return this.api.post(
      this.path + "/cerca/" + pagina + "..." + quantita,
      item
    );
  }
  public getNumberOfPageFilter(
    item: string,
    pagina: number,
    quantita: number
  ): Observable<any> {
    return this.api.post(
      this.path + "/getPagesFilter/" + pagina + "..." + quantita,
      item
    );
  }
}
