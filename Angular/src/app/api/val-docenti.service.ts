import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ValDocentiService {
  constructor(private api: ApiService) {}
  private readonly path = "valutazioniDocenti";

  public getAll(): Observable<any> {
    return this.api.get(this.path + "/valdocenti");
  }

  public add(item: any): Observable<any> {
    return this.api.post(this.path + "/inserisci", item);
  }

  public info(idDoc: any, idCor: any): Observable<any> {
    return this.api.get(this.path + "/info/" + idDoc + ".." + idCor);
  }

  public elimina(item: any): Observable<any> {
    return this.api.delete(this.path + "/elimina/" + item);
  }
  public filtro(item: string): Observable<any> {
    return this.api.post(this.path + "/cerca/", item);
  }
}
