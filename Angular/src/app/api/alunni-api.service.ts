import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ApiService } from "./api.service";

@Injectable({
  providedIn: "root",
})
export class AlunniApiService {
  constructor(private api: ApiService) {}
  private readonly path = "dipendenti";

  public getAll(): Observable<any> {
    return this.api.get(this.path);
  }
  public getAlunni(): Observable<any> {
    return this.api.get(this.path + "/alunni");
  }

  public getAllPage(pagine: number, quantita: number): Observable<any> {
    return this.api.get(this.path + "/" + pagine + "..." + quantita);
  }

  public getNumberOfPage(pagine: number, quantita: number): Observable<any> {
    return this.api.get(this.path + "/getPages/" + pagine + "..." + quantita);
  }

  public getById(item: any): Observable<any> {
    return this.api.get(this.path + "/id/" + item);
  }

  public doppioni(pagine: number, quantita: number): Observable<any> {
    return this.api.get(this.path + "/doppioni/" + pagine + ".." + quantita);
  }

  public doppioniPages(pagine: number, quantita: number): Observable<any> {
    return this.api.get(
      this.path + "/getPagesDopp/" + pagine + "..." + quantita
    );
  }

  public totDoppioni(): Observable<any> {
    return this.api.get(this.path + "/totDoppioni");
  }

  public filtro(
    item: string,
    pagina: number,
    quantita: number
  ): Observable<any> {
    return this.api.post(
      this.path + "/cerca/" + pagina + "..." + quantita,
      item
    );
  }
  public getNumberOfPageFilter(
    item: string,
    pagina: number,
    quantita: number
  ): Observable<any> {
    return this.api.post(
      this.path + "/getPagesFilter/" + pagina + "..." + quantita,
      item
    );
  }
  public filtroInt(
    item: string,
    pagina: number,
    quantita: number
  ): Observable<any> {
    return this.api.post(
      this.path + "/cercaInt/" + pagina + "..." + quantita,
      item
    );
  }
  public getNumberOfPageFilterInt(
    item: string,
    pagina: number,
    quantita: number
  ): Observable<any> {
    return this.api.post(
      this.path + "/getPagesFilterInt/" + pagina + "..." + quantita,
      item
    );
  }
}
