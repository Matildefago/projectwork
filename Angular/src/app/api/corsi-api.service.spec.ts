import { TestBed } from '@angular/core/testing';

import { CorsiApiService } from './corsi-api.service';

describe('CorsiApiService', () => {
  let service: CorsiApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CorsiApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
