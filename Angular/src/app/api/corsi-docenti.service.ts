import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class CorsiDocentiService {
  constructor(private api: ApiService) {}

  public associaDocenti(item: any): Observable<any> {
    return this.api.post("corsiDocenti/inserisci", item);
  }

  public getDocByCorso(item: any): Observable<any> {
    return this.api.get("corsiDocenti/docenti/" + item);
  }

  public getCorsiByEsterno(item: any): Observable<any> {
    return this.api.get("corsiDocenti/corsiEsterni/" + item);
  }

  public getCorsiByInterno(item: any): Observable<any> {
    return this.api.get("corsiDocenti/corsiInterni/" + item);
  }

  public getPagesEsterni(pagine: number, quantita: number) {
    return this.api.get("corsiDocenti/docentiEst/" + pagine + ".." + quantita);
  }

  public numberEsterni(pagine: number, quantita: number) {
    return this.api.get("corsiDocenti/pagesDocEst/" + pagine + ".." + quantita);
  }

  public getPagesInterni(pagine: number, quantita: number) {
    return this.api.get("corsiDocenti/docentiInt/" + pagine + ".." + quantita);
  }

  public numberInterni(pagine: number, quantita: number) {
    return this.api.get("corsiDocenti/pagesDocInt/" + pagine + ".." + quantita);
  }

  public delete(index: any) {
    return this.api.delete("corsiDocenti/elimina/" + index);
  }
  public filter(
    campo: string,
    pagine: number,
    quantita: number
  ): Observable<any> {
    return this.api.post(
      "corsiDocenti/cerca/interno/" + pagine + ".." + quantita,
      campo
    );
  }
  public getPagesFiltro(item: string, quantita: number): Observable<any> {
    return this.api.post(
      "corsiDocenti/getPages/interno/filtro/" + quantita,
      item
    );
  }
}
