import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ValUtentiService {
  constructor(private api: ApiService) {}
  private readonly path = "valutazioniUtenti";

  public getAll(): Observable<any> {
    return this.api.get(this.path + "/valutenti");
  }

  public add(item: any): Observable<any> {
    return this.api.post(this.path + "/inserisci", item);
  }

  public getInfo(idDip: any, idCors: any) {
    return this.api.get(this.path + "/info/" + idDip + ".." + idCors);
  }

  public elimina(item: any): Observable<any> {
    return this.api.delete(this.path + "/elimina/" + item);
  }
  public filtro(item: string): Observable<any> {
    return this.api.post(this.path + "/cerca/", item);
  }
}
