import { TestBed } from '@angular/core/testing';

import { CorsiDocentiService } from './corsi-docenti.service';

describe('CorsiDocentiService', () => {
  let service: CorsiDocentiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CorsiDocentiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
