import { TestBed } from '@angular/core/testing';

import { ValUtentiService } from './val-utenti.service';

describe('ValUtentiService', () => {
  let service: ValUtentiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValUtentiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
