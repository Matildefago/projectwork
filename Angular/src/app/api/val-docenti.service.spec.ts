import { TestBed } from '@angular/core/testing';

import { ValDocentiService } from './val-docenti.service';

describe('ValDocentiService', () => {
  let service: ValDocentiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValDocentiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
