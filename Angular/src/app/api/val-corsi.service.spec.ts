import { TestBed } from '@angular/core/testing';

import { ValCorsiService } from './val-corsi.service';

describe('ValCorsiService', () => {
  let service: ValCorsiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValCorsiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
