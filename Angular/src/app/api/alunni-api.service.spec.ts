import { TestBed } from "@angular/core/testing";
import { AlunniApiService } from "./alunni-api.service";

describe("AlunniApiService", () => {
  let service: AlunniApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AlunniApiService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
