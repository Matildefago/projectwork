import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class DocentiApiService {

  constructor(private api:ApiService) { }
  private readonly path = 'docenti';

  public getAll():Observable<any> {
    return this.api.get(this.path);
  }

  public deleteById(id: any):Observable<any>{
    return this.api.delete(this.path + '/elimina/' + id);
  }

  public add(item:any):Observable<any>{
     return this.api.post(this.path + '/inserisci', item);
   }

   public getById(id: any):Observable<any>{
    return this.api.get(this.path + '/id/' + id);
   }

   public replace(item:any):Observable<any>{
     return this.api.put(this.path + '/modifica', item);
   }

   public getAllDip():Observable<any> {
    return this.api.get('dipendenti/listaDocenti');
  }

  public getAllPage(pagine:number, quantita:number):Observable<any> {
    return this.api.get(this.path + '/' + pagine + '...' + quantita);
  }

  public getNumberOfPage(pagine:number, quantita:number):Observable<any> {
    return this.api.get(this.path + '/getPages/' + pagine + '...' + quantita);
  }

  public filter(campo: string, pagine:number, quantita:number):Observable<any> {
    return this.api.post(this.path + '/cerca/' + pagine + '...' + quantita, campo);
  }

  public getNumberOfPageFilter(campo: string, pagine:number, quantita:number):Observable<any> {
    return this.api.post(this.path + '/getPagesFilter/' + pagine + '...' + quantita , campo);
  }

}
