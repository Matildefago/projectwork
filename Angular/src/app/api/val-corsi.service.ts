import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ValCorsiService {
  constructor(private api: ApiService) {}
  private readonly path = "valutazioniCorsi";

  public getAll(): Observable<any> {
    return this.api.get(this.path + "/valcorso");
  }

  public add(item: any): Observable<any> {
    return this.api.post(this.path + "/inserisci", item);
  }

  public info(item: any): Observable<any> {
    return this.api.get(this.path + "/info/" + item);
  }

  public elimina(item: any): Observable<any> {
    return this.api.delete(this.path + "/elimina/" + item);
  }
  public filtro(item: string): Observable<any> {
    return this.api.post(this.path + "/cerca/", item);
  }
}
